using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LevelCompleteTimer : MonoBehaviour
{
    public static LevelCompleteTimer current;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(this);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (LevelManager.current.LevelCompleteTimer > 0)
        {
            StartCoroutine(CountDown());
        }
    }

    public void RestartCountdown()
    {
        StartCoroutine(CountDown());
    }
    IEnumerator CountDown()
    {
        while (LevelManager.current.LevelCompleteTimer > 0 && LevelManager.current.isGameOver == false)
        {
            yield return new WaitForSeconds(1f);
            LevelManager.current.LevelCompleteTimer--;
        }
    }
}
