using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSpecialBlock : MonoBehaviour
{
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        transform.position = target.position + new Vector3(0, 0,-0.8f);
       
    }
}
