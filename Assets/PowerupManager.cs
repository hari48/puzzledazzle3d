using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
public class PowerupManager : MonoBehaviour
{
    public static PowerupManager current;

    [SerializeField]
    Button PowerupButton;
    [SerializeField]
    GameObject PowerupVisualIndicator;
    [SerializeField]
    GameObject BombLightningstrike;

    [SerializeField]
    Button PowerUpButtonSwap;

    [SerializeField]
    Button PowerUpButtonSpecialAdd;
    bool powerUPClciked;
    bool PowerUpSwapClicked;
    bool PowerUpSpecialAdd;

    [SerializeField]
    int range = 2;

    int a = 0;
    GameObject gm1;
    GameObject gm2;
    Vector2 firstgrid;

    bool fingerpressed = false;
    float h = 0, v = 0;
    Vector2 pressedcube;

    public GameObject HorizontalSpecialAdd;
    public GameObject VerticalSpecialAdd;

    [SerializeField]
    int PowerUpBombCounter;
    [SerializeField]
    int PowerUpSwapCounter;
    [SerializeField]
    int PowerUpSpecialAddCounter;
    [SerializeField]
    TMP_Text PowerUPBombCountText;
    [SerializeField]
    TMP_Text PowerUPSwapCountText;
    [SerializeField]
    TMP_Text PowerUPSpecialAddCountText;

    [SerializeField]
    GameObject PowerupBombCancelButton;
    [SerializeField]
    GameObject PowerupSwapCancelButton;
    [SerializeField]
    GameObject PowerupSpecialaddCancelButton;

    GameObject bombstoreui;
    GameObject swapstoreui;
    GameObject adspecialstoreui;

    [SerializeField]
    float SwapSpeed = 1;

    int camzvalue;

    bool isbombing = false;
    bool isswapping = false;
    bool isadspescial = false;

    PieceControl controls;
    EventSystem es;

    [SerializeField]
    GameObject SelectBlock;

    GameObject SpawnedSelectBlock;
    bool selectblockspawned = false;

    bool adspecialcotnrollercheck=false;
    private void Awake()
    {
        camzvalue = 20;
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(this);
        }
        es = EventSystem.current;
        controls = new PieceControl();
        controls.Gameplay.PowerupMenu.performed += ctx => PowerupButtonClicked();
        controls.UI.Select.performed += ctx => UsePowerUPClicked();
        controls.UI.Up.performed += ctx => MoveSelectUP();
        controls.UI.Down.performed += ctx => MoveSelectDown();
        controls.UI.Left.performed += ctx => MoveSelectLeft();
        controls.UI.Right.performed += ctx => MoveSelectRight();
    }
    private void OnEnable()
    {
        controls.Gameplay.Enable();
        controls.UI.Enable();
    }
    private void OnDisable()
    {
        controls.Gameplay.Disable();
        controls.UI.Disable();
    }

    bool Pclick = false;
    void PowerupButtonClicked()
    {
        if (Pclick == false)
        {
            Pclick = true;
            GameManager.instance.PauseGameWithoutUI();
            es.SetSelectedGameObject(PowerupButton.gameObject);

        }
        else
        {
            Pclick = false;
            powerUPClciked = false;
            PowerUpSwapClicked = false;
            PowerUpSpecialAdd = false;
            EnablePowerUPButtons();
            GameManager.instance.ResumeGameWithoutUI();
            es.SetSelectedGameObject(null);

        }

    }

    void UsePowerUPClicked()
    {
        if (powerUPClciked)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;


            if (x >= 0 && y >= 0 && x < 16 && y < 10)
            {
                powerUPClciked = false;
                StartCoroutine(PowerUpExplosion(x, y));
            }
        }

        
        if (PowerUpSwapClicked && a <= 2)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;

            if (x >= 0 && y >= 0 && x < 16 && y < 10)
            {
                if (GridSystem.grid[x, y] != null)
                {
                    a++;
                    if (a == 1)
                    {
                        gm1 = GridSystem.grid[x, y].gameObject;
                        firstgrid = new Vector2(x, y);
                    }
                    if (a == 2)
                    {
                        gm2 = GridSystem.grid[x, y].gameObject;

                        Vector3 temp1;
                        Vector3 temp2;

                        temp1 = gm1.transform.position;
                        temp2 = gm2.transform.position;
                        //GetComponent<AudioSource>().Play();
                        StartCoroutine(PowerUPSwapAnimation(gm1, gm2, temp2, temp1, x, y));

                    }
                }
            }
        }

        if (PowerUpSpecialAdd)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;
            if (x >= 0 && y >= 0 && x < 16 && y < 10)
            {
                if (GridSystem.grid[x, y].gameObject.name != "stripeHorizontal" && GridSystem.grid[x, y].gameObject.name != "stripeVertical")
                {
                    adspecialcotnrollercheck = true;
                    h = x;
                    v = y;
                    pressedcube = new Vector2(x, y);
                }
            }
        }

    }

    void MoveSelectUP()
    {
        if (selectblockspawned)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;
            if (x >= 0 && y >= 0 && x < 10 && y < 16)
            {
                SpawnedSelectBlock.transform.position += new Vector3(0, 1, 0);
            }
                
        }

        if (PowerUpSpecialAdd && adspecialcotnrollercheck)
        {
            adspecialcotnrollercheck = false;
           
            GameObject t = GridSystem.grid[(int)pressedcube.x, (int)pressedcube.y].gameObject;
            t.name = "stripeVertical";
            GameObject g = Instantiate(VerticalSpecialAdd, t.transform.position, Quaternion.Euler(-90, 0, 0));
            //g.transform.Rotate(0, 0, 90);
            g.GetComponent<FollowSpecialBlock>().target = t.transform;
            
            PowerUpSpecialAdd = false;
            if (PowerUpSpecialAddCounter > 0)
            {
                PowerUpSpecialAddCounter--;
            }
            EnablePowerUPButtons();
            isadspescial = false;
            GridSystem.instance.FinishClearing();
        }
    }
    void MoveSelectDown()
    {
        if (selectblockspawned)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;
            if (x >= 0 && y >0 && x < 10 && y <= 16)
            {
                SpawnedSelectBlock.transform.position += new Vector3(0, -1, 0);
            }
        }

        if (PowerUpSpecialAdd && adspecialcotnrollercheck)
        {

            adspecialcotnrollercheck = false;
  
            GameObject t = GridSystem.grid[(int)pressedcube.x, (int)pressedcube.y].gameObject;
            t.name = "stripeVertical";
            GameObject g = Instantiate(VerticalSpecialAdd, t.transform.position, Quaternion.Euler(-90, 0, 0));
            //g.transform.Rotate(0, 0, 90);
            g.GetComponent<FollowSpecialBlock>().target = t.transform;
            
            PowerUpSpecialAdd = false;
            if (PowerUpSpecialAddCounter > 0)
            {
                PowerUpSpecialAddCounter--;
            }
            EnablePowerUPButtons();
            isadspescial = false;
            GridSystem.instance.FinishClearing();
        }
    }
    void MoveSelectLeft()
    {
        if (selectblockspawned)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;
            if (x > 0 && y >= 0 && x <= 10 && y <= 16)
            {
                SpawnedSelectBlock.transform.position += new Vector3(-1,0, 0);
            }
        }

        if (PowerUpSpecialAdd && adspecialcotnrollercheck)
        {

            adspecialcotnrollercheck = false;

            GameObject t = GridSystem.grid[(int)pressedcube.x, (int)pressedcube.y].gameObject;
            t.name = "stripeHorizontal";
            GameObject g = Instantiate(HorizontalSpecialAdd, t.transform.position, Quaternion.Euler(-90, 0, 0));
            g.GetComponent<FollowSpecialBlock>().target = t.transform;

            PowerUpSpecialAdd = false;
            if (PowerUpSpecialAddCounter > 0)
            {
                PowerUpSpecialAddCounter--;
            }
            EnablePowerUPButtons();
            isadspescial = false;
            GridSystem.instance.FinishClearing();
        }
    }
    void MoveSelectRight()
    {
        if (selectblockspawned)
        {
            int x = (int)SpawnedSelectBlock.transform.position.x;
            int y = (int)SpawnedSelectBlock.transform.position.y;
            if (x >= 0 && y >= 0 && x < 9 && y <= 16)
            {
                SpawnedSelectBlock.transform.position += new Vector3(1, 0, 0);
            }
        }

        if (PowerUpSpecialAdd && adspecialcotnrollercheck)
        {

            adspecialcotnrollercheck = false;

            GameObject t = GridSystem.grid[(int)pressedcube.x, (int)pressedcube.y].gameObject;
            t.name = "stripeHorizontal";
            GameObject g = Instantiate(HorizontalSpecialAdd, t.transform.position, Quaternion.Euler(-90, 0, 0));
            g.GetComponent<FollowSpecialBlock>().target = t.transform;

            PowerUpSpecialAdd = false;
            if (PowerUpSpecialAddCounter > 0)
            {
                PowerUpSpecialAddCounter--;
            }
            EnablePowerUPButtons();
            isadspescial = false;
            GridSystem.instance.FinishClearing();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //  PurchaseUI pui = GameObject.FindObjectOfType<PurchaseUI>();
        // bombstoreui = pui.bombui;
        // swapstoreui = pui.swapui;
        //  adspecialstoreui = pui.adspecialui;

        PowerUpBombCounter = PlayerPrefs.GetInt("PBombCount", 3);
        PowerUpSwapCounter = PlayerPrefs.GetInt("PSwapCount", 3);
        PowerUpSpecialAddCounter = PlayerPrefs.GetInt("PSpecialAddCount", 3);

        SetPowerUPCountText();

    }

    // Update is called once per frame
    void Update()
    {/*
        if (Input.GetKeyDown(KeyCode.B) && isbombing==false)
        {
            isbombing = true;
            OnPowerUPCLicked();
        }
        else if (Input.GetKeyDown(KeyCode.B) && isbombing == true)
        {
            isbombing = false;
            OnBombCancel();
        }

        if (Input.GetKeyDown(KeyCode.N) && isswapping == false)
        {
            isswapping = true;
            OnPowerUPClickedSwap();
        }
        else if (Input.GetKeyDown(KeyCode.N) && isswapping == true)
        {
            isswapping = false;
            OnSwapCancel();
        }

        if (Input.GetKeyDown(KeyCode.M) && isadspescial == false)
        {
            isadspescial = true;
            OnPowerUpSpecialAddClicked();
        }
        else if (Input.GetKeyDown(KeyCode.M) && isadspescial == true)
        {
            isadspescial = false;
            OnAddSpecialCancel();
        }*/

        if (Input.GetMouseButtonDown(0) && powerUPClciked)
        {
            int x = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).x + 0.5f);
            int y = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).y + 0.5f);


            if (x >= 0 && y >= 0 && x < 10 && y < 16)
            {
                powerUPClciked = false;
                StartCoroutine(PowerUpExplosion(x, y));
            }

        }

        if (Input.GetMouseButtonDown(0) && PowerUpSwapClicked && a <= 2)
        {
            int x = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).x + 0.5f);
            int y = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).y + 0.5f);

            if (x >= 0 && y >= 0 && x < 10 && y < 16)
            {
                if (GridSystem.grid[x, y] != null)
                {
                    a++;
                    if (a == 1)
                    {
                        gm1 = GridSystem.grid[x, y].gameObject;
                        firstgrid = new Vector2(x, y);
                    }
                    if (a == 2)
                    {
                        gm2 = GridSystem.grid[x, y].gameObject;

                        Vector3 temp1;
                        Vector3 temp2;

                        temp1 = gm1.transform.position;
                        temp2 = gm2.transform.position;
                        //GetComponent<AudioSource>().Play();
                        StartCoroutine(PowerUPSwapAnimation(gm1, gm2, temp2, temp1, x, y));

                    }
                }
            }

        }

        if (Input.GetMouseButtonDown(0) && PowerUpSpecialAdd)
        {
            int x = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).x + 0.5f);
            int y = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).y + 0.5f);
            if (x >= 0 && y >= 0 && x < 10 && y < 16)
            {
                if (GridSystem.grid[x, y].gameObject.name != "stripeHorizontal" && GridSystem.grid[x, y].gameObject.name != "stripeVertical")
                {
                    fingerpressed = true;
                    h = x;
                    v = y;
                    pressedcube = new Vector2(x, y);
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && PowerUpSpecialAdd && fingerpressed)
        {
            int x = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).x + 0.5f);
            int y = (int)(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camzvalue)).y + 0.5f);
            fingerpressed = false;
            if (Mathf.Abs(x - h) >= Mathf.Abs(y - v))
            {
                GameObject t = GridSystem.grid[(int)pressedcube.x, (int)pressedcube.y].gameObject;
                t.name = "stripeHorizontal";
                GameObject g = Instantiate(HorizontalSpecialAdd, t.transform.position, Quaternion.Euler(-90, 0, 0));
                g.GetComponent<FollowSpecialBlock>().target = t.transform;
            }
            else
            {
                GameObject t = GridSystem.grid[(int)pressedcube.x, (int)pressedcube.y].gameObject;
                t.name = "stripeVertical";
                GameObject g = Instantiate(VerticalSpecialAdd, t.transform.position, Quaternion.Euler(-90, 0, 0));
                //g.transform.Rotate(0, 0, 90);
                g.GetComponent<FollowSpecialBlock>().target = t.transform;
            }
            PowerUpSpecialAdd = false;
            if (PowerUpSpecialAddCounter > 0)
            {
                PowerUpSpecialAddCounter--;
            }
            EnablePowerUPButtons();
            isadspescial = false;
            GridSystem.instance.FinishClearing();
        }


        /*
        if (GoogleAdsManager.current.RewardpowerUP == 1)
        {
            PowerUpBombCounter++;
            PlayerPrefs.SetInt("PBombCount", PlayerPrefs.GetInt("PBombCount", 3) + 1);
            GoogleAdsManager.current.RewardpowerUP = -1;
        }

        if (GoogleAdsManager.current.RewardpowerUP == 2)
        {
            PowerUpSwapCounter++;
            PlayerPrefs.SetInt("PSwapCount", PlayerPrefs.GetInt("PSwapCount", 3) + 1);
            GoogleAdsManager.current.RewardpowerUP = -1;
        }

        if (GoogleAdsManager.current.RewardpowerUP == 3)
        {
            PowerUpSpecialAddCounter++;
            PlayerPrefs.SetInt("PSpecialAddCount", PlayerPrefs.GetInt("PSpecialAddCount", 3) + 1);
            GoogleAdsManager.current.RewardpowerUP = -1;
        }*/

        SetPowerUPCountText();
    }

    IEnumerator PowerUpExplosion(int x, int y)
    {

        //GameObject gobj = Instantiate(PowerupVisualIndicator, new Vector3(x, y, 0), Quaternion.identity);
        //yield return new WaitForSecondsRealtime(0.5f);
        //Destroy(gobj);
        //GameObject l = Instantiate(BombLightningstrike, new Vector3(x, y, 0), Quaternion.Euler(-90, 0, 0));
        yield return new WaitForSecondsRealtime(0.5f);
        //Destroy(l, 1f);

        if (x >= 0 && y >= 0 && x < 10 && y < 16)
        {
            for (int i = 1; i < range; i++)
            {
                for (int a = i; a >= -i; a--)
                {
                    for (int b = -i; b <= i; b++)
                    {
                        if (x + a >= 0 && x + a < 16 && y + b >= 0 && y + b < 10)
                        {
                            if (GridSystem.grid[x + a, y + b] != null)
                            {
                                GridSystem.instance.ClearWithoutMatch(x + a, y + b);
                            }
                        }
                    }
                }
            }
        }
        if (PowerUpBombCounter > 0)
        {
            PowerUpBombCounter--;
        }
        EnablePowerUPButtons();
        isbombing = false;
        GridSystem.instance.SettlePeicesinEmpty();
    }
    bool gm1FinishedMove = false;
    bool gm2FinishedMove = false;
    IEnumerator PowerUPSwapAnimation(GameObject gm1, GameObject gm2, Vector3 target1, Vector3 target2, int x, int y)
    {
        gm1FinishedMove = false;
        gm2FinishedMove = false;

        StartCoroutine(MoveGM1(gm1, target1));
        StartCoroutine(MoveGM2(gm2, target2));

        while (gm1FinishedMove == false || gm2FinishedMove == false)
        {
            yield return null;
        }

        GridSystem.grid[(int)firstgrid.x, (int)firstgrid.y] = gm2.transform;
        GridSystem.grid[x, y] = gm1.transform;
        PowerUpSwapClicked = false;
        if (PowerUpSwapCounter > 0)
        {
            PowerUpSwapCounter--;
        }
        EnablePowerUPButtons();
        isswapping = false;
        GridSystem.instance.FinishClearing();
    }
    IEnumerator MoveGM1(GameObject gm1, Vector3 target1)
    {
        //gm1.GetComponent<SpriteRenderer>().sortingOrder = 1;
        //gm1.transform.localScale = new Vector3(1, 1, 1);
        while (gm1.transform.position != target1)
        {
            gm1.transform.position = Vector3.MoveTowards(gm1.transform.position, target1, SwapSpeed * Time.fixedDeltaTime);
            yield return null;
        }
        //gm1.GetComponent<SpriteRenderer>().sortingOrder = 0;
        // gm1.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
        gm1FinishedMove = true;
    }
    IEnumerator MoveGM2(GameObject gm2, Vector3 target2)
    {
        //gm2.GetComponent<SpriteRenderer>().sortingOrder = 1;
        // gm2.transform.localScale = new Vector3(1, 1, 1);
        while (gm2.transform.position != target2)
        {
            gm2.transform.position = Vector3.MoveTowards(gm2.transform.position, target2, SwapSpeed * Time.fixedDeltaTime);
            yield return null;
        }
        //gm2.GetComponent<SpriteRenderer>().sortingOrder = 0;
        //gm2.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
        gm2FinishedMove = true;
    }

    public void OnPowerUPCLicked()
    {/*
        if (PowerUpBombCounter <= 0)
        {
            bombstoreui.SetActive(true);
            GameManager.instance.PauseGameWithoutUI();
            return;
        }*/
        DisablePowerUpbuttons();
        powerUPClciked = true;
        PowerupBombCancelButton.SetActive(true);

        SpawnedSelectBlock = Instantiate(SelectBlock, new Vector3(0, 0, -1), Quaternion.identity);
        selectblockspawned = true;
    }

    public void OnPowerUPClickedSwap()
    {
        /*if (PowerUpSwapCounter <= 0)
        {
            swapstoreui.SetActive(true);
            GameManager.instance.PauseGameWithoutUI();
            return;
        }*/

        DisablePowerUpbuttons();
        PowerUpSwapClicked = true;
        a = 0;
        gm1 = null;
        gm2 = null;
        PowerupSwapCancelButton.SetActive(true);

        SpawnedSelectBlock = Instantiate(SelectBlock, new Vector3(0, 0, -1), Quaternion.identity);
        selectblockspawned = true;
    }

    public void OnPowerUpSpecialAddClicked()
    {/*
        if (PowerUpSpecialAddCounter <= 0)
        {
            adspecialstoreui.SetActive(true);
            GameManager.instance.PauseGameWithoutUI();
            return;
        }*/
        DisablePowerUpbuttons();
        PowerUpSpecialAdd = true;
        fingerpressed = false;
        h = 0;
        v = 0;
        PowerupSpecialaddCancelButton.SetActive(true);

        SpawnedSelectBlock = Instantiate(SelectBlock, new Vector3(0, 0, -1), Quaternion.identity);
        selectblockspawned = true;
    }

    public void OnBombCancel()
    {
        powerUPClciked = false;
        EnablePowerUPButtons();
    }

    public void OnSwapCancel()
    {
        PowerUpSwapClicked = false;
        EnablePowerUPButtons();
    }

    public void OnAddSpecialCancel()
    {
        PowerUpSpecialAdd = false;
        EnablePowerUPButtons();
    }

    void DisablePowerUpbuttons()
    {
        PowerupButton.interactable = false;
        PowerUpButtonSwap.interactable = false;
        PowerUpButtonSpecialAdd.interactable = false;
        GameManager.instance.PauseGameWithoutUI();
    }
    void EnablePowerUPButtons()
    {
        PowerupButton.interactable = true;
        PowerUpButtonSwap.interactable = true;
        PowerUpButtonSpecialAdd.interactable = true;
        PlayerPrefs.SetInt("PBombCount", PowerUpBombCounter);
        PlayerPrefs.SetInt("PSwapCount", PowerUpSwapCounter);
        PlayerPrefs.SetInt("PSpecialAddCount", PowerUpSpecialAddCounter);
        PowerupBombCancelButton.SetActive(false);
        PowerupSwapCancelButton.SetActive(false);
        PowerupSpecialaddCancelButton.SetActive(false);
        GameManager.instance.ResumeGameWithoutUI();

        if (SpawnedSelectBlock != null)
        {
            Destroy(SpawnedSelectBlock);
        }
        selectblockspawned = false;
    }
    void SetPowerUPCountText()
    {
        PowerUPBombCountText.text = PowerUpBombCounter.ToString();
        PowerUPSwapCountText.text = PowerUpSwapCounter.ToString();
        PowerUPSpecialAddCountText.text = PowerUpSpecialAddCounter.ToString();
    }

    /*
    public void ShowPowerupAD(int PowerupID)
    {
        GoogleAdsManager.current.ShowRewardedAd(PowerupID);
    }*/



    public void AddBombPowerUp(int value)
    {
        PowerUpBombCounter = PowerUpBombCounter + value;
    }
    public void AddSwapPowerUp(int value)
    {
        PowerUpSwapCounter = PowerUpSwapCounter + value;
    }
    public void AddSpecialAddPowerup(int value)
    {
        PowerUpSpecialAddCounter = PowerUpSpecialAddCounter + value;
    }
}
