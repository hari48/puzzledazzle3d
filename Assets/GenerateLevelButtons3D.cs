using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class GenerateLevelButtons3D : MonoBehaviour
{
    [SerializeField]
    GameObject Button3d;
    [SerializeField]
    Transform CameraT;

    [SerializeField]
    List<GameObject> buttonboxes = new List<GameObject>();

    public int currentfocuesedButton;
    public float cameraSpeed=100f;
    public float boxanimateSpeed = 100f;
    [Range(0, 125)]
    public List<int> IndividualButtonPos;

    Camera camerathis;

    PieceControl controls;
    private void Awake()
    {
        controls = new PieceControl();
        controls.UI.Left.performed += ctx => PreviousLevel();
        controls.UI.Right.performed += ctx => NextLevel();
        controls.UI.Select.performed += cx => SelectLevel();
    }
    private void OnEnable()
    {
        controls.UI.Enable();
    }
    private void OnDisable()
    {
        controls.UI.Disable();
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 50; i++)
        {
            GameObject bth = Instantiate(Button3d, transform);
            bth.transform.position = new Vector3(IndividualButtonPos[i],transform.position.y,transform.position.z-(i*100));
            SelectLevel sl = bth.GetComponent<SelectLevel>();
            sl.ButtonLevelid = i + 1;
            sl.lvltxt.text = (i + 1).ToString();
            buttonboxes.Add(bth);
        }

        currentfocuesedButton = PlayerPrefs.GetInt("MaxLevel",1)-1;
        CameraT.position = new Vector3(buttonboxes[currentfocuesedButton].transform.position.x, CameraT.position.y, buttonboxes[currentfocuesedButton].transform.position.z);

        camerathis = CameraT.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //CameraT.position = new Vector3(buttonboxes[currentfocuesedButton].transform.position.x, CameraT.position.y, buttonboxes[currentfocuesedButton].transform.position.z);
        CameraT.position = Vector3.MoveTowards(CameraT.position, new Vector3(buttonboxes[currentfocuesedButton].transform.position.x, CameraT.position.y, buttonboxes[currentfocuesedButton].transform.position.z), cameraSpeed * Time.deltaTime);

      
        buttonboxes[currentfocuesedButton].transform.position = Vector3.MoveTowards(buttonboxes[currentfocuesedButton].transform.position, new Vector3(buttonboxes[currentfocuesedButton].transform.position.x, 80f, buttonboxes[currentfocuesedButton].transform.position.z), boxanimateSpeed * Time.deltaTime);
        
        /*
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            PreviousLevel();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            NextLevel();
        }
        */
        for (int i = 0; i < 50; i++)
        {
            if (buttonboxes[i].transform.localPosition.y != 0 && i!=currentfocuesedButton)
            {
               // buttonboxes[i].transform.localPosition = new Vector3(buttonboxes[i].transform.localPosition.x, 0, buttonboxes[i].transform.localPosition.z);
                buttonboxes[i].transform.position = Vector3.MoveTowards(buttonboxes[i].transform.position, new Vector3(buttonboxes[i].transform.position.x, 6f, buttonboxes[i].transform.position.z),boxanimateSpeed*Time.deltaTime);
            }
        }

        RaycastHit hit;
        Ray ray = camerathis.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.collider.tag == "ButtonBlock" && Input.GetMouseButtonDown(0))
            {
                SelectLevel sl = hit.collider.GetComponent<SelectLevel>();
                if (sl.islocked == false)
                {
                    sl.SelectLevelOnClick();
                }
               
            }
        }

        /*
        if (Input.GetKeyDown(KeyCode.KeypadEnter)|| Input.GetKeyDown(KeyCode.Return))
        {
            SelectLevel sl = buttonboxes[currentfocuesedButton].GetComponent<SelectLevel>();
            if (sl.islocked == false)
            {
                sl.SelectLevelOnClick();
            }
        }*/
    }

    public void PreviousLevel()
    {
        if (currentfocuesedButton > 0)
        {
            currentfocuesedButton -= 1;
        }
    }
    public void NextLevel()
    {
        if (currentfocuesedButton < 50)
        {
            currentfocuesedButton += 1;
        }
    }

    void SelectLevel()
    {
        SelectLevel sl = buttonboxes[currentfocuesedButton].GetComponent<SelectLevel>();
        if (sl.islocked == false)
        {
            sl.SelectLevelOnClick();
        }
    }
}
