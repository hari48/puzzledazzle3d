using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LeaderBoardUIHandler : MonoBehaviour
{
    [SerializeField] Transform holder;
    [SerializeField] GameObject highscorePrefab;
    List<GameObject> highscorePrefabs = new List<GameObject>();
    public void BtnBeginFillLeaderboardLocal()
    {
        LeaderBoardManager.current.GetLeaderBoardData(Steamworks.ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobalAroundUser, 14);
    }
    public void BtnBeginFillLeaderboardGlobal()
    {
        LeaderBoardManager.current.GetLeaderBoardData(Steamworks.ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 14);
    }
    public void BtnBeginFillLeaderboardFriends()
    {
        LeaderBoardManager.current.GetLeaderBoardData(Steamworks.ELeaderboardDataRequest.k_ELeaderboardDataRequestFriends, 14);
    }
    public void FillLeaderboard(List<LeaderBoardManager.LeaderboardData> lDataset)
    {
        Debug.Log("filling leaderboard");
        foreach (GameObject g in highscorePrefabs)
        {
            Destroy(g);
        }
        foreach (LeaderBoardManager.LeaderboardData lD in lDataset)
        {
            GameObject g = Instantiate(highscorePrefab, holder);
            highscorePrefabs.Add(g);
            FillHighscorePrefab(g, lD);
        }
    }
    void FillHighscorePrefab(GameObject _prefab, LeaderBoardManager.LeaderboardData _lData)
    {
        _prefab.transform.Find("username").GetComponent<TMP_Text>().text = _lData.username;
        _prefab.transform.Find("score").GetComponent<TMP_Text>().text = _lData.score.ToString();
        _prefab.transform.Find("rank").GetComponent<TMP_Text>().text = _lData.rank.ToString();
    }
}
