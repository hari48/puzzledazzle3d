using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTimeProgressForSkyDome : MonoBehaviour
{
    [SerializeField]
    TOD_Sky tSky;
    [SerializeField]
    TOD_Time tTime;
    int x,y,z;

    public bool changetheskybox;
    public Material skyboxmat;

    public bool changeskyvalue;
    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(tSky.Cycle.Hour>=-0.1 && tSky.Cycle.Hour <= 0.1 && x==0)
        {
            x = 1;
            tTime.ProgressTime = false;
        }

        if(changetheskybox && y == 0)
        {
            y = 1;
            RenderSettings.skybox = skyboxmat;
        }

        if (changeskyvalue && z == 0)
        { z = 1;
            StartCoroutine(ChangeCloudCoverage());
        }
    }

    IEnumerator ChangeCloudCoverage()
    {
        int a = 12;
        float b = 1;
        while (a > 0)
        {
            a--;
            yield return new WaitForSeconds(1f);
            tSky.Clouds.Coverage = b;
            b -= 0.1f;
        }
       
    }
}
