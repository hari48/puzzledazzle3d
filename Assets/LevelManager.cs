using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class LevelManager : MonoBehaviour                             //    !! The colors Go in Order Of Blue>Yellow>Red>Green. !!
{
    public static LevelManager current;
    public int BlueCubesDestroyed = 0;
    public int YellowCubesDestroyed = 0;
    public int RedDestroyed = 0;
    public int GreenDestroyed = 0;
    public int Combocounter = 0;
    public int Combocounter2x = 0;
    public int Combocounter3x = 0;
    public int Combocounter4x = 0;
    public int MaxCombo = 0;
    public int HorizontalPowerUPUsed = 0;
    public int HorizontalPowerUpMaxRowsCleared = 0;
    public int VerticalPowerUPUsed = 0;
    public int VerticalPowerUpMaxRowsCleared = 0;
    public int blue6ComboCounter = 0;
    public int Yellow6ComboCounter = 0;
    public int Red6ComboCounter = 0;
    public int Green6ComboCounter = 0;
    public int Blue8ComboCounter = 0;
    public int Yellow8ComboCounter = 0;
    public int Red8ComboCounter = 0;
    public int Green8ComboCounter = 0;
    public int LevelCompleteTimer = 0;

    [SerializeField]
    TMP_Text levelObjectiveTxt;

    public int currentLevel = 0;
    int x = 0;
    public bool isGameOver = false;
    public bool isGameRealyOver = false;
    public int NumberOfColorsToSpawn = 2;
    public int NumberOfObjectives = 0;
    public float fallspeed;
    public int StartBlocksInGame;
    public int ChanceOfSpawningBlue;
    public int ChanceOfSpawningYellow;
    public int ChanceOfSpawningRed;
    public int ChanceOfSpawningGreen;
    public int ChanceOfSpawningSpecial;                   //30 is default go down for more spawn chance
    public int ChanceOfSpawningHorizontal;
    public int ChanceOfSpawningVertical;
    public int ChanceOfSpawning4Piece;
    public int ChanceOfSpawning2Piece;

    public bool[] BlueObjectiveSetter;
    public bool[] YellowObjectiveSetter;
    public bool[] RedObjectiveSetter;
    public bool[] GreenObjectiveSetter;
    [SerializeField]
    GameObject FireWorksParticle;

    public bool HasRecievedLoseReward = false;

    bool chkforwinscreenclick = true;
    float stepsbeforewinspeed = 1f;
    public int loseflag = 0;
    [Range(60, 1000)]
    public int[] LevelTimers;

    PieceControl controls;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        controls = new PieceControl();
        controls.UI.Back.performed += ctx => ClickBack();
        //test
        /* if (PlayerPrefs.GetInt("Testing1", 0) == 0)
         {
             PlayerPrefs.SetInt("MaxLevel", 50);
             PlayerPrefs.SetInt("PBombCount", 50);
             PlayerPrefs.SetInt("PSwapCount", 50);
             PlayerPrefs.SetInt("PSpecialAddCount", 50);
             PlayerPrefs.SetInt("Testing1", 1);
         }*/
    }
    private void OnEnable()
    {
        controls.UI.Enable();
    }
    private void OnDisable()
    {
        controls.UI.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        // AnalyticsManager.current.TriggerEventGame_Started();

        Application.targetFrameRate = PlayerPrefs.GetInt("FPS", -1);
    }

    void ClickBack()
    {
        GameObject btn = GameObject.FindGameObjectWithTag("BackButton");
        if (btn == null)
        {
            return;
        }
        Button btnb = btn.GetComponent<Button>();
        EventSystem nes = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        ExecuteEvents.Execute(btn, new BaseEventData(nes), ExecuteEvents.submitHandler);
    }

    // Update is called once per frame
    void Update()
    {


        if (SceneManager.GetActiveScene().buildIndex >= 2 && !isGameOver)
        {
            CheckResultCondition();
            CheckLoseCondition();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject btn = GameObject.FindGameObjectWithTag("BackButton");
            if (btn == null)
            {
                return;
            }
            Button btnb = btn.GetComponent<Button>();
            EventSystem nes = GameObject.Find("EventSystem").GetComponent<EventSystem>();
            ExecuteEvents.Execute(btn, new BaseEventData(nes), ExecuteEvents.submitHandler);
        }

        CheckForClickOnWin();
    }
    public void ResetValues()
    {
        x = 0;
        BlueCubesDestroyed = 0;
        YellowCubesDestroyed = 0;
        RedDestroyed = 0;
        GreenDestroyed = 0;
        Combocounter = 0;
        Combocounter2x = 0;
        Combocounter3x = 0;
        Combocounter4x = 0;
        MaxCombo = 0;
        HorizontalPowerUPUsed = 0;
        HorizontalPowerUpMaxRowsCleared = 0;
        VerticalPowerUPUsed = 0;
        VerticalPowerUpMaxRowsCleared = 0;
        blue6ComboCounter = 0;
        Yellow6ComboCounter = 0;
        Red6ComboCounter = 0;
        Green6ComboCounter = 0;
        Blue8ComboCounter = 0;
        Yellow8ComboCounter = 0;
        Red8ComboCounter = 0;
        Green8ComboCounter = 0;
        LevelCompleteTimer = 0;
        ChanceOfSpawningBlue = 10;
        ChanceOfSpawningYellow = 10;
        ChanceOfSpawningRed = 10;
        ChanceOfSpawningGreen = 10;
        ChanceOfSpawningHorizontal = 10;
        ChanceOfSpawningVertical = 10;
        ChanceOfSpawningSpecial = 30;
        ChanceOfSpawning4Piece = 10;
        ChanceOfSpawning2Piece = 10;
        BlueObjectiveSetter = new bool[] { false, false, false, false };
        YellowObjectiveSetter = new bool[] { false, false, false, false };
        RedObjectiveSetter = new bool[] { false, false, false, false };
        GreenObjectiveSetter = new bool[] { false, false, false, false };
        isGameOver = false;
        isGameRealyOver = false;
        HasRecievedLoseReward = false;
        loseflag = 0;
        chkforwinscreenclick = true;
        stepsbeforewinspeed = 1f;
    }
    public void LoadLevel(int level)
    {
        if (level == 0)
        {
            ResetValues();
            SceneManager.LoadScene(1);
            return;
        }
        if (level == -1)
        {
            ResetValues();
            currentLevel = level;
            DecideInGameValues();
            StartCoroutine(ShowObjectiveText());
            LevelCompleteTimer = 1000;
            if (SteamManager.s_EverInitialized == true)
            {
                AchievmentManager.current.UnlockAchievment(7);
            }
          
            SceneManager.LoadScene(Random.Range(2,52));
            return;
        }
        if (PlayerPrefs.GetInt("MaxLevel", 1) < level)
        {
            PlayerPrefs.SetInt("MaxLevel", level);
        }
        currentLevel = level;
        ResetValues();
        DecideInGameValues();
        StartCoroutine(ShowObjectiveText());
        SceneManager.LoadScene(level+1);
    }
    public void CallShowObjective()
    {
        StartCoroutine(ShowObjectiveText());
    }
    IEnumerator ShowObjectiveText()
    {/*
        if (PlayerPrefs.GetInt("TutorialPanel", 0) == 0 && currentLevel > 0)
        {
            yield break;
        }*/
        yield return new WaitForSecondsRealtime(0.5f);
        levelObjectiveTxt.gameObject.transform.parent.gameObject.SetActive(true);
        switch (currentLevel)                                 //Yellow=0,Red=1,green=2,blue=3,6block=4,horizontal=5,vertical=6 sprite id.
        {
            case -1:
                levelObjectiveTxt.text = "Earn Highest Score!";
                break;
            case 1:
                levelObjectiveTxt.text = "Clear 15<sprite=3>blocks and 10<sprite=0>blocks";
                break;
            case 2:
                levelObjectiveTxt.text = "Clear 20<sprite=3>blocks and 20<sprite=0>blocks";
                break;
            case 3:
                levelObjectiveTxt.text = "Clear 30<sprite=3>blocks and 30<sprite=0>blocks";
                break;
            case 4:
                levelObjectiveTxt.text = "Clear blocks 3 turns in a row";
                break;
            case 5:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 35<sprite=0>blocks";
                break;
            case 6:
                levelObjectiveTxt.text = "Clear 60 blocks and 2 blocks in a row";
                break;
            case 7:
                levelObjectiveTxt.text = "Clear 80 blocks and 3 blocks in a row";
                break;
            case 8:
                levelObjectiveTxt.text = "Clear 50<sprite=3>blocks and 3 blocks in a row";
                break;
            case 9:
                levelObjectiveTxt.text = "Clear using<sprite=5>powerup 3 times";
                break;
            case 10:
                levelObjectiveTxt.text = "Clear 30<sprite=3>blocks and 50<sprite=0>blocks";
                break;
            case 11:
                levelObjectiveTxt.text = "Clear 100 blocks";
                break;
            case 12:
                levelObjectiveTxt.text = "Clear a group of 6 blocks";
                break;
            case 13:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 40<sprite=0>blocks";
                break;
            case 14:
                levelObjectiveTxt.text = "Clear 40<sprite=0>blocks and Clear 2 Blocks in a row";
                break;
            case 15:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 30<sprite=0>blocks";
                break;
            case 16:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 50<sprite=0>blocks";
                break;
            case 17:
                levelObjectiveTxt.text = "Clear 3 Blocks in a row";
                break;
            case 18:
                levelObjectiveTxt.text = "Clear 60<sprite=3>blocks and 40<sprite=0>blocks";
                break;
            case 19:
                levelObjectiveTxt.text = "Clear 70 blocks";
                break;
            case 20:
                levelObjectiveTxt.text = "Clear 3 rows using<sprite=5>powerup";
                break;
            case 21:
                levelObjectiveTxt.text = "Clear 3 columns using<sprite=6>powerup";
                break;
            case 22:
                levelObjectiveTxt.text = "Clear 40<sprite=0>blocks and 30<sprite=1>blocks";
                break;
            case 23:
                levelObjectiveTxt.text = "Clear 20<sprite=3>blocks and 40<sprite=0>blocks and 50<sprite=1>blocks";
                break;
            case 24:
                levelObjectiveTxt.text = "Clear a group of 6 blocks once";
                break;
            case 25:
                levelObjectiveTxt.text = "Clear a group of 8 blocks once";
                break;
            case 26:
                levelObjectiveTxt.text = "Clear 3 blocks in a row";
                break;
            case 27:
                levelObjectiveTxt.text = "Clear a Group of 6 blocks twice";
                break;
            case 28:
                levelObjectiveTxt.text = "Clear 20<sprite=3>blocks and 30<sprite=0>blocks and 30<sprite=1>blocks";
                break;
            case 29:
                levelObjectiveTxt.text = "Clear 30<sprite=1>blocks and Clear 2 blocks in a row";
                break;
            case 30:
                levelObjectiveTxt.text = "Clear 40<sprite=0>blocks and 30<sprite=1>blocks";
                break;
            case 31:
                levelObjectiveTxt.text = "Clear 30<sprite=3>blocks and 40<sprite=1>blocks";
                break;
            case 32:
                levelObjectiveTxt.text = "Clear 35<sprite=0>blocks";
                break;
            case 33:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks, 30<sprite=0>blocks and 40<sprite=1>blocks";
                break;
            case 34:
                levelObjectiveTxt.text = "Clear 3 Blocks in a row";
                break;
            case 35:
                levelObjectiveTxt.text = "Clear 30<sprite=1>and clear 2 columns using<sprite=6>powerup";
                break;
            case 36:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks, 30<sprite=0>blocks and 2 rows using<sprite=5>powerup";
                break;
            case 37:
                levelObjectiveTxt.text = "Clear 30<sprite=0>Blocks and 40<sprite=1>Blocks";
                break;
            case 38:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 25<sprite=1>blocks";
                break;
            case 39:
                levelObjectiveTxt.text = "Clear a group of 8 blocks once";
                break;
            case 40:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 50<sprite=1>blocks";
                break;
            case 41:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 6 blocks twice";
                break;
            case 42:
                levelObjectiveTxt.text = "Clear 30<sprite=3>blocks, 40<sprite=0>blocks and 30<sprite=1>blocks";
                break;
            case 43:
                levelObjectiveTxt.text = "Clear 30<sprite=1>blocks and 6 blocks twice";
                break;
            case 44:
                levelObjectiveTxt.text = "Clear 30<sprite=1>blocks and clear 3 blocks in a row";
                break;
            case 45:
                levelObjectiveTxt.text = "Clear 35<sprite=3>blocks, 35<sprite=0>blocks and Clear 2 blocks in a row";
                break;
            case 46:
                levelObjectiveTxt.text = "Clear a group of 6 blocks 2 times";
                break;
            case 47:
                levelObjectiveTxt.text = "Clear using 2<sprite=6>powerups and Clear 2<sprite=5>powerups";
                break;
            case 48:
                levelObjectiveTxt.text = "Clear a group of 8 blocks once";
                break;
            case 49:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks and 50<sprite=0>blocks";
                break;
            case 50:
                levelObjectiveTxt.text = "Clear 40<sprite=3>blocks, 40<sprite=0>blocks and 40<sprite=1>blocks";
                break;
        }
    }  //Yellow=0,Red=1,green=2,blue=3,6block=4,horizontal=5,vertical=6 sprite id.
    void DecideInGameValues()
    {
        switch (currentLevel)
        {
            case -1:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 0;
                fallspeed = 0.8f;
                StartBlocksInGame = 0;
                break;
            case 1:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.8f;
                StartBlocksInGame = 0;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 2:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.8f;
                StartBlocksInGame = 0;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 3:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.8f;
                StartBlocksInGame = 0;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 4:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.8f;
                StartBlocksInGame = 0;
                break;
            case 5:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.78f;
                StartBlocksInGame = 0;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 6:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.78f;
                StartBlocksInGame = 10;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[0] = true;
                break;
            case 7:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.8f;
                StartBlocksInGame = 15;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[0] = true;
                break;
            case 8:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.8f;
                StartBlocksInGame = 0;
                BlueObjectiveSetter[0] = true;
                break;
            case 9:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.8f;
                StartBlocksInGame = 10;
                ChanceOfSpawningSpecial = 10;
                ChanceOfSpawningHorizontal = 40;
                break;
            case 10:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.8f;
                StartBlocksInGame = 14;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 11:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[0] = true;
                break;
            case 12:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                break;
            case 13:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 24;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 14:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 22;
                YellowObjectiveSetter[0] = true;
                break;
            case 15:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 17;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 16:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 15;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 17:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 20;
                break;
            case 18:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 16;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 19:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[0] = true;
                break;
            case 20:
                NumberOfColorsToSpawn = 2;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 30;
                ChanceOfSpawningSpecial = 10;
                ChanceOfSpawningHorizontal = 40;
                break;
            case 21:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 12;
                ChanceOfSpawningSpecial = 10;
                ChanceOfSpawningVertical = 40;
                break;
            case 22:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 16;
                ChanceOfSpawningSpecial = 10;
                ChanceOfSpawningBlue = 5;
                YellowObjectiveSetter[0] = true;
                RedObjectiveSetter[1] = true;
                break;
            case 23:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                RedObjectiveSetter[2] = true;
                break;
            case 24:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 18;
                break;
            case 25:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 0;
                ChanceOfSpawning4Piece = 20;
                break;
            case 26:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 10;
                break;
            case 27:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 12;
                break;
            case 28:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 22;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                RedObjectiveSetter[2] = true;
                break;
            case 29:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                RedObjectiveSetter[0] = true;
                break;
            case 30:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 12;
                YellowObjectiveSetter[0] = true;
                RedObjectiveSetter[1] = true;
                break;
            case 31:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 18;
                BlueObjectiveSetter[0] = true;
                RedObjectiveSetter[1] = true;
                break;
            case 32:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 20;
                YellowObjectiveSetter[0] = true;
                break;
            case 33:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                RedObjectiveSetter[2] = true;
                break;
            case 34:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 5;
                break;
            case 35:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 18;
                RedObjectiveSetter[0] = true;
                ChanceOfSpawningSpecial = 10;
                ChanceOfSpawningVertical = 40;
                break;
            case 36:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 14;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                ChanceOfSpawningSpecial = 10;
                ChanceOfSpawningHorizontal = 40;
                break;
            case 37:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 17;
                YellowObjectiveSetter[0] = true;
                RedObjectiveSetter[1] = true;
                break;
            case 38:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 20;
                BlueObjectiveSetter[0] = true;
                GreenObjectiveSetter[1] = true;
                break;
            case 39:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.64f;
                StartBlocksInGame = 15;
                break;
            case 40:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 16;
                BlueObjectiveSetter[0] = true;
                RedObjectiveSetter[1] = true;
                break;
            case 41:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 15;
                ChanceOfSpawning4Piece = 20;
                BlueObjectiveSetter[0] = true;
                break;
            case 42:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 24;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                RedObjectiveSetter[2] = true;
                break;
            case 43:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 20;
                ChanceOfSpawning4Piece = 20;
                ChanceOfSpawningSpecial = 10;
                RedObjectiveSetter[0] = true;
                break;
            case 44:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 16;
                RedObjectiveSetter[0] = true;
                break;
            case 45:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 20;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 46:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.72f;
                StartBlocksInGame = 10;
                break;
            case 47:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 21;
                ChanceOfSpawningSpecial = 10;
                break;
            case 48:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 1;
                fallspeed = 0.64f;
                StartBlocksInGame = 20;
                break;
            case 49:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 2;
                fallspeed = 0.72f;
                StartBlocksInGame = 25;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                break;
            case 50:
                NumberOfColorsToSpawn = 3;
                NumberOfObjectives = 3;
                fallspeed = 0.72f;
                StartBlocksInGame = 25;
                BlueObjectiveSetter[0] = true;
                YellowObjectiveSetter[1] = true;
                RedObjectiveSetter[2] = true;
                break;
        }

        if (currentLevel > 0)
        {
            NumberOfObjectives = NumberOfObjectives + 1;
            LevelCompleteTimer = LevelTimers[currentLevel - 1];
        }

        SetNumberOfColorsToSpawn();
    }
    void SetNumberOfColorsToSpawn()
    {
        if (NumberOfColorsToSpawn == 1)
        {
            ChanceOfSpawningYellow = 0;
            ChanceOfSpawningRed = 0;
            ChanceOfSpawningGreen = 0;
        }
        else if (NumberOfColorsToSpawn == 2)
        {
            ChanceOfSpawningRed = 0;
            ChanceOfSpawningGreen = 0;
        }
        else if (NumberOfColorsToSpawn == 3)
        {
            ChanceOfSpawningGreen = 0;
        }
    }

    public void ResumeGame()
    {
        GameManager.instance.ResumeGameWithoutUI();
    }
    void RequestReview()
    {
        if (PlayerPrefs.GetInt("ReviewCounter", 0) == 0)
        {
           // ReviewHandler.current.CallNativeReview();
            PlayerPrefs.SetInt("ReviewCounter", 1);
        }
    }
    void CheckResultCondition()
    {
        switch (currentLevel)
        {
            case -1: return;

            case 1:
                if (BlueCubesDestroyed >= 15 && YellowCubesDestroyed >= 10 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(0);
                    }
                     
                    StepsBeforeResult();
                    Invoke("RequestReview", 3);
                    
                }
                break;
            case 2:
                if (BlueCubesDestroyed >= 20 && YellowCubesDestroyed >= 20 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 3:
                if (BlueCubesDestroyed >= 30 && YellowCubesDestroyed >= 30 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 4:
                if (MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 5:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 35 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(1);
                    }
                    StepsBeforeResult();
                }
                break;
            case 6:
                if (BlueCubesDestroyed + YellowCubesDestroyed > 60 && MaxCombo >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 7:
                if (BlueCubesDestroyed + YellowCubesDestroyed > 80 && MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 8:
                if (BlueCubesDestroyed >= 50 && MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 9:
                if (HorizontalPowerUPUsed >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 10:
                if (BlueCubesDestroyed >= 30 && YellowCubesDestroyed >= 50 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 11:
                if (BlueCubesDestroyed + YellowCubesDestroyed >= 100 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 12:
                if (blue6ComboCounter + Yellow6ComboCounter >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 13:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 40 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 14:
                if (YellowCubesDestroyed >= 40 && Combocounter2x >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 15:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 30 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 16:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 50 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 17:
                if (MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 18:
                if (BlueCubesDestroyed >= 60 && YellowCubesDestroyed >= 40 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 19:
                if (BlueCubesDestroyed + YellowCubesDestroyed >= 70 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 20:
                if (HorizontalPowerUPUsed >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(2);
                    }
                    StepsBeforeResult();
                }
                break;
            case 21:
                if (VerticalPowerUPUsed >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 22:
                if (YellowCubesDestroyed >= 40 && RedDestroyed >= 30 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 23:
                if (BlueCubesDestroyed >= 20 && YellowCubesDestroyed >= 40 && RedDestroyed >= 50 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 24:
                if (blue6ComboCounter + Yellow6ComboCounter + Red6ComboCounter >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 25:
                if (Blue8ComboCounter + Yellow8ComboCounter + Red8ComboCounter >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(3);
                    }
                    StepsBeforeResult();
                }
                break;
            case 26:
                if (MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 27:
                if (blue6ComboCounter + Yellow6ComboCounter + Red6ComboCounter >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 28:
                if (BlueCubesDestroyed >= 20 && YellowCubesDestroyed >= 30 && RedDestroyed >= 30 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 29:
                if (RedDestroyed >= 30 && MaxCombo >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 30:
                if (YellowCubesDestroyed >= 40 && RedDestroyed >= 30 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 31:
                if (BlueCubesDestroyed >= 30 && RedDestroyed >= 40 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 32:
                if (YellowCubesDestroyed >= 35 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 33:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 30 && RedDestroyed >= 40 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 34:
                if (MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 35:
                if (RedDestroyed >= 30 && VerticalPowerUPUsed >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(4);
                    }
                    StepsBeforeResult();
                }
                break;
            case 36:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 40 && HorizontalPowerUPUsed >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 37:
                if (YellowCubesDestroyed >= 30 && RedDestroyed >= 40 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 38:
                if (BlueCubesDestroyed >= 40 && RedDestroyed >= 25 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 39:
                if (Blue8ComboCounter + Yellow8ComboCounter + Red8ComboCounter >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 40:
                if (BlueCubesDestroyed >= 40 && RedDestroyed >= 50 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 41:
                if (BlueCubesDestroyed >= 40 && (blue6ComboCounter + Yellow6ComboCounter + Red6ComboCounter + Green6ComboCounter) >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 42:
                if (BlueCubesDestroyed >= 30 && YellowCubesDestroyed >= 40 && RedDestroyed >= 30 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 43:
                if (RedDestroyed >= 30 && blue6ComboCounter + Yellow6ComboCounter + Red6ComboCounter + Green6ComboCounter >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 44:
                if (RedDestroyed >= 30 && MaxCombo >= 3 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 45:
                if (BlueCubesDestroyed >= 35 && YellowCubesDestroyed >= 35 && Combocounter2x >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(5);
                    }
                    StepsBeforeResult();
                }
                break;
            case 46:
                if (blue6ComboCounter + Yellow6ComboCounter + Red6ComboCounter + Green6ComboCounter >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 47:
                if (HorizontalPowerUPUsed >= 2 && VerticalPowerUPUsed >= 2 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 48:
                if (Blue8ComboCounter + Yellow8ComboCounter + Red8ComboCounter + Green8ComboCounter >= 1 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 49:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 50 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    StepsBeforeResult();
                }
                break;
            case 50:
                if (BlueCubesDestroyed >= 40 && YellowCubesDestroyed >= 40 && RedDestroyed >= 40 && x == 0)
                {
                    x = 1;
                    isGameOver = true;
                    if (SteamManager.s_EverInitialized == true)
                    {
                        AchievmentManager.current.UnlockAchievment(6);
                    }
                    StepsBeforeResult();
                }
                break;
        }
    }
    void StepsBeforeResult()
    {
        isGameRealyOver = true;
        StartCoroutine(StepsBeforeResultWait());
    }

    void CheckForClickOnWin()
    {
        if (!isGameOver)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) && chkforwinscreenclick == false)
        {
            stepsbeforewinspeed = 0f;
            chkforwinscreenclick = true;
        }

    }

    IEnumerator StepsBeforeResultWait()
    {
       // AnalyticsManager.current.TriggerEventArcadeMode_LevelWon(currentLevel);
        ScoreManager.instance.SaveScore();
        AudioHandler.Instance.FadeAudio();
        UIHandler.instance.ControlPauseAndPowerupsUI(false);
        //GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverWinClip;
        GameObject.Find("falling sound").GetComponent<AudioSource>().Play();

        SettleBlocks();

        yield return new WaitForSecondsRealtime(stepsbeforewinspeed);
        chkforwinscreenclick = false;
        GameObject fw = Instantiate(FireWorksParticle, new Vector3(5, 8, 0), Quaternion.identity);
        Transform[] fwt = fw.GetComponentsInChildren<Transform>(true);
        for (int i = 1; i < fwt.Length; i++)
        {
            fwt[i].transform.position += new Vector3(Random.Range(-3, 4), Random.Range(-4, 5), 0);
            fwt[i].gameObject.SetActive(true);
            yield return new WaitForSecondsRealtime(stepsbeforewinspeed / 5f);
        }
        GridSystem.instance.GameWinClear();
        yield return new WaitForSecondsRealtime(stepsbeforewinspeed);
        Destroy(fw);
        //PlayfabManager.current.SendLeaderboard(PlayerPrefs.GetInt("hs"), "Sandbox Mode");
        Invoke("ShowResult", stepsbeforewinspeed * 2);
    }
    void ShowResult()
    {
        if (PlayerPrefs.GetInt("MaxLevel", 0) < currentLevel + 1)
        {
            PlayerPrefs.SetInt("MaxLevel", currentLevel + 1);
        }


        if (currentLevel < 50)
        {
            UIHandler.instance.ShowResult(1);
        }
        else
        {
            UIHandler.instance.ShowResult(4);
        }

    }

    public void SetGameplayUI(bool value)
    {
        UIHandler.instance.SetGameplayUI(value);
        GameManager.instance.currentPiece.SetActive(true);
    }

    void CheckLoseCondition()
    {
        if (LevelCompleteTimer <= 0 && loseflag == 0)
        {
            StartCoroutine(LoseConditions());

        }
    }
    IEnumerator LoseConditions()
    {/*
        loseflag = 1;
        AudioHandler.Instance.FadeAudio();
        UIHandler.instance.ShowTimesUPPopup();
        isGameOver = true;
        GameManager.instance.PauseGameWithoutUI();
        UIHandler.instance.ControlPauseAndPowerupsUI(false);
        yield return new WaitForSecondsRealtime(2f);

        if (GoogleAdsManager.current.HasRewardedAdReady == false && HasRecievedLoseReward == false)
        {
            isGameRealyOver = true;
            ScoreManager.instance.SaveScore();
            AudioHandler.Instance.FadeAudio();
            UIHandler.instance.ControlPauseAndPowerupsUI(false);
            GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
            GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            yield return new WaitForSecondsRealtime(2f);
            ShowResult2();
        }
        else if (GoogleAdsManager.current.HasRewardedAdReady == true && HasRecievedLoseReward == false)
        {
            AudioHandler.Instance.FadeAudio();
            GameManager.instance.PauseGameWithoutUI();
            UIHandler.instance.ShowLosePopupTimer();

        }
        else if (GoogleAdsManager.current.HasRewardedAdReady == true && HasRecievedLoseReward == true)
        {
            isGameRealyOver = true;
            ScoreManager.instance.SaveScore();
            AudioHandler.Instance.FadeAudio();
            UIHandler.instance.ControlPauseAndPowerupsUI(false);
            GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
            GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            yield return new WaitForSecondsRealtime(2f);
            ShowResult2();
        }*/
        loseflag = 1;
        isGameOver = true;
        isGameRealyOver = true;
        ScoreManager.instance.SaveScore();
        AudioHandler.Instance.FadeAudio();
        UIHandler.instance.ControlPauseAndPowerupsUI(false);
        GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
        GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
        yield return new WaitForSecondsRealtime(2f);
        ShowResult2();
    }
    void ShowResult2()
    {
        UIHandler.instance.ShowResult(3);
    }

    void SettleBlocks()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 16; j++)
            {

                if (GridSystem.grid[i, j] != null)
                {
                    if (j != 0)
                    {
                        for (int k = j - 1; k >= 0; k--)
                        {
                            if (GridSystem.grid[i, k] == null)
                            {
                                Transform go = GridSystem.grid[i, k + 1].GetComponent<Transform>();
                                go.position = new Vector3(i, k, 0);
                                GridSystem.grid[i, k + 1] = null;
                                GridSystem.grid[i, k] = go;
                            }
                        }
                    }
                }
            }
        }
    }
}
