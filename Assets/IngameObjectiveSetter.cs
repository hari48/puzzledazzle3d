using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IngameObjectiveSetter : MonoBehaviour
{
    public static IngameObjectiveSetter current;

    [SerializeField]
    TMP_Text slot1;
    [SerializeField]
    TMP_Text slot2;
    [SerializeField]
    TMP_Text slot3;
    [SerializeField]
    TMP_Text slot4;
    [SerializeField]
    GameObject ObjectiveCompleteImage1;
    [SerializeField]
    GameObject ObjectiveCompleteImage2;
    [SerializeField]
    GameObject ObjectiveCompleteImage3;
    [SerializeField]
    GameObject ObjectiveCompleteImage4;

    [SerializeField]
    CheckMarkObjectives WinScreenCMO;
    [SerializeField]
    CheckMarkObjectives LoseScreenCMO;

    int BlueLateCount;
    int YellowLateCount;
    int RedLateCount;
    int GreenLateCount;
    bool isbluecounting;
    bool isyellowcounting;
    bool isredcounting;
    bool isgreencounting;

    public bool Objective1;
    public bool Objective2;
    public bool Objective3;
    public bool Objective4;

    int firsttime = 0;


    public GameObject obj1box;
    public GameObject obj2box;
    public GameObject obj3box;
    public GameObject obj4box;

    [SerializeField]
    GameObject Endlessmodemanager;
    [SerializeField]
    LevelCompleteTimer timercountdown;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        DecideSlots();
        BlueLateCount = LevelManager.current.BlueCubesDestroyed;
        YellowLateCount = LevelManager.current.YellowCubesDestroyed;
        RedLateCount = LevelManager.current.RedDestroyed;
        GreenLateCount = LevelManager.current.GreenDestroyed;
        Objective1 = false;
        Objective2 = false;
        Objective3 = false;
        Objective4 = false;
        firsttime = 0;
        if (LevelManager.current.currentLevel == -1)
        {
            timercountdown.enabled = false;
            Endlessmodemanager.SetActive(true);
        }
    }
    void DecideSlots()
    {
        switch (LevelManager.current.NumberOfObjectives)
        {
            case 1:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                slot2.gameObject.transform.parent.gameObject.SetActive(false);
                slot3.gameObject.transform.parent.gameObject.SetActive(false);
                slot4.gameObject.transform.parent.gameObject.SetActive(false);

                obj1box.GetComponent<RectTransform>().position = obj3box.GetComponent<RectTransform>().position;
                break;
            case 2:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                slot2.gameObject.transform.parent.gameObject.SetActive(true);
                slot3.gameObject.transform.parent.gameObject.SetActive(false);
                slot4.gameObject.transform.parent.gameObject.SetActive(false);

                obj1box.GetComponent<RectTransform>().position = obj3box.GetComponent<RectTransform>().position;
                obj2box.GetComponent<RectTransform>().position = obj4box.GetComponent<RectTransform>().position;
                break;
            case 3:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                slot2.gameObject.transform.parent.gameObject.SetActive(true);
                slot3.gameObject.transform.parent.gameObject.SetActive(true);
                slot4.gameObject.transform.parent.gameObject.SetActive(false);
                break;
            case 4:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                slot2.gameObject.transform.parent.gameObject.SetActive(true);
                slot3.gameObject.transform.parent.gameObject.SetActive(true);
                slot4.gameObject.transform.parent.gameObject.SetActive(true);
                break;
        }
    }
    IEnumerator BlueLateCountWait()
    {
        isbluecounting = true;
        yield return new WaitForSeconds(0.5f);
        while (BlueLateCount != LevelManager.current.BlueCubesDestroyed)
        {
            BlueLateCount++;
            yield return new WaitForSeconds(0.3f);
        }
        isbluecounting = false;
    }
    IEnumerator YellowLateCountWait()
    {
        isyellowcounting = true;
        yield return new WaitForSeconds(0.5f);
        while (YellowLateCount != LevelManager.current.YellowCubesDestroyed)
        {
            YellowLateCount++;
            yield return new WaitForSeconds(0.3f);
        }
        isyellowcounting = false;
    }
    IEnumerator RedLateCountWait()
    {
        isredcounting = true;
        yield return new WaitForSeconds(0.5f);
        while (RedLateCount != LevelManager.current.RedDestroyed)
        {
            RedLateCount++;
            yield return new WaitForSeconds(0.3f);
        }
        isredcounting = false;
    }
    IEnumerator GreenLateCountWait()
    {
        isgreencounting = true;
        yield return new WaitForSeconds(0.5f);
        while (GreenLateCount != LevelManager.current.GreenDestroyed)
        {
            GreenLateCount++;
            yield return new WaitForSeconds(0.3f);
        }
        isgreencounting = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (BlueLateCount != LevelManager.current.BlueCubesDestroyed && !isbluecounting)
        {
            StartCoroutine(BlueLateCountWait());
        }

        if (YellowLateCount != LevelManager.current.YellowCubesDestroyed && !isyellowcounting)
        {
            StartCoroutine(YellowLateCountWait());
        }

        if (RedLateCount != LevelManager.current.RedDestroyed && !isredcounting)
        {
            StartCoroutine(RedLateCountWait());
        }

        if (GreenLateCount != LevelManager.current.GreenDestroyed && !isgreencounting)
        {
            StartCoroutine(GreenLateCountWait());
        }


        switch (LevelManager.current.currentLevel)             //Yellow=0,Red=1,green=2,blue=3,6block=4,horizontal=5,vertical=6,time=7 sprite id.
        {
            case 1:
                {
                    int obj1 = 15 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 10 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 2:
                {
                    int obj1 = 20 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 20 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 3:
                {
                    int obj1 = 30 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 30 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 4:
                {
                    int obj1 = LevelManager.current.MaxCombo;
                    if (obj1 >= 3)
                    {
                        obj1 = 3;
                        Objective1 = true;
                    }
                    slot1.text = "Combo count: " + (obj1).ToString() + "/3";
                }
                break;
            case 5:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 35 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 6:
                {
                    int obj1 = 60 - (BlueLateCount + YellowLateCount);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "Blocks: " + (obj1).ToString();
                    int obj2 = LevelManager.current.MaxCombo;
                    if (obj2 >= 2)
                    {
                        obj2 = 2;
                        Objective2 = true;
                    }
                    slot2.text = "In a row: " + (obj2).ToString() + "/2";
                }
                break;
            case 7:
                {
                    int obj1 = 80 - (BlueLateCount + YellowLateCount);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "Blocks: " + (obj1).ToString();
                    int obj2 = LevelManager.current.MaxCombo;
                    if (obj2 >= 3)
                    {
                        obj2 = 3;
                        Objective2 = true;
                    }
                    slot2.text = "In a row: " + (obj2).ToString() + "/3";
                }
                break;
            case 8:
                {
                    int obj1 = 50 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = LevelManager.current.MaxCombo;
                    if (obj2 >= 3)
                    {
                        obj2 = 3;
                        Objective2 = true;
                    }
                    slot2.text = "In a row: " + (obj2).ToString() + "/3";
                }
                break;
            case 9:
                {
                    int obj1 = 3 - LevelManager.current.HorizontalPowerUPUsed;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=5>: " + (obj1).ToString();
                }
                break;
            case 10:
                {
                    int obj1 = 30 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 50 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 11:
                {
                    int obj1 = 100 - (BlueLateCount + YellowLateCount);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "blocks: " + (obj1).ToString();
                }
                break;
            case 12:
                {
                    int obj1 = 1 - (LevelManager.current.blue6ComboCounter + LevelManager.current.Yellow6ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=4>clear: " + (obj1).ToString();
                }
                break;
            case 13:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 14:
                {
                    int obj1 = 40 - YellowLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=0>: " + (obj1).ToString();
                    int obj2 = 2 - LevelManager.current.MaxCombo;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "In a row: " + (obj2).ToString();
                }
                break;
            case 15:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 30 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 16:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 50 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 17:
                {
                    int obj1 = LevelManager.current.MaxCombo;
                    if (obj1 >= 3)
                    {
                        obj1 = 3;
                        Objective1 = true;
                    }
                    slot1.text = "In a row: " + (obj1).ToString() + "/3";
                }
                break;
            case 18:
                {
                    int obj1 = 60 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 19:
                {
                    int obj1 = 70 - (BlueLateCount + YellowLateCount);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "Blocks: " + (obj1).ToString();
                }
                break;
            case 20:
                {
                    int obj1 = 3 - LevelManager.current.HorizontalPowerUPUsed;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=5>rows: " + (obj1).ToString();
                }
                break;
            case 21:
                {
                    int obj1 = 3 - LevelManager.current.VerticalPowerUPUsed;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=6>columns: " + (obj1).ToString();
                }
                break;
            case 22:
                {
                    int obj1 = 40 - YellowLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=0>: " + (obj1).ToString();
                    int obj2 = 30 - RedLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=1>: " + (obj2).ToString();
                }
                break;
            case 23:
                {
                    int obj1 = 20 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 50 - RedLateCount;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "<sprite=1>: " + (obj3).ToString();
                }
                break;
            case 24:
                {
                    int obj1 = 1 - (LevelManager.current.blue6ComboCounter + LevelManager.current.Yellow6ComboCounter + LevelManager.current.Red6ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "6 block clear: " + (obj1).ToString();
                }
                break;
            case 25:
                {
                    int obj1 = 1 - (LevelManager.current.Blue8ComboCounter + LevelManager.current.Yellow8ComboCounter + LevelManager.current.Red8ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "8 block clear: " + (obj1).ToString();
                }
                break;
            case 26:
                {
                    int obj1 = 3 - LevelManager.current.MaxCombo;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "In a row: " + (obj1).ToString();
                }
                break;
            case 27:
                {
                    int obj1 = 2 - (LevelManager.current.blue6ComboCounter + LevelManager.current.Yellow6ComboCounter + LevelManager.current.Red6ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "6 block clear: " + (obj1).ToString();
                }
                break;
            case 28:
                {
                    int obj1 = 20 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 30 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 30 - RedLateCount;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "<sprite=1>: " + (obj3).ToString();
                }
                break;
            case 29:
                {
                    int obj1 = 30 - RedLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=1>: " + (obj1).ToString();
                    int obj2 = 2 - LevelManager.current.MaxCombo;
                    if (obj2 >= 2)
                    {
                        obj2 = 2;
                        Objective2 = true;
                    }
                    slot2.text = "In a row: " + (obj2).ToString();
                }
                break;
            case 30:
                {
                    int obj1 = 40 - YellowLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=0>: " + (obj1).ToString();
                    int obj2 = 30 - RedLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=1>: " + (obj2).ToString();
                }
                break;
            case 31:
                {
                    int obj1 = 30 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - RedLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=1>: " + (obj2).ToString();
                }
                break;
            case 32:
                {
                    int obj1 = 35 - YellowLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=0>: " + (obj1).ToString();
                }
                break;
            case 33:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 30 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 40 - RedLateCount;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "<sprite=1>: " + (obj3).ToString();
                }
                break;
            case 34:
                {
                    int obj1 = 3 - LevelManager.current.MaxCombo;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "In a row: " + (obj1).ToString();
                }
                break;
            case 35:
                {
                    int obj1 = 30 - RedLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=1>: " + (obj1).ToString();
                    int obj2 = 2 - LevelManager.current.VerticalPowerUPUsed;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=6>: " + (obj2).ToString();
                }
                break;
            case 36:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 2 - LevelManager.current.HorizontalPowerUPUsed;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "<sprite=5>rows cleared: " + (obj3).ToString();
                }
                break;
            case 37:
                {
                    int obj1 = 30 - YellowLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=0>: " + (obj1).ToString();
                    int obj2 = 40 - RedLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=1>: " + (obj2).ToString();
                }
                break;
            case 38:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 25 - RedLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=1>: " + (obj2).ToString();
                }
                break;
            case 39:
                {
                    int obj1 = 1 - (LevelManager.current.Blue8ComboCounter + LevelManager.current.Yellow8ComboCounter + LevelManager.current.Red8ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "8 block clear: " + (obj1).ToString();
                }
                break;
            case 40:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 50 - RedLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=1>: " + (obj2).ToString();
                }
                break;
            case 41:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 2 - (LevelManager.current.blue6ComboCounter + LevelManager.current.Yellow6ComboCounter + LevelManager.current.Red6ComboCounter + LevelManager.current.Green6ComboCounter);
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=4>counter: " + (obj2).ToString();
                }
                break;
            case 42:
                {
                    int obj1 = 30 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 30 - RedLateCount;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "<sprite=1>: " + (obj3).ToString();
                }
                break;
            case 43:
                {
                    int obj1 = 30 - RedLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=1>: " + (obj1).ToString();
                    int obj2 = 2 - (LevelManager.current.blue6ComboCounter + LevelManager.current.Yellow6ComboCounter + LevelManager.current.Red6ComboCounter + LevelManager.current.Green6ComboCounter);
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=4>cleared: " + (obj2).ToString();
                }
                break;
            case 44:
                {
                    int obj1 = 30 - RedLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=1>: " + (obj1).ToString();
                    int obj2 = LevelManager.current.MaxCombo;
                    if (obj2 >= 3)
                    {
                        obj2 = 3;
                        Objective2 = true;
                    }
                    slot2.text = "combo counter: " + (obj2).ToString() + "/3";
                }
                break;
            case 45:
                {
                    int obj1 = 35 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 35 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 2 - LevelManager.current.MaxCombo;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "In a row: " + (obj3).ToString();
                }
                break;
            case 46:
                {
                    int obj1 = 2 - (LevelManager.current.blue6ComboCounter + LevelManager.current.Yellow6ComboCounter + LevelManager.current.Red6ComboCounter + LevelManager.current.Green6ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=4>counter: " + (obj1).ToString();
                }
                break;
            case 47:
                {
                    int obj1 = 2 - LevelManager.current.HorizontalPowerUPUsed;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=5>count: " + (obj1).ToString();
                    int obj2 = 2 - LevelManager.current.VerticalPowerUPUsed;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=6>count: " + (obj2).ToString();
                }
                break;
            case 48:
                {
                    int obj1 = 1 - (LevelManager.current.Blue8ComboCounter + LevelManager.current.Yellow8ComboCounter + LevelManager.current.Red8ComboCounter + LevelManager.current.Green8ComboCounter);
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "8 block clear: " + (obj1).ToString();
                }
                break;
            case 49:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 50 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                }
                break;
            case 50:
                {
                    int obj1 = 40 - BlueLateCount;
                    if (obj1 <= 0)
                    {
                        obj1 = 0;
                        Objective1 = true;
                    }
                    slot1.text = "<sprite=3>: " + (obj1).ToString();
                    int obj2 = 40 - YellowLateCount;
                    if (obj2 <= 0)
                    {
                        obj2 = 0;
                        Objective2 = true;
                    }
                    slot2.text = "<sprite=0>: " + (obj2).ToString();
                    int obj3 = 40 - RedLateCount;
                    if (obj3 <= 0)
                    {
                        obj3 = 0;
                        Objective3 = true;
                    }
                    slot3.text = "<sprite=1>: " + (obj3).ToString();
                }
                break;

        }
        if (Objective1 == true)
        {
            slot1.gameObject.SetActive(false);
            ObjectiveCompleteImage1.SetActive(true);
        }
        if (Objective2 == true)
        {
            slot2.gameObject.SetActive(false);
            ObjectiveCompleteImage2.SetActive(true);
        }
        if (Objective3 == true)
        {
            slot3.gameObject.SetActive(false);
            ObjectiveCompleteImage3.SetActive(true);
        }
        if (Objective4 == true)
        {
            slot4.gameObject.SetActive(false);
            ObjectiveCompleteImage4.SetActive(true);
        }


        switch (LevelManager.current.NumberOfObjectives)
        {
            case 2:
                int objtimer1 = LevelManager.current.LevelCompleteTimer;
                if (objtimer1 <= 0)
                {
                    objtimer1 = 0;
                }
                slot2.text = "<sprite=7>: " + ConvertSecondsToTimeformat(objtimer1);
                break;
            case 3:
                int objtimer2 = LevelManager.current.LevelCompleteTimer;
                if (objtimer2 <= 0)
                {
                    objtimer2 = 0;
                }
                slot3.text = "<sprite=7>: " + ConvertSecondsToTimeformat(objtimer2);
                break;
            case 4:
                int objtimer3 = LevelManager.current.LevelCompleteTimer;
                if (objtimer3 <= 0)
                {
                    objtimer3 = 0;
                }
                slot4.text = "<sprite=7>: " + ConvertSecondsToTimeformat(objtimer3);
                break;
        }


        if (firsttime == 0)
        {
            firsttime = 1;
            /*
            WinScreenCMO.slot1.text = slot1.text;
            WinScreenCMO.slot2.text = slot2.text;
            WinScreenCMO.slot3.text = slot3.text;
            WinScreenCMO.slot4.text = slot4.text;

            LoseScreenCMO.slot1.text = slot1.text;
            LoseScreenCMO.slot2.text = slot2.text;
            LoseScreenCMO.slot3.text = slot3.text;
            LoseScreenCMO.slot4.text = slot4.text;*/
        }
    } //Yellow=0,Red=1,green=2,blue=3,6block=4,horizontal=5,vertical=6,time=7 sprite id.


    string ConvertSecondsToTimeformat(int seconds)
    {
        int sec = seconds % 60;
        int min = seconds / 60;
        return (min + ":" + sec);
    }

    public void ShakeObjective(int objvalue)
    {
        switch (objvalue)
        {
            case 1:
                StartCoroutine(ShakeObj(obj1box));
                break;
            case 2:
                StartCoroutine(ShakeObj(obj2box));
                break;
            case 3:
                StartCoroutine(ShakeObj(obj3box));
                break;
            case 4:
                StartCoroutine(ShakeObj(obj4box));
                break;
            default:
                break;
        }
    }
    IEnumerator ShakeObj(GameObject obj)
    {
        obj.transform.localScale = new Vector3(2, 0.6f, 0.7f);
        yield return new WaitForSeconds(0.1f);
        obj.transform.localScale = new Vector3(1.765754f, 0.4370281f, 0.5841362f);
    }
}
