using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.FeedbacksForThirdParty;

public class FeedBackManager : MonoBehaviour
{
    public static FeedBackManager current;

    [SerializeField]
    MMFeedbackCinemachineImpulse mmfc;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(this);
        }
    }
    public void ShakeCamera()
    {
        mmfc.Play(new Vector3(1, 1, 1), 1);
    }
}
