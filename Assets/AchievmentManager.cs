using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class AchievmentManager : MonoBehaviour
{
    public static AchievmentManager current;

    public string[] Achievmentids;
    bool AchievmentUnlocked;

    static string steamid= "1588790";

    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);
    }

    public void UnlockAchievment(int id)
    {
        SteamUserStats.GetAchievement(steamid, out AchievmentUnlocked);
        if (!AchievmentUnlocked)
        {
            SteamUserStats.SetAchievement(Achievmentids[id]);
            SteamUserStats.StoreStats();
        }
    }
}
