using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CheckMarkObjectives : MonoBehaviour
{
    public TMP_Text slot1;
    public TMP_Text slot2;
    public TMP_Text slot3;
    public TMP_Text slot4;
    [SerializeField]
    GameObject ObjectiveCompleteImage1;
    [SerializeField]
    GameObject ObjectiveCompleteImage2;
    [SerializeField]
    GameObject ObjectiveCompleteImage3;
    [SerializeField]
    GameObject ObjectiveCompleteImage4;
    [SerializeField]
    GameObject ObjectiveNotCompleteImage1;
    [SerializeField]
    GameObject ObjectiveNotCompleteImage2;
    [SerializeField]
    GameObject ObjectiveNotCompleteImage3;
    [SerializeField]
    GameObject ObjectiveNotCompleteImage4;

    float waittime = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartcheckMarkAnim());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator StartcheckMarkAnim()
    {
        switch (LevelManager.current.NumberOfObjectives)
        {
            case 1:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                slot2.gameObject.transform.parent.gameObject.SetActive(false);
                slot3.gameObject.transform.parent.gameObject.SetActive(false);
                slot4.gameObject.transform.parent.gameObject.SetActive(false);
                break;
            case 2:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                yield return new WaitForSeconds(waittime);
                slot2.gameObject.transform.parent.gameObject.SetActive(true);
                slot3.gameObject.transform.parent.gameObject.SetActive(false);
                slot4.gameObject.transform.parent.gameObject.SetActive(false);
                break;
            case 3:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                yield return new WaitForSeconds(waittime);
                slot2.gameObject.transform.parent.gameObject.SetActive(true);
                yield return new WaitForSeconds(waittime);
                slot3.gameObject.transform.parent.gameObject.SetActive(true);
                slot4.gameObject.transform.parent.gameObject.SetActive(false);
                break;
            case 4:
                slot1.gameObject.transform.parent.gameObject.SetActive(true);
                yield return new WaitForSeconds(waittime);
                slot2.gameObject.transform.parent.gameObject.SetActive(true);
                yield return new WaitForSeconds(waittime);
                slot3.gameObject.transform.parent.gameObject.SetActive(true);
                yield return new WaitForSeconds(waittime);
                slot4.gameObject.transform.parent.gameObject.SetActive(true);
                break;
        }
        yield return new WaitForSeconds(waittime);

        if (IngameObjectiveSetter.current.Objective1 == true)
        {
            ObjectiveCompleteImage1.SetActive(true);
            ObjectiveNotCompleteImage1.SetActive(false);
        }
        else
        {
            ObjectiveCompleteImage1.SetActive(false);
            ObjectiveNotCompleteImage1.SetActive(true);
        }

        yield return new WaitForSeconds(waittime);

        if (IngameObjectiveSetter.current.Objective2 == true)
        {
            ObjectiveCompleteImage2.SetActive(true);
            ObjectiveNotCompleteImage2.SetActive(false);
        }
        else
        {
            ObjectiveCompleteImage2.SetActive(false);
            ObjectiveNotCompleteImage2.SetActive(true);
        }

        yield return new WaitForSeconds(waittime);

        if (IngameObjectiveSetter.current.Objective3 == true)
        {
            ObjectiveCompleteImage3.SetActive(true);
            ObjectiveNotCompleteImage3.SetActive(false);
        }
        else
        {
            ObjectiveCompleteImage3.SetActive(false);
            ObjectiveNotCompleteImage3.SetActive(true);
        }

        yield return new WaitForSeconds(waittime);

        if (IngameObjectiveSetter.current.Objective4 == true)
        {
            ObjectiveCompleteImage4.SetActive(true);
            ObjectiveNotCompleteImage4.SetActive(false);
        }
        else
        {
            ObjectiveCompleteImage4.SetActive(false);
            ObjectiveNotCompleteImage4.SetActive(true);
        }

    }
}
