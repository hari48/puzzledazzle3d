using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBlockAtStart : MonoBehaviour
{
    [SerializeField]
    GameObject Cube;
    [SerializeField]
    GameObject BlockSpawnParticle;
    int currentsquarecolor=0;

    int numberOfBlocksToSpawn = 0;
    // Start is called before the first frame update
    void Start()
    {
        numberOfBlocksToSpawn = LevelManager.current.StartBlocksInGame;
        if (LevelManager.current.StartBlocksInGame > 0)
        {
            while (numberOfBlocksToSpawn > 0)
            {
                SpawnblocksRandomly();
            }
            SettleBlocks();
            StartCoroutine(DisplayBlocks());
        }
    }

    void SpawnblocksRandomly()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (numberOfBlocksToSpawn <= 0)
                {
                    return;
                }
                int x = Random.Range(0, 3);
                if (x == 0 && GridSystem.grid[i,j]==null)
                {
                    switch (currentsquarecolor)
                    {
                        case 0:
                            GameObject sqrobj = Instantiate(Cube, new Vector3(i,j,0), Quaternion.identity);
                            sqrobj.GetComponent<Renderer>().material = LevelUniqueIdentifier.instance.basicMat1;
                            sqrobj.tag = "Blue";
                            sqrobj.GetComponent<MeshRenderer>().enabled = false;
                            GridSystem.grid[i, j] = sqrobj.transform;
                            break;
                        case 1:
                            GameObject sqrobj2 = Instantiate(Cube, new Vector3(i, j, 0), Quaternion.identity);
                            sqrobj2.GetComponent<Renderer>().material = LevelUniqueIdentifier.instance.basicMat2;
                            sqrobj2.tag = "Yellow";
                            sqrobj2.GetComponent<MeshRenderer>().enabled = false;
                            GridSystem.grid[i, j] = sqrobj2.transform;
                            break;
                        case 2:
                            GameObject sqrobj3 = Instantiate(Cube, new Vector3(i, j, 0), Quaternion.identity);
                            sqrobj3.GetComponent<Renderer>().material = LevelUniqueIdentifier.instance.basicMat3;
                            sqrobj3.tag = "Red";
                            sqrobj3.GetComponent<MeshRenderer>().enabled = false;
                            GridSystem.grid[i, j] = sqrobj3.transform;
                            break;
                        case 3:
                            GameObject sqrobj4 = Instantiate(Cube, new Vector3(i, j, 0), Quaternion.identity);
                            sqrobj4.GetComponent<Renderer>().material = LevelUniqueIdentifier.instance.basicMat4;
                            sqrobj4.tag = "Green";
                            sqrobj4.GetComponent<MeshRenderer>().enabled = false;
                            GridSystem.grid[i, j] = sqrobj4.transform;
                            break;
                    }
                    currentsquarecolor++;
                    if (currentsquarecolor >= LevelManager.current.NumberOfColorsToSpawn)
                    {
                        currentsquarecolor = 0;
                    }
                    numberOfBlocksToSpawn--;
                }
            }
        }
    }

    void SettleBlocks()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (GridSystem.grid[i, j] != null)
                {
                    if (j != 0)
                    {
                        for (int k = j-1; k >= 0; k--)
                        {
                            if (GridSystem.grid[i, k] == null)
                            {
                                Transform go = GridSystem.grid[i, k + 1].GetComponent<Transform>();
                                go.position = new Vector3(i, k, 0);
                                GridSystem.grid[i, k + 1] = null;
                                GridSystem.grid[i, k] = go;
                            }
                        }
                    }
                }
            }
        }
    }

    IEnumerator DisplayBlocks()
    {
        int x = Random.Range(0, 2);

        if (x == 0)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (GridSystem.grid[i, j] != null)
                    {
                        float waittime;
                        if (Time.timeScale == 0)
                        {
                            waittime = 0.1f;
                        }
                        else
                        {
                            waittime = 0.1f;
                        }
                        GameObject bsp = Instantiate(BlockSpawnParticle, GridSystem.grid[i, j].position, Quaternion.identity);
                        foreach (var item in bsp.GetComponentsInChildren<Transform>())
                        {
                            ParticleSystem.MainModule ps = item.GetComponent<ParticleSystem>().main;
                            if (GridSystem.grid[i, j].CompareTag("Blue"))
                            {
                                Color32 cl = new Color32(114, 249, 253, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                            else if (GridSystem.grid[i, j].CompareTag("Yellow"))
                            {
                                Color32 cl = new Color32(255, 255, 92, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                            else if (GridSystem.grid[i, j].CompareTag("Red"))
                            {
                                Color32 cl = new Color32(255, 59, 42, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                            else if (GridSystem.grid[i, j].CompareTag("Green"))
                            {
                                Color32 cl = new Color32(0, 249, 53, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                        }
                        Destroy(bsp, 3f);
                        yield return new WaitForSecondsRealtime(waittime);
                        GridSystem.grid[i, j].GetComponent<MeshRenderer>().enabled = true;
                    }
                }
            }
        }
        if (x == 1)
        {
            for (int i = 9; i >= 0; i--)
            {
                for (int j = 5; j >=0; j--)
                {
                    if (GridSystem.grid[i, j] != null)
                    {
                        float waittime;
                        if (Time.timeScale == 0)
                        {
                            waittime = 0.1f;
                        }
                        else
                        {
                            waittime = 0.1f;
                        }
                        GameObject bsp = Instantiate(BlockSpawnParticle, GridSystem.grid[i, j].position, Quaternion.identity);
                        foreach (var item in bsp.GetComponentsInChildren<Transform>())
                        {
                            ParticleSystem.MainModule ps = item.GetComponent<ParticleSystem>().main;
                            if (GridSystem.grid[i, j].CompareTag("Blue"))
                            {
                                Color32 cl = new Color32(114, 249, 253, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                            else if (GridSystem.grid[i, j].CompareTag("Yellow"))
                            {
                                Color32 cl = new Color32(255, 255, 92, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                            else if (GridSystem.grid[i, j].CompareTag("Red"))
                            {
                                Color32 cl = new Color32(255, 59, 42, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                            else if (GridSystem.grid[i, j].CompareTag("Green"))
                            {
                                Color32 cl = new Color32(0, 249, 53, 255);
                                ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                            }
                        }
                        Destroy(bsp, 3f);
                        yield return new WaitForSecondsRealtime(waittime);
                        GridSystem.grid[i, j].GetComponent<MeshRenderer>().enabled = true;
                    }
                }
            }
        }
       
    }
}
