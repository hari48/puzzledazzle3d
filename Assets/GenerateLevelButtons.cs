using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using UnityEngine.UI.Extensions;
public class GenerateLevelButtons : MonoBehaviour
{
    [SerializeField]
    ScrollRect scrollRect;
    [SerializeField]
    GameObject LevelButton;
    [SerializeField]
    RectTransform sRContent;
    [SerializeField]
   // UILineTextureRenderer lr;
    List<RectTransform> buttonPos = new List<RectTransform>();
    public List<RectTransform> buttonPosBase = new List<RectTransform>();
    [Range(-20, 20)]
    public List<int> IndividualButtonPos;
    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < 50; i++)
        {
            GameObject bth = Instantiate(LevelButton, transform);
            bth.transform.parent = transform;
            RectTransform btt = bth.transform.GetChild(0).GetComponent<RectTransform>();
            SelectLevel sl = bth.transform.GetChild(0).GetComponent<SelectLevel>();
            TMP_Text bthtxt = bth.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
            btt.anchoredPosition = new Vector3(IndividualButtonPos[i], 0, 0);
            sl.ButtonLevelid = i + 1;
            bthtxt.text = (i + 1).ToString();
            buttonPos.Add(btt);
            buttonPosBase.Add(bth.GetComponent<RectTransform>());
        }
       // lr.Points = new Vector2[PlayerPrefs.GetInt("MaxLevel", 1)];
        Invoke("SnapToDelay", 0.01f);
    }
    void SnapToDelay()
    {

        SnapTo(buttonPosBase[49 - (PlayerPrefs.GetInt("MaxLevel", 1) - 1)]);
        Invoke("showline", 0.1f);
    }
    void showline()
    {
       // lr.enabled = true;
    }
    public void SnapTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();
        sRContent.anchoredPosition = new Vector2(sRContent.anchoredPosition.x, target.anchoredPosition.y + 100);

    }
    private void Update()
    {
        /*
        lr.rectTransform.anchoredPosition = new Vector3(gameObject.GetComponent<RectTransform>().anchoredPosition.x, Mathf.Abs(buttonPosBase[0].anchoredPosition.y) + gameObject.GetComponent<RectTransform>().anchoredPosition.y, 0);
        for (int i = 0; i < buttonPos.Count; i++)
        {
            if (PlayerPrefs.GetInt("MaxLevel", 1) != 1)
            {
                if (i < PlayerPrefs.GetInt("MaxLevel", 1))
                {
                    lr.Points[i] = new Vector2(buttonPos[i].anchoredPosition.x, buttonPosBase[i].anchoredPosition.y);
                }
            }
        }
        */
    }
}