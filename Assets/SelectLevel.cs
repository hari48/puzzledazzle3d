using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SelectLevel : MonoBehaviour
{
    public int ButtonLevelid = 0;
    public TMP_Text lvltxt;
    public bool islocked=false;
    [SerializeField]
    GameObject lockImg;

    private void Start()
    {
        //Button bt = GetComponent<Button>();
        if (PlayerPrefs.GetInt("MaxLevel", 1) < ButtonLevelid)
        {
            lockImg.SetActive(true);
            islocked = true;
           // bt.interactable = false;
        }
        else if (PlayerPrefs.GetInt("MaxLevel", 1) >= ButtonLevelid)
        {
            lockImg.SetActive(false);
            islocked = false;
          //  bt.interactable = true;
        }
    }
    public void SelectLevelOnClick()
    {
        //AnalyticsManager.current.TriggerEventLevelSelect_StartedLevel(ButtonLevelid);
        LevelManager.current.LoadLevel(ButtonLevelid);
    }
}
