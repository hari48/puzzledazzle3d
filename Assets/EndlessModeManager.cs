using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessModeManager : MonoBehaviour
{
    float starttime;
    int x = 0;
    // Start is called before the first frame update
    void Start()
    {
        starttime = Time.time;
        x = 0;
    }

    // Update is called once per frame
    void Update()
    {
        SetScalingObjective();
    }
    void SetScalingObjective()
    {
        if (Time.time - starttime >= 90f && x == 0)
        {
            GameManager.instance.FallSpeed = 0.78f;
            x++;
        }
        else if (Time.time - starttime >= 180f && x == 1)
        {
            LevelManager.current.ChanceOfSpawningRed = 10;
            x++;
        }
        else if (Time.time - starttime >= 198f && x == 2)
        {
            GameManager.instance.FallSpeed = 0.72f;
            x++;
        }
        else if (Time.time - starttime >= 300f && x == 3)
        {
            // LevelManager.current.ChanceOfSpawningGreen = 10;
            x++;
        }
        else if (Time.time - starttime >= 360f && x == 4)
        {
            GameManager.instance.FallSpeed = 0.68f;
            x++;
        }
        else if (Time.time - starttime >= 480f && x == 5)
        {
            GameManager.instance.FallSpeed = 0.62f;
            x++;
        }
    }
}
