﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GridSystem : MonoBehaviour
{

    public int width = 16;
    public int height = 10;

    public Color[] color;
    public Sprite blueSprite, yellowSprite;
    public Material blueMat, yellowMat,redMat,greenMat;
    public Color[] currentColors;
    private GameObject particleExplosion;
    private GameObject activeParticle = null;

    public static Transform[,] grid;

    public float[,] gridPositions;
    public static List<int> gridListToClear = new List<int>();
    public float initialX = 0.03f;
    public float initialY = 0.05f;
    public Transform gridCenterPos;
    

    public static GridSystem instance;




    public GameObject trailHead;

    public bool combo1 = false;  //anand v additions. Piececontroller just before spawnpiece, will turn this On




    public bool isCleared = false;

    [SerializeField]
    GameObject WinParticle;
    [SerializeField]
    GameObject GameWinShow;
    int bluesinglecombocounter;
    int yellowsinglecombocounter;
    int redsinglecombocounter;
    int greensinglecombocounter;

    [SerializeField]
    GameObject CollectParticle;
    [SerializeField]
    GameObject HorizontalSpecialExplosionParticle;
    [SerializeField]
    GameObject VerticalSpecialExplosionParticle;
    [SerializeField]
    GameObject blockClearSparkleParticle;
    [SerializeField]
    GameObject BlockClearBombParticle;
    int combotestcounter;

    [SerializeField]
    AudioSource clearing1;
    [SerializeField]
    AudioSource clearing2;
    [SerializeField]
    AudioSource clearing3;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        grid = new Transform[width, height];
        gridPositions = new float[width, height];
    }

    private void Start()
    {
        Time.timeScale = 1f;
        int index = PlayerPrefs.GetInt("theme", 0);
        currentColors = new Color[4];
        currentColors[0] = LevelUniqueIdentifier.instance.blockColor1;
        currentColors[1] = LevelUniqueIdentifier.instance.blockColor2;
        currentColors[2] = LevelUniqueIdentifier.instance.blockColor3;
        currentColors[3] = LevelUniqueIdentifier.instance.blockColor4;


        color[0] = LevelUniqueIdentifier.instance.blockColor1;
        color[1] = LevelUniqueIdentifier.instance.blockColor2;

        blueMat.SetColor("_EmissionColor", color[0]);
        yellowMat.SetColor("_EmissionColor", color[1]);
        trailHead = FindObjectOfType<TrailHead>().trailHead;

        if (LevelUniqueIdentifier.instance.clearingAnim == null)
            particleExplosion = Resources.Load("parEx") as GameObject;
        else
            particleExplosion = LevelUniqueIdentifier.instance.clearingAnim;

        StartCoroutine(DestroyLater());


    }

    private void Update()
    {/*
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SettlePeicesinEmpty();
        }

        if (Input.GetKeyDown(KeyCode.J) && ScoreManager.instance.CheckLoadingBar(0.66f, 1.0f))             //Anand V Mod. Combo Power code
        {
            ScoreManager.instance.LBA2(0.66f);
            for (int i = 0; i < width; i++)
            {
                if (grid[i, 0] != null)
                {
                    tempMatch.Add(grid[i, 0].gameObject);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.K) && ScoreManager.instance.CheckLoadingBar(1.0f, 2.0f))               //Anand V Mod. Combo Power code     
        {
            ScoreManager.instance.CheckLoadingBar(1.0f, 2.0f);
            for (int j = 0; j < 2; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    if (grid[i, j] != null)
                    {
                        tempMatch.Add(grid[i, j].gameObject);
                    }
                }
            }
        }*/
    }
    /*
    public void FirstCombo(PieceController piece)
    {
        for(int x = 0; x <= 4; x++)
        {
            for(int y = 0; y <= 4; y++)
            {
                if(grid[x,y] != null)
                {
                    Vector2 bomb = Round(piece.transform.position);
                    grid[(int)bomb.x, (int)bomb.y] = null;
                }
            }
        }
    }
    */
    public void UpdateGrid(PieceController piece)
    {
        if (piece != null)
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    if (grid[x, y] != null)
                    {
                        if (grid[x, y].parent == piece.transform)
                        {
                            grid[x, y] = null;
                        }
                    }
                }
            }

        /*
        bool addedOnce = true;
        foreach (Transform child in piece.transform)
        {
            Vector2 pos = Round(child.position);
            //   y > 0 && y < 10 && x > 0 && x < 16
            if (pos.y < height && pos.y >= 0 && pos.x >= 0 && pos.x < 16)
            {
                grid[(int)pos.x, (int)pos.y] = child;
               
                if (combo1)    //Anand V Mods. From piececontroller spawnPiece 158
                {
                    //Debug.Log("piece x: "+pos.x +"piece y: " + pos.y);
                    tempMatch.Add(grid[(int)pos.x, (int)pos.y].gameObject);
                    if (addedOnce)
                    {

                        addedOnce = false;
                        int o = (int)pos.x;
                        int p = (int)pos.y;
                        adderToTempMatch(o - 1, p);
                        adderToTempMatch(o - 1, p - 1);
                        adderToTempMatch(o, p - 2);
                        adderToTempMatch(o + 1, p - 2);
                        adderToTempMatch(o + 2, p);
                        adderToTempMatch(o + 2, p - 1);

                       // adderToTempMatch(grid[o, p]);
                        //adderToTempMatch(grid[o, p-1]);
                       // adderToTempMatch(grid[o+1, p-1]);
                       // adderToTempMatch(grid[o+1, p]); 
                    }

                }
            }

        }
        combo1 = false;   //anandV Mod
        */

        foreach (Transform child in piece.transform)
        {
            Vector2 pos = Round(child.position);
            //   y > 0 && y < 10 && x > 0 && x < 16
            if (pos.y < height && pos.y >= 0 && pos.x >= 0 && pos.x < 16)
            {
                grid[(int)pos.x, (int)pos.y] = child;
            }
        }

    }

    /* private void adderToTempMatch (Transform gridKiller)      // anand v  adds grids for deletion to tempMatch for combo1 
     {
         if(gridKiller != null)
         {
             tempMatch.Add(gridKiller.gameObject);
         }
     }*/

    /*
    private void adderToTempMatch(int o, int j)      // anand v  adds grids for deletion to tempMatch for combo1 
    {
        if (o>=0 && o<width && j>=0 && j<height)
        {
            Debug.Log("gridsystem 191 O: " + o + " & J: " + j);
            if (grid[o, j] != null)
                tempMatch.Add(grid[o,j].gameObject);
        }
    }
    */
    public void UpdateIndividualGrid(Transform child)
    {

        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                if (grid[x, y] != null)
                {
                    if (grid[x, y] == child.transform)
                    {
                        grid[x, y] = null;
                    }
                }
            }
        }

        Vector2 pos = Round(child.position);
        if (pos.y < height && pos.y >= 0 && pos.x >= 0 && pos.x < 16)
        {
            grid[(int)pos.x, (int)pos.y] = child;
        }

    }


    public Transform GetTransformAtGridPosition(Vector2 pos)
    {
        if (pos.y > height - 1)
        {
            return null;
        }
        else
        {

            return grid[(int)pos.x, (int)pos.y];
        }
    }

    public bool CheckIfInsideGrid(Vector2 pos)
    {

        if (GameManager.instance.currentPiece.GetComponent<PieceController>().ReturnChildCount() == 2)
        {
            if (GameManager.instance.currentPiece.GetComponent<PieceController>().ReturnAngle().z == 90)
                return ((int)pos.x >= 0 && (int)pos.x < width - 1 && (int)pos.y >= 0);
            else if (GameManager.instance.currentPiece.GetComponent<PieceController>().ReturnAngle().z == 270)
                return ((int)pos.x >= 1 && (int)pos.x < width && (int)pos.y >= 0);
        }

        return ((int)pos.x >= 0 && (int)pos.x < width && (int)pos.y >= 0);
    }

    public Vector2 Round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
        //return new Vector2(Mathf.Floor(pos.x), Mathf.Floor(pos.y));
    }

    public Transform[,] ReturnGrid()
    {
        return grid;
    }

    public TMP_InputField X;
    public TMP_InputField Y;
    public void DisplayGridInformationAsked()
    {
        int x = int.Parse(X.text);
        int y = int.Parse(Y.text);
        Debug.Log("Grid[" + X.text + "][ " + Y.text + "] = " + grid[x, y]);
    }


    public List<GameObject> matchList = new List<GameObject>();
    public void FloodFillAlgorithm(int i, int j, string colorTag)
    {
        //   Debug.Log("i = " + i + " j = " + j);
        if (CheckIfInsideGrid(new Vector2(i, j)))
        {
            if (j < 10)
                if (grid[i, j] != null && grid[i, j].tag == colorTag && !matchList.Contains(grid[i, j].gameObject))
                {
                    matchList.Add(grid[i, j].gameObject);
                    if (i < 16)
                        FloodFillAlgorithm(i + 1, j, colorTag); //re-do check to the right
                    if (i > 0)
                        FloodFillAlgorithm(i - 1, j, colorTag); //re-do check to the left
                    if (j < 10)
                        FloodFillAlgorithm(i, j + 1, colorTag); //re-do check up
                    if (j > 0)
                        FloodFillAlgorithm(i, j - 1, colorTag); //re-do check down
                }
        }
    }

    public List<GameObject> tempMatch = new List<GameObject>();
    public Color curColor;
    public void ClearAllMatches(int i, int j)
    {
        if (combotestcounter == 0 && (i == width - 1) && (j == height - 1))
        {
            LevelManager.current.Combocounter = 0;
        }
        if (grid[i, j] == null)
            return;
        GameObject piece = grid[i, j].gameObject;
        FloodFillAlgorithm(i, j, piece.tag);


        if (matchList.Count >= 4)
        {
            if (i < width - 1 && j < height - 1)
                if (grid[i, j] != null && grid[i + 1, j] != null && grid[i, j + 1] != null && grid[i + 1, j + 1] != null)
                {
                    if (grid[i, j].tag == grid[i + 1, j].tag && grid[i, j].tag == grid[i, j + 1].tag && grid[i, j].tag == grid[i + 1, j + 1].tag)
                    {
                        Sprite sprite;
                        Material mat;


                        if (piece.tag == "Blue")
                        {
                            sprite = blueSprite;
                            mat = blueMat;
                            curColor = currentColors[0];
                        }
                        else if(piece.tag == "Yellow")
                        {
                            sprite = yellowSprite;
                            mat = yellowMat;
                            curColor = currentColors[1];
                        }
                        else if(piece.tag=="Red")
                        {
                            mat = redMat;
                            curColor = currentColors[2];
                        }
                        else
                        {
                            mat = greenMat;
                            curColor = currentColors[3];
                        }

                        curColor.a = 256;
                        if (!tempMatch.Contains(grid[i, j].gameObject))
                        {
                            //         grid[i, j].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            grid[i, j].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i, j].gameObject);
                            CheckForSpecialPower(grid[i, j].gameObject, i, j);
                            combotestcounter++;
                        }
                        if (!tempMatch.Contains(grid[i + 1, j].gameObject))
                        {
                            //    grid[i + 1, j].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            //   grid[i + 1, j].gameObject.GetComponent<SpriteRenderer>().color = curColor;
                            grid[i + 1, j].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i + 1, j].gameObject);
                            CheckForSpecialPower(grid[i + 1, j].gameObject, i + 1, j);
                            combotestcounter++;
                        }
                        if (!tempMatch.Contains(grid[i, j + 1].gameObject))
                        {
                            //    grid[i, j + 1].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            //   grid[i, j + 1].gameObject.GetComponent<SpriteRenderer>().color = curColor;
                            grid[i, j + 1].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i, j + 1].gameObject);
                            CheckForSpecialPower(grid[i, j + 1].gameObject, i, j + 1);
                            combotestcounter++;
                        }
                        if (!tempMatch.Contains(grid[i + 1, j + 1].gameObject))
                        {
                            //   grid[i + 1, j + 1].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            //   grid[i + 1, j + 1].gameObject.GetComponent<SpriteRenderer>().color = curColor;
                            grid[i + 1, j + 1].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i + 1, j + 1].gameObject);
                            CheckForSpecialPower(grid[i + 1, j + 1].gameObject, i + 1, j + 1);
                            combotestcounter++;
                        }
                    }
                }

            /*  for (int k = 0; k < matchList.Count; k++)
              {
                  matchList[k].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                  Vector2 tempPos = Round(matchList[k].transform.position);
                  if (!tempMatch.Contains(matchList[k]))
                      tempMatch.Add(matchList[k]);
              }*/


        }
        else
        {
            isCleared = false;

        }

        matchList.Clear();

    }
    public void ClearWithoutMatch(int i, int j)
    {
        GameObject piece = grid[i, j].gameObject;
        Material mat;
        if (piece.tag == "Blue")
        {
            mat = blueMat;
            curColor = currentColors[0];
        }
        else if (piece.tag == "Yellow")
        {
            mat = yellowMat;
            curColor = currentColors[1];
        }
        else if (piece.tag == "Red")
        {
            mat = redMat;
            curColor = currentColors[2];
        }
        else
        {
            mat = greenMat;
            curColor = currentColors[3];
        }

        curColor.a = 256;
        if (!tempMatch.Contains(grid[i, j].gameObject))
        {
            grid[i, j].gameObject.GetComponent<Renderer>().material = mat;
            tempMatch.Add(grid[i, j].gameObject);
            CheckForSpecialPower(grid[i, j].gameObject, i, j);
        }
    }
    private void CheckForSpecialPower(GameObject currentObject, int x, int y)
    {
        Sprite sprite;
        Material mat;
        if (currentObject.name == "stripeHorizontal")
        {
            int rowscleared = 0;
            for (int i = 0; i < width; i++)
            {
                if (grid[i, y] != null)
                {
                    GameObject piece = grid[i, y].gameObject;
                    if (!tempMatch.Contains(piece))
                    {
                        tempMatch.Add(piece);
                        if (piece.tag == "Blue")
                        {
                            mat = blueMat;
                            sprite = blueSprite;
                            curColor = currentColors[0];
                        }
                        else if (piece.tag == "Yellow")
                        {
                            sprite = yellowSprite;
                            mat = yellowMat;
                            curColor = currentColors[1];
                        }
                        else if (piece.tag == "Red")
                        {
                            mat = redMat;
                            curColor = currentColors[2];
                        }
                        else
                        {
                            mat = greenMat;
                            curColor = currentColors[3];
                        }
                        rowscleared++;
                        curColor.a = 256;
                        piece.gameObject.GetComponent<Renderer>().material = mat;
                        //piece.gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                        //piece.gameObject.GetComponent<SpriteRenderer>().color = curColor;

                        if (piece.name == "stripeHorizontal")
                        {
                            LevelManager.current.HorizontalPowerUPUsed++;
                        }
                    }
                }
            }
            LevelManager.current.HorizontalPowerUPUsed++;
            /*
            GameObject hsep = Instantiate(HorizontalSpecialExplosionParticle, new Vector3(4.5f, y - 0.5f, 0), Quaternion.Euler(90, 0, 0));
            StartCoroutine(EnlargeSpecialParticles(hsep));
            Destroy(hsep, 0.5f);
            GameObject hsep1 = Instantiate(HorizontalSpecialExplosionParticle, new Vector3(4.5f, y, 0), Quaternion.Euler(90, 0, 0));
            StartCoroutine(EnlargeSpecialParticles(hsep1));
            Destroy(hsep1, 0.5f);
            GameObject hsep2 = Instantiate(HorizontalSpecialExplosionParticle, new Vector3(4.5f, y + 0.5f, 0), Quaternion.Euler(90, 0, 0));
            StartCoroutine(EnlargeSpecialParticles(hsep2));
            Destroy(hsep2, 0.5f);*/
            if (LevelManager.current.HorizontalPowerUpMaxRowsCleared < rowscleared)
            {
                LevelManager.current.HorizontalPowerUpMaxRowsCleared = rowscleared;
            }
            specialPower = true;
        }
        else if (currentObject.name == "stripeVertical")
        {
            int columnscleared = 0;
            for (int i = 0; i < height; i++)
            {
                if (grid[x, i] != null)
                {
                    GameObject piece = grid[x, i].gameObject;
                    if (!tempMatch.Contains(piece))
                    {
                        tempMatch.Add(piece);
                        if (piece.tag == "Blue")
                        {
                            mat = blueMat;
                            sprite = blueSprite;
                            curColor = currentColors[0];
                        }
                        else if (piece.tag == "Yellow")
                        {
                            sprite = yellowSprite;
                            mat = yellowMat;
                            curColor = currentColors[1];
                        }
                        else if (piece.tag == "Red")
                        {
                            mat = redMat;
                            curColor = currentColors[2];
                        }
                        else
                        {
                            mat = greenMat;
                            curColor = currentColors[3];
                        }
                        columnscleared++;
                        curColor.a = 256;
                        piece.gameObject.GetComponent<Renderer>().material = mat;
                        // piece.gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                        // piece.gameObject.GetComponent<SpriteRenderer>().color = curColor;
                        if (piece.name == "stripeVertical")
                        {
                            LevelManager.current.VerticalPowerUPUsed++;
                        }
                    }
                }
            }
            LevelManager.current.VerticalPowerUPUsed++;
            /*
            GameObject vsep = Instantiate(VerticalSpecialExplosionParticle, new Vector3(x - 0.5f, 7.5f, 0), Quaternion.Euler(0, 90, -90));
            StartCoroutine(EnlargeSpecialParticles(vsep));
            Destroy(vsep, 0.5f);
            GameObject vsep1 = Instantiate(VerticalSpecialExplosionParticle, new Vector3(x, 7.5f, 0), Quaternion.Euler(0, 90, -90));
            StartCoroutine(EnlargeSpecialParticles(vsep1));
            Destroy(vsep1, 0.5f);
            GameObject vsep2 = Instantiate(VerticalSpecialExplosionParticle, new Vector3(x + 0.5f, 7.5f, 0), Quaternion.Euler(0, 90, -90));
            StartCoroutine(EnlargeSpecialParticles(vsep2));
            Destroy(vsep2, 0.5f);*/
            if (LevelManager.current.VerticalPowerUpMaxRowsCleared < columnscleared)
            {
                LevelManager.current.VerticalPowerUpMaxRowsCleared = columnscleared;
            }
            specialPower = true;
        }
    }
    IEnumerator EnlargeSpecialParticles(GameObject particle)
    {
        while (particle.transform.localScale.x < 1.2f)
        {
            if (particle == null)
            {
                yield break;
            }
            particle.transform.localScale += new Vector3(0.02f, 0.02f, 0.02f);
            yield return new WaitForSeconds(0.05f);
        }
    }
    bool specialPower = false;
    float delaycollectioncount = 0;
    private IEnumerator DestroyLater()
    {
        
        while (true)
        {
            if (tempMatch.Count > 0)
            {
                /*
                GameObject go = tempMatch[0];
                if (go.transform.childCount == 1)
                {
                    //  Destroy(go.transform.GetChild(0).gameObject);
                        // go.transform.GetChild(0).gameObject.AddComponent<TriangleExplosion>();
                        // StartCoroutine(go.transform.GetChild(0).GetComponent <TriangleExplosion>().SplitMesh(true));
                       //  tempMatch.Remove(tempMatch[0]);
                       //  Instantiate(particleExplosion, go.transform.position, Quaternion.identity);
                       //  ScoreManager.instance.UpdateScore();
                }
                else
                {
                    tempMatch[0].GetComponent<Renderer>().enabled = true;
                    tempMatch.Remove(tempMatch[0]);

                    if (activeParticle == null)
                    {
                        activeParticle = Instantiate(particleExplosion, gridCenterPos.position, Quaternion.identity);
                        GameObject.Destroy(activeParticle, 3f);
                    }
                    Debug.Log("GridSystem : inside IEnumerator DestroyLater() line 332");
                    
                    Rigidbody rb = go.AddComponent<Rigidbody>();
                    rb.useGravity = false;
                    rb.AddExplosionForce(1200, rb.transform.position + (new Vector3(Random.Range(0, 2), Random.Range(-2, 2), 2)), 10, 0);
                    rb.AddTorque(new Vector3(Random.Range(-500, 500), Random.Range(-500, 500), Random.Range(-500, 500)));
                    //   StartCoroutine(GameManager.instance.TempSlowMotion(0.4f, 0.5f));
                    Vector2 tempPos = Round(go.transform.position);
                    grid[(int)tempPos.x, (int)tempPos.y] = null;
                    ScoreManager.instance.UpdateScore();
                    Destroy(go, 2f);
                        //  if (trailHead.transform.position.x > tempMatch[0].transform.position.x && (trailHead.transform.position.x - tempMatch[0].transform.position.x <= 2f))
                         // {


                          //}

                }
                if (tempMatch.Count == 0)
                {
                    GameObject.FindGameObjectWithTag("H").GetComponent<AudioSource>().Play();
                    GameManager.instance.currentPiece.GetComponent<PieceController>().isCleared = true;


                    Invoke("SettlePeicesinEmpty", 0.24f);
                }*/


                if (tempMatch[0] != null)
                {
                    for (int i = 0; i < tempMatch.Count; i++)
                    {


                        if (tempMatch[i].tag == "Blue")
                        {
                            LevelManager.current.BlueCubesDestroyed += 1;
                            if (bluesinglecombocounter == 0)
                            {
                                StartCoroutine(resetsinglecombocounter1());
                            }

                            bluesinglecombocounter++;

                            for (int x = 0; x < 4; x++)
                            {
                                if (LevelManager.current.BlueObjectiveSetter[x] == true)
                                {
                                    GameObject cp = Instantiate(CollectParticle, tempMatch[i].transform.position, Quaternion.identity);
                                   /* CollectionParticleMover cpm = cp.GetComponent<CollectionParticleMover>();
                                    cpm.ColorID = 0;
                                    cpm.DestinationID = x;
                                    cpm.delay = delaycollectioncount;*/
                                }
                            }
                        }
                        else if (tempMatch[i].tag == "Yellow")
                        {
                            LevelManager.current.YellowCubesDestroyed += 1;
                            if (yellowsinglecombocounter == 0)
                            {
                                StartCoroutine(resetsinglecombocounter2());
                            }

                            yellowsinglecombocounter++;

                            for (int x = 0; x < 4; x++)
                            {
                                if (LevelManager.current.YellowObjectiveSetter[x] == true)
                                {
                                    GameObject cp = Instantiate(CollectParticle, tempMatch[i].transform.position, Quaternion.identity);
                                   /* CollectionParticleMover cpm = cp.GetComponent<CollectionParticleMover>();
                                    cpm.ColorID = 1;
                                    cpm.DestinationID = x;
                                    cpm.delay = delaycollectioncount;*/
                                }
                            }
                        }
                        else if (tempMatch[i].tag == "Red")
                        {

                            LevelManager.current.RedDestroyed += 1;
                            if (redsinglecombocounter == 0)
                            {
                                StartCoroutine(resetsinglecombocounter3());
                            }

                            redsinglecombocounter++;

                            for (int x = 0; x < 4; x++)
                            {
                                if (LevelManager.current.RedObjectiveSetter[x] == true)
                                {
                                    GameObject cp = Instantiate(CollectParticle, tempMatch[i].transform.position, Quaternion.identity);
                                    /*CollectionParticleMover cpm = cp.GetComponent<CollectionParticleMover>();
                                    cpm.ColorID = 2;
                                    cpm.DestinationID = x;
                                    cpm.delay = delaycollectioncount;*/
                                }
                            }


                        }
                        else
                        {

                            LevelManager.current.GreenDestroyed += 1;
                            if (greensinglecombocounter == 0)
                            {
                                StartCoroutine(resetsinglecombocounter4());
                            }

                            greensinglecombocounter++;

                            for (int x = 0; x < 4; x++)
                            {
                                if (LevelManager.current.GreenObjectiveSetter[x] == true)
                                {
                                    GameObject cp = Instantiate(CollectParticle, tempMatch[i].transform.position, Quaternion.identity);
                                    /*CollectionParticleMover cpm = cp.GetComponent<CollectionParticleMover>();
                                    cpm.ColorID = 3;
                                    cpm.DestinationID = x;
                                    cpm.delay = delaycollectioncount;*/
                                }
                            }


                        }

                        delaycollectioncount += 0.2f;

                        GameObject go = tempMatch[i];

                        Rigidbody rb = go.AddComponent<Rigidbody>();
                        rb.useGravity = false;
                        rb.AddExplosionForce(1200, rb.transform.position + (new Vector3(Random.Range(0, 2), Random.Range(-2, 2), 2)), 10, 0);
                        rb.AddTorque(new Vector3(Random.Range(-500, 500), Random.Range(-500, 500), Random.Range(-500, 500)));

                        GameObject blockclearsparkle = Instantiate(particleExplosion, go.transform.position, Quaternion.identity);
                        ParticleSystem.MainModule ps = blockclearsparkle.GetComponent<ParticleSystem>().main;

                        if (tempMatch[i].tag == "Blue")
                        {
                            Color32 cl = new Color32(114, 249, 253, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (tempMatch[i].tag == "Yellow")
                        {
                            Color32 cl = new Color32(255, 255, 92, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (tempMatch[i].tag == "Red")
                        {
                            Color32 cl = new Color32(255, 59, 42, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else
                        {
                            Color32 cl = new Color32(0, 249, 53, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }



                        tempMatch.Remove(tempMatch[i]);


                        GameObject sparkle = Instantiate(blockClearSparkleParticle, go.transform.position, Quaternion.identity);
                        Destroy(sparkle, 1f);
                        GameObject bombeffect = Instantiate(BlockClearBombParticle, go.transform.position, Quaternion.identity);
                        Destroy(bombeffect, 2f);
                        int randnumber = Random.Range(0, 3);

                        if (randnumber == 0)
                        {
                            clearing1.Play();
                        }
                        else if (randnumber == 1)
                        {
                            clearing2.Play();
                        }
                        else
                        {
                            clearing3.Play();
                        }
                        Vector2 tempPos = Round(go.transform.position);
                        grid[(int)tempPos.x, (int)tempPos.y] = null;
                        ScoreManager.instance.UpdateScore();

                        GameObject.Destroy(go, 0.3f);

                        
                        /*GameObject go1 = Instantiate(Resources.Load("PointsPop") as GameObject, go.transform.position, Quaternion.identity);
                        go1.transform.GetComponentInChildren<TextMeshProUGUI>().color = go.GetComponent<SpriteRenderer>().color;

                        GameObject.Destroy(go1, 1f);*/


                    }

                    if (tempMatch.Count == 0)
                    {
                        delaycollectioncount = 0;

                        ScoreManager.instance.SaveScore();
                        GameObject.FindGameObjectWithTag("H").GetComponent<AudioSource>().Play();
                        if (UIHandler.instance.isVibrationOn == 1)

                            isCleared = true;
                        LevelManager.current.Combocounter++;
                        if (LevelManager.current.MaxCombo < LevelManager.current.Combocounter)
                        {
                            LevelManager.current.MaxCombo = LevelManager.current.Combocounter;
                        }


                        if (LevelManager.current.Combocounter >= 2)
                        {
                            LevelManager.current.Combocounter2x++;
                        }
                        if (LevelManager.current.Combocounter >= 3)
                        {
                            LevelManager.current.Combocounter3x++;
                        }
                        if (LevelManager.current.Combocounter >= 4)
                        {
                            LevelManager.current.Combocounter4x++;
                        }

                        Invoke("SettlePeicesinEmptyAfterExplosion", 0.24f);
                    }
                }
            }

            yield return null;
        }
    }
    IEnumerator resetsinglecombocounter1()
    {
        yield return new WaitForSeconds(0.2f);
        if (bluesinglecombocounter >= 6 && bluesinglecombocounter < 8)
        {
            LevelManager.current.blue6ComboCounter++;
        }
        else if (bluesinglecombocounter >= 8)
        {
            LevelManager.current.blue6ComboCounter++;
            LevelManager.current.Blue8ComboCounter++;
        }
        bluesinglecombocounter = 0;


    }
    IEnumerator resetsinglecombocounter2()
    {
        yield return new WaitForSeconds(0.2f);
        if (yellowsinglecombocounter >= 6 && yellowsinglecombocounter < 8)
        {
            LevelManager.current.Yellow6ComboCounter++;
        }
        else if (yellowsinglecombocounter >= 8)
        {
            LevelManager.current.Yellow6ComboCounter++;
            LevelManager.current.Yellow8ComboCounter++;
        }
        yellowsinglecombocounter = 0;

    }
    IEnumerator resetsinglecombocounter3()
    {
        yield return new WaitForSeconds(0.2f);
        if (redsinglecombocounter >= 6 && redsinglecombocounter < 8)
        {
            LevelManager.current.Red6ComboCounter++;
        }
        else if (redsinglecombocounter >= 8)
        {
            LevelManager.current.Red6ComboCounter++;
            LevelManager.current.Red8ComboCounter++;
        }
        redsinglecombocounter = 0;

    }
    IEnumerator resetsinglecombocounter4()
    {
        yield return new WaitForSeconds(0.2f);
        if (greensinglecombocounter >= 6 && greensinglecombocounter < 8)
        {
            LevelManager.current.Green6ComboCounter++;
        }
        else if (greensinglecombocounter >= 8)
        {
            LevelManager.current.Green6ComboCounter++;
            LevelManager.current.Green8ComboCounter++;
        }
        greensinglecombocounter = 0;

    }
    public IEnumerator TransitToPosition(GameObject target, Vector2 pos)
    {

        float waitForSeconds = 0.5f;
        float waitTime = 0;
        while (waitTime < waitForSeconds)
        {
            waitTime += Time.deltaTime / waitForSeconds;
            target.transform.position = Vector2.Lerp(target.transform.position, pos, waitTime);


            /* Original code
             *   target.transform.position = Vector2.Lerp(target.transform.position, pos, (waitTime / waitForSeconds));
            waitTime += Time.deltaTime;
            */
            yield return null;
        }

        StopCoroutine(TransitToPosition(null, Vector2.zero));
        target.transform.position = new Vector3((int)target.transform.position.x + 0.15f, (int)target.transform.position.y + 0.2f);
    }


    public float moveOffset;
    public List<int> pieceNumber = new List<int>();
    public List<GameObject> pieceExtra = new List<GameObject>();
    public void SettlePeicesinEmpty()
    {
        AlignPieces();
        spe = 0;
        pieceNumber.Clear();
        pieceExtra.Clear();
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {

                StartCoroutine(SettlePiecesinEmptyWait(i, j));

                if (spe == 0 && i == width - 1 && j == height - 1)
                {
                    AlignPieces();
                    FinishClearing();
                }
            }
        }
        //    if(tempMatch.Count == 0)
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;

    }
    int spe = 0;
    IEnumerator SettlePiecesinEmptyWait(int i, int j)
    {
        if (grid[i, j] == null)
        {
            yield break;
        }
        GameObject square = grid[i, j].gameObject;
        Vector2 pos = Round(square.transform.position);
        int x = (int)(pos.x);
        int y = (int)(pos.y);
        if (CheckIfInsideGrid(square.transform.position))
        {
            for (int k = y - 1; k >= 0; k--)
            {
                if (grid[x, k] == null)
                {
                    spe++;
                    //Then Go Down, becuase you see? There is a fucking empty space :/
                    //GameObject go = Instantiate(square, square.transform.position, square.transform.rotation);
                    //Destroy(go, 0.55f);
                    // pieceExtra.Add(go);
                    //square.GetComponent<SpriteRenderer>().enabled = false;
                    if (square != null)
                    {
                        #region Checking Conditions
                        if (x == 0 && grid[x + 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                        }
                        else if (x == width - 1 || x == 0)
                        {
                            square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                        }
                        else if (x == 0 && grid[x + 1, k] == null || (grid[x + 1, k] == null && grid[x - 1, k] == null) || (x == width - 1 && grid[x - 1, k] == null))
                        {
                            square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                        }
                        else if (x == width - 1 && grid[x - 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x - 1, k].position.x - 1, grid[x - 1, k].position.y);
                        }
                        else if (grid[x + 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                        }
                        else if (grid[x - 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x - 1, k].position.x + 1, grid[x - 1, k].position.y);
                        }
                        else
                        {
                            Debug.Log("None of them is being Executed");
                        }
                        #endregion

                        pos = Round(square.transform.position);
                        x = (int)pos.x;                      //Use this X to transit
                        y = (int)pos.y;                      //Use this Y to transit



                        pieceNumber.Add(y);
                        UpdateIndividualGrid(square.transform);
                    }
                    yield return new WaitForSeconds(0.3f);
                   // if (k - 1 >= 0)
                   // {


                    if ( k - 1 < 0)
                    {
                        FinishClearing();
                        break;
                    }

                    if (grid[x, k - 1] != null)
                    {
                            FinishClearing();
                            break;
                    }
                  //  }

                }
            }

        }

        //DO the transition work from here
        if (pieceExtra != null && pieceExtra.Count > 0)
        {
            StartCoroutine(TransitToPosition(pieceExtra[0], new Vector2(square.transform.position.x, square.transform.position.y)));
            pieceExtra.Clear();
            StartCoroutine(EnableCheckingPhase(x, y));
        }
    }
    
    /*public void SettlePeicesinEmpty()
    {
        pieceNumber.Clear();
        pieceExtra.Clear();

        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, j] != null)
                {
                    GameObject square = grid[i, j].gameObject;
                    Vector2 pos = Round(square.transform.position);
                    int x = (int)(pos.x);
                    int y = (int)(pos.y);
                    if (CheckIfInsideGrid(square.transform.position))
                    {
                        for (int k = y - 1; k >= 0; k--)
                        {
                            if (grid[x, k] == null)
                            {

                                //Then Go Down, becuase you see? There is a fucking empty space :/

                                GameObject go = Instantiate(square, square.transform.position, square.transform.rotation);
                                Destroy(go, 0.55f);
                                pieceExtra.Add(go);
                                square.GetComponent<Renderer>().enabled = false;

                                #region Checking Conditions
                                if (x == 0 && grid[x + 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                                }
                                else if (x == width - 1 || x == 0)
                                {
                                    square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                                }
                                else if (x == 0 && grid[x + 1, k] == null || (grid[x + 1, k] == null && grid[x - 1, k] == null) || (x == width - 1 && grid[x - 1, k] == null))
                                {
                                    square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                                }
                                else if (x == width - 1 && grid[x - 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x - 1, k].position.x - 1, grid[x - 1, k].position.y);
                                }
                                else if (grid[x + 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                                }
                                else if (grid[x - 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x - 1, k].position.x + 1, grid[x - 1, k].position.y);
                                }
                                else
                                {
                                    Debug.Log("None of them is being Executed");
                                }
                                #endregion

                                pos = Round(square.transform.position);
                                x = (int)pos.x;                      //Use this X to transit
                                y = (int)pos.y;                      //Use this Y to transit



                                pieceNumber.Add(y);
                                UpdateIndividualGrid(square.transform);
                            }
                        }

                    }

                    //DO the transition work from here
                    if (pieceExtra != null && pieceExtra.Count > 0)
                    {
                        square.transform.position = new Vector3((int)square.transform.position.x + 0.15f, (int)square.transform.position.y + 0.2f);
                        StartCoroutine(TransitToPosition(pieceExtra[0], new Vector2(square.transform.position.x, square.transform.position.y)));
                        pieceExtra.Clear();
                        StartCoroutine(EnableCheckingPhase(x, y));
                    }
                }
            }
        }
        //    if(tempMatch.Count == 0)
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;

    }*/

    int spe2 = 0;
    public void SettlePeicesinEmptyAfterExplosion()
    {
        AlignPieces();
        spe2 = 0;
        pieceNumber.Clear();
        pieceExtra.Clear();
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {

                StartCoroutine(SettlePiecesinEmptyWaitAfterExplosion(i, j));

                if (spe2 == 0 && i == width - 1 && j == height - 1)
                {
                    AlignPieces();
                    FinishClearingWithoutComboreset();
                }
            }
        }
        //    if(tempMatch.Count == 0)
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;

    }
    IEnumerator SettlePiecesinEmptyWaitAfterExplosion(int i, int j)
    {
        if (grid[i, j] == null)
        {
            yield break;
        }
        GameObject square = grid[i, j].gameObject;
        Vector2 pos = Round(square.transform.position);
        int x = (int)(pos.x);
        int y = (int)(pos.y);
        if (CheckIfInsideGrid(square.transform.position))
        {
            for (int k = y - 1; k >= 0; k--)
            {
                if (grid[x, k] == null)
                {
                    spe2++;
                    //Then Go Down, becuase you see? There is a empty space :/
                    //GameObject go = Instantiate(square, square.transform.position, square.transform.rotation);
                    //Destroy(go, 0.55f);
                    // pieceExtra.Add(go);
                    //square.GetComponent<SpriteRenderer>().enabled = false;
                    if (square != null)
                    {
                        #region Checking Conditions
                        if (x == 0 && grid[x + 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                        }
                        else if (x == width - 1 || x == 0)
                        {
                            square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                        }
                        else if (x == 0 && grid[x + 1, k] == null || (grid[x + 1, k] == null && grid[x - 1, k] == null) || (x == width - 1 && grid[x - 1, k] == null))
                        {
                            square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                        }
                        else if (x == width - 1 && grid[x - 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x - 1, k].position.x - 1, grid[x - 1, k].position.y);
                        }
                        else if (grid[x + 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                        }
                        else if (grid[x - 1, k] != null)
                        {
                            square.transform.position = new Vector2(grid[x - 1, k].position.x + 1, grid[x - 1, k].position.y);
                        }
                        else
                        {
                            Debug.Log("None of them is being Executed");
                        }
                        #endregion

                        pos = Round(square.transform.position);
                        x = (int)pos.x;                      //Use this X to transit
                        y = (int)pos.y;                      //Use this Y to transit



                        pieceNumber.Add(y);
                        UpdateIndividualGrid(square.transform);
                    }
                    yield return new WaitForSeconds(0.3f);
                   // if (k - 1 >= 0)
                   // {
                        if (k - 1 < 0)
                        {
                            FinishClearingWithoutComboreset();
                            break;
                        }

                        if (grid[x, k - 1] != null)
                        {
                            FinishClearingWithoutComboreset();
                            break;
                        }

                    //}

                }
            }

        }

        //DO the transition work from here
        if (pieceExtra != null && pieceExtra.Count > 0)
        {
            StartCoroutine(TransitToPosition(pieceExtra[0], new Vector2(square.transform.position.x, square.transform.position.y)));
            pieceExtra.Clear();
            StartCoroutine(EnableCheckingPhase(x, y));
        }
    }
    private IEnumerator EnableCheckingPhase(int x, int y)
    {
        yield return new WaitForSeconds(0.5f);
        if (grid[x, y] != null)
            grid[x, y].GetComponent<Renderer>().enabled = true;
        yield return new WaitForSeconds(0.1f);
        FinishClearing();
        //   StopAllCoroutines();

    }
    public void FinishClearingWithoutComboreset()
    {
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                ClearAllMatches(i, j);
            }
        }
    }
    public void FinishClearing()
    {
        combotestcounter=0;
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                ClearAllMatches(i, j);
            }
        }
      //  SettlePeicesinEmpty();
    }

    public bool CheckForStack(PieceController piece)
    {
        bool status = false;
        int count = 0;

        for (int i = 0; i < 4; i++)
        {
            Transform tempG = piece.transform.GetChild(i);
            Vector2 pos = Round(tempG.position);
            if ((int)pos.y < 1.2f)
            {
                count++;
                if (count == 4)
                {
                    status = true;
                    return status;
                }
            }
            else
            {
                if (grid[(int)pos.x, (int)pos.y - 1] != null)
                {
                    status = true;
                }
                else
                {
                    status = false;
                }
            }
        }
        Debug.Log("Status Checked With Code " + status + " And the Damn Count is " + count);

        return status;
    }
    public void AlignPieces()
    {
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, j] != null)
                {
                    GameObject piece = grid[i, j].gameObject;
                    piece.transform.position = new Vector3((int)piece.transform.position.x, (int)piece.transform.position.y, (int)piece.transform.position.z);
                }
            }
        }
    }


    public void GameWinClear()
    {
        // GameWinShow.SetActive(true);
        StartCoroutine(GameWinClearWait());
        StartCoroutine(GameWinClearWaitReverse());

    }
    IEnumerator GameWinClearWait()
    {
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, j] != null)
                {
                    yield return new WaitForSecondsRealtime(0.06f);
                    GameObject winparticle = Instantiate(WinParticle, grid[i, j].transform.position, Quaternion.identity);
                    foreach (var item in winparticle.GetComponentsInChildren<Transform>())
                    {
                        ParticleSystem.MainModule ps = item.GetComponent<ParticleSystem>().main;
                        if (grid[i, j].CompareTag("Blue"))
                        {
                            Color32 cl = new Color32(114, 249, 253, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Yellow"))
                        {
                            Color32 cl = new Color32(255, 255, 92, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Red"))
                        {
                            Color32 cl = new Color32(255, 59, 42, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Green"))
                        {
                            Color32 cl = new Color32(0, 249, 53, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                    }
                    //ps.startColor = grid[i, j].GetComponent<SpriteRenderer>().color;
                    if (grid[i, j] != null)
                    {
                        Destroy(grid[i, j].gameObject);
                    }

                    Destroy(winparticle, 3f);
                }
            }
        }
    }
    IEnumerator GameWinClearWaitReverse()
    {
        for (int j = height - 1; j >= 0; j--)
        {
            for (int i = width - 1; i >= 0; i--)
            {
                if (grid[i, j] != null)
                {
                    yield return new WaitForSecondsRealtime(0.06f);
                    GameObject winparticle = Instantiate(WinParticle, grid[i, j].transform.position, Quaternion.identity);
                    foreach (var item in winparticle.GetComponentsInChildren<Transform>())
                    {
                        ParticleSystem.MainModule ps = item.GetComponent<ParticleSystem>().main;
                        if (grid[i, j].CompareTag("Blue"))
                        {
                            Color32 cl = new Color32(114, 249, 253, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Yellow"))
                        {
                            Color32 cl = new Color32(255, 255, 92, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Red"))
                        {
                            Color32 cl = new Color32(255, 59, 42, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Green"))
                        {
                            Color32 cl = new Color32(0, 249, 53, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                    }
                    //ps.startColor = grid[i, j].GetComponent<SpriteRenderer>().color;
                    if (grid[i, j] != null)
                    {
                        Destroy(grid[i, j].gameObject);
                    }

                    Destroy(winparticle, 3f);
                }
            }
        }
    }
    public void LoseReWardClear()
    {
        StartCoroutine(LoseRewardWait());
    }
    IEnumerator LoseRewardWait()
    {
        for (int j = (height / 2) - 1; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, j] != null)
                {
                    yield return new WaitForSecondsRealtime(0.03f);
                    GameObject winparticle = Instantiate(WinParticle, grid[i, j].transform.position, Quaternion.identity);
                    foreach (var item in winparticle.GetComponentsInChildren<Transform>())
                    {
                        ParticleSystem.MainModule ps = item.GetComponent<ParticleSystem>().main;
                        if (grid[i, j].CompareTag("Blue"))
                        {
                            Color32 cl = new Color32(114, 249, 253, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Yellow"))
                        {
                            Color32 cl = new Color32(255, 255, 92, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Red"))
                        {
                            Color32 cl = new Color32(255, 59, 42, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                        else if (grid[i, j].CompareTag("Green"))
                        {
                            Color32 cl = new Color32(0, 249, 53, 255);
                            ps.startColor = new ParticleSystem.MinMaxGradient(cl);
                        }
                    }
                    Destroy(grid[i, j].gameObject);
                    Destroy(winparticle, 3f);

                }
            }
        }
    }
}
