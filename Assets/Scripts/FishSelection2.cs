﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSelection2 : MonoBehaviour
{
    
    float timeCounter = 0f;
    float rotate = 0f;
    public GameObject[] Objects;
    public int selectedFish = 0;
    private string selectedFishDataName = "selectedFish";

    // Start is called before the first frame update
    void Start()
    {

        HideAllFishes();

        selectedFish = PlayerPrefs.GetInt(selectedFishDataName, 0);

        Objects[selectedFish].SetActive(true);
        InvokeRepeating("FishSelect", 5, 5);

    }

    private void HideAllFishes()
    {
        foreach (GameObject g in Objects)
        {
            g.SetActive(false);
        }
    }
    public void FishSelect()
    {
        Objects[selectedFish].SetActive(false);
        selectedFish++;
        if (selectedFish >= Objects.Length)
        {
            selectedFish = 0;
        }
        Objects[selectedFish].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        float ms = (AudioPeer.amplitude * 2f);
        timeCounter += Time.deltaTime;        
        float x = Mathf.Cos(timeCounter);
        float y = Mathf.Sin(timeCounter);
        foreach (GameObject g in Objects)
        {
            g.transform.position = new Vector3(-(x*2f),-(y*2f), y*ms*4);
            g.transform.rotation = Quaternion.Euler(y * 30f, 305, 0);
        }
    }
}
