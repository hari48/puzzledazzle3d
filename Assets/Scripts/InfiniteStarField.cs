﻿using UnityEngine;
using System.Collections;

public class InfiniteStarField : MonoBehaviour
{
	private Transform tx;
	private ParticleSystem.Particle[] points;

	public int starsMax = 100;
	public float starSize = 1;
	public float starDistance = 10;
	public float starClipDistance = 1;
	private float starDistanceSqr;
	private float starClipDistanceSqr;

	public Camera cam;
	public float moveSpeed = 10;

	public AudioVisualizer.AudioFileEventListener audio;


	// Use this for initialization
	void Start()
	{
		tx = transform;
		starDistanceSqr = starDistance * starDistance;
		starClipDistanceSqr = starClipDistance * starClipDistance;
	}


    private void OnEnable()
    {
		StartCoroutine(DelayedBeats());
	//	AudioVisualizer.AudioFileEventListener.OnBeatRecognized += MakeStarsLight;
	}

	private IEnumerator DelayedBeats()
    {
		yield return new WaitForSeconds(1f);
		audio.gameObject.SetActive(true);
		AudioVisualizer.AudioFileEventListener.OnBeatRecognized += MakeStarsBright;
	}

    private void CreateStars()
	{
		points = new ParticleSystem.Particle[starsMax];

		for (int i = 0; i < starsMax; i++) {
			points[i].position = Random.insideUnitSphere * starDistance + tx.position;
			points[i].startColor = new Color(0, 230f/255,1, 1);
			points[i].startSize = starSize;
			points[i].velocity = new Vector3(0,0, 10);
		}
	}

    private void FixedUpdate()
    {
		float speed = moveSpeed * (AudioPeer.amplitude * 2);
		cam.transform.position += Vector3.forward * speed * Time.deltaTime;
	


	}

	public void MakeStarsBright(AudioVisualizer.Beat beat)
    {
		GetComponent<ParticleSystemRenderer>().material.EnableKeyword("_EMISSION");
		GetComponent<ParticleSystemRenderer>().material.SetColor("_EmissionColor", new Color(0, 230f / 255, 1, 1));
		StartCoroutine(MakeStarsLight());
	}

	public IEnumerator MakeStarsLight()
	{
		yield return new WaitForSeconds(0.4f);
		GetComponent<ParticleSystemRenderer>().material.DisableKeyword("_EMISSION");
	//	GetComponent<ParticleSystemRenderer>().material.SetColor("_EmissionColor", Color.white);
	}

	// Update is called once per frame
	void Update()
	{
		if (points == null) CreateStars();

		for (int i = 0; i < starsMax; i++) {

			if ((points[i].position - tx.position).sqrMagnitude > starDistanceSqr) {
				points[i].position = Random.insideUnitSphere.normalized * starDistance + tx.position;
			}

			if ((points[i].position - tx.position).sqrMagnitude <= starClipDistanceSqr) {
				float percent = (points[i].position - tx.position).sqrMagnitude / starClipDistanceSqr;
				points[i].startColor = new Color(1, 1, 1, percent);
				points[i].startSize = percent * starSize;
			}


		}



		// older versions of Unity
		// particleSystem.SetParticles ( points, points.Length );
		// Unity 5.4+ and probably some sooner versions
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);

	}
}