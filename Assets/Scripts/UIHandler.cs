﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEditor;
using DG.Tweening;
using UnityEngine.Analytics;

public class UIHandler : MonoBehaviour
{
    //Static Variable
    public static UIHandler instance;

    [Header("Result")]
    public Sprite retry_ON;
    public Sprite retry_OFF;
    public TextMeshProUGUI score_game;
    public TextMeshProUGUI highScore_game;
    public TextMeshProUGUI score_result;
    public TextMeshProUGUI highscore_result;
    public TextMeshProUGUI cleared_UI;

    [Header("GameObjects")]
    //Unity Objects
    public GameObject MainMenu;
    public GameObject HelpPanel;
    public GameObject ThemesPanel;
    public GameObject Options;
    public GameObject Quit;
    public GameObject Paused;
    public GameObject GamePlay;
    public GameObject EndDemo;
    public GameObject ResultWin;
    public GameObject ResultLose;

    public int isSoundOn;
    public int isMusicOn;
    public int isVibrationOn;

    public GameObject popup;

    public Button SelectedButton;

    public TextMeshProUGUI[] coinsText;
    public TextMeshProUGUI targettoAchieve;
    public InputField nameI;


    public string levelNameToLoad = "MagicOrb";
    public TextMeshProUGUI mainmenuHighScore;


    //[SerializeField]
    //GameObject LosePopup;
    //[SerializeField]
   // GameObject LosePopupTimer;
    [SerializeField]
    TMP_Text Winlvltxt;

    [SerializeField]
    GameObject PauseButton;
    [SerializeField]
    GameObject PowerupPanel;
    [SerializeField]
    GameObject timesUPPopup;
    [SerializeField]
    TMP_Text LoseScreeninfoText;
    [SerializeField]
    GameObject LastWinScreen;

    private void Awake()
    {
        //Initializing instance of this script
        if(instance == null)
        {
            instance = this;
        }

  //      if(SelectedButton != null)
   //     SelectedButton.Select();
     
    }

    public void LoadLevelWithBubbleName()
    {
        SceneManager.LoadScene(levelNameToLoad);
    }

    public void LoadLevel(int level)
    {
        LevelManager.current.LoadLevel(level);
    }

    private void Start()
    {
        isSoundOn = PlayerPrefs.GetInt("sound", 1);
        isMusicOn = PlayerPrefs.GetInt("music", 1);
        isVibrationOn = PlayerPrefs.GetInt("vibration", 1);

     /*   if (SceneManager.GetActiveScene().name == "Menu")
        {
            int l1 = PlayerPrefs.GetInt("levelsCompleted_Magic Orb" + SceneManager.GetActiveScene().name, 0);
            int l2 = PlayerPrefs.GetInt("levelsCompleted_Snowfall" + SceneManager.GetActiveScene().name, 0);
            int l3 = PlayerPrefs.GetInt("levelsCompleted_Into The Ocean" + SceneManager.GetActiveScene().name, 0);

            int showedDemoPage = PlayerPrefs.GetInt("showedDemoPage", 0);

            if (l1 == 1 && l2 == 1 && l3 == 1 && showedDemoPage == 0)
                UIHandler.instance.ShowEndDemoPage();
        }*/

        UpdateCoins(0);

        if (LevelManager.current.currentLevel > 0)
        {
            if (Winlvltxt != null)
            {
                Winlvltxt.text = "Level " + LevelManager.current.currentLevel;
            }

        }
    }

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape) && SceneManager.GetActiveScene().name == "Menu")
        {
            OnPressedQuit();
        }
    }
    private IEnumerator LoadSceneWithDelay(int level)
    {
        yield return new WaitForSeconds(0.55f);
        SceneManager.LoadScene(level);
    }

    private IEnumerator LoadSceneWithDelay(string level)
    {
        yield return new WaitForSeconds(0.55f);
        SceneManager.LoadScene(level);
    }

    #region Menu Buttons
    public void OnPressedPlay()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        AudioHandler.Instance.FadeAudio();

        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);

        StartCoroutine(LoadSceneWithDelay(leveltoload));

    }



    public void OnPressedPlayWithModeName(string modeName)
    {

        if (modeName == "Survival")
        {
            PlayerPrefs.SetInt("isSurvival", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isSurvival", 0);
        }
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        AudioHandler.Instance.FadeAudio();
        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);
        StartCoroutine(LoadSceneWithDelay(leveltoload));

    }

    public void OnPressedPlayWithModeNameNoLoad(string modeName)
    {

        if (modeName == "Survival")
        {
            PlayerPrefs.SetInt("isSurvival", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isSurvival", 0);
        }

        Camera.main.transform.DOMove(new Vector3(-50.91f, 37.22f, -9.79f), 1f);
    //    AudioHandler.Instance.FadeAudio();
        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);
    //    StartCoroutine(LoadSceneWithDelay(leveltoload));

    }

    public void OnPressedBackWithModeNameNoLoad(string modeName)
    {

        if (modeName == "Survival")
        {
            PlayerPrefs.SetInt("isSurvival", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isSurvival", 0);
        }

        Camera.main.transform.DOMove(new Vector3(-244.1155f, 28.97977f, 80.65014f), 1f);
        //    AudioHandler.Instance.FadeAudio();
        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);
        //    StartCoroutine(LoadSceneWithDelay(leveltoload));

    }


    public void OnPressedPlayPotrait()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        AudioHandler.Instance.FadeAudio();

        StartCoroutine(LoadSceneWithDelay("Level_Portrait"));

    }

    public void LoadLevelWithName(string name)
    {
        StartCoroutine(LoadSceneWithDelay(name));
    }

    public void ControlPauseAndPowerupsUI(bool value)
    {
        PauseButton.SetActive(value);
        PowerupPanel.SetActive(value);
    }

    public void OnPressedTheme()
    {

      //  currentButton.GetComponent<Image>().sprite = options_ON;     enable this later
        Invoke("EnableThemesPanel", 0.1f);
    }

    public void EnableThemesPanel()
    {
        UpdateCoins(0);
        ThemesPanel.SetActive(true);
        MainMenu.SetActive(false);
    }

    public void OnPressedOptions()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        Invoke("EnableOptionsPanel", 0.1f);
    }

    public void EnableOptionsPanel()
    {
        MainMenu.SetActive(false);
        Options.SetActive(true);

        Button[] buttons = Options.GetComponentsInChildren<Button>();

        foreach(Button button in buttons)
        {
            if(button.name == "Back")
            {
           //     button.Select();
            }
        }
    }

    public void OnPressedHelp()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        Invoke("EnableHelpPanel", 0.1f);
    }

    public void EnableHelpPanel()
    {
        MainMenu.SetActive(false);
        HelpPanel.SetActive(true);

        Button btn = HelpPanel.GetComponentInChildren<Button>();
    //    btn.Select();
    }

    public void OnPressedQuit()
    {


        Invoke("EnableQuitPanel", 0.1f);
    }

    public void EnableQuitPanel()
    {
        MainMenu.SetActive(false);
        Quit.SetActive(true);

        Button[] buttons = Quit.GetComponentsInChildren<Button>();


    //    buttons[0].Select();
    }
    #endregion

    #region Option Buttons
    
    public void OnPressedSound()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        if (isSoundOn == 1)
        {
            isSoundOn = 0;
            PlayerPrefs.SetInt("sound", 0);
        }
        else
        {
            isSoundOn = 1;
            PlayerPrefs.SetInt("sound", 1);
        }
    }

    public void OnPressedMusic()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        if (isMusicOn == 1)
        {
            isMusicOn = 0;
            PlayerPrefs.SetInt("music", 0);
            AudioHandler.Instance.mainAudioSource.enabled = false;
        }
        else
        {
            isMusicOn = 1;
            PlayerPrefs.SetInt("music", 1);
            AudioHandler.Instance.mainAudioSource.enabled = true;
        }
    }

    public void OnPressedVibration()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        if (isVibrationOn == 1)
        {
            isVibrationOn = 0;
            PlayerPrefs.SetInt("vibration", 0);
            
        }
        else
        {
            isVibrationOn = 1;
            PlayerPrefs.SetInt("vibration", 1);
        }
    }

    public void OnPressedBackResult()
    {
        LevelManager.current.LoadLevel(0);
        /*
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        AudioHandler.Instance.FadeAudio();
        Invoke("GoBackToMenu", 0.55f);
        */
    }

    public void OnPressedNextLevel()
    {
        LevelManager.current.LoadLevel(LevelManager.current.currentLevel + 1);
    }

    public void OnPressedBackTheme()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        Invoke("GoBackToMenu", 0.1f);
    }
    public void OnPressedBack()
    {
        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }

        if (SceneManager.GetActiveScene().name == "Menu_portrait" || SceneManager.GetActiveScene().name == "Menu")
            Invoke("GoBackToMenu", 0.1f);
        else
        {
            SceneManager.LoadScene("Menu");
            isSoundOn = PlayerPrefs.GetInt("sound", 1);
            isMusicOn = PlayerPrefs.GetInt("music", 1);
        }

    }

    public void PressedBack()
    {
        MainMenu.SetActive(true);
        Options.SetActive(false);
        Quit.SetActive(false);
        Paused.SetActive(false);
    }

    private void GoBackToMenu()
    {
        SceneManager.LoadScene("Menu");
        isSoundOn = PlayerPrefs.GetInt("sound", 1);
        isMusicOn = PlayerPrefs.GetInt("music", 1);

        MainMenu.SetActive(true);
        Options.SetActive(false);
        Quit.SetActive(false);
        Paused.SetActive(false);
        if (HelpPanel != null)
            HelpPanel.SetActive(false);
        if (ThemesPanel != null)
            ThemesPanel.SetActive(false);
    }
    #endregion

    #region Quit Buttons
    public void OnPressedQuitYes()
    {
 
       StartCoroutine(QuitApplication());
    }

    private IEnumerator QuitApplication()
    {
        UIHandler.instance.ShowEndDemoPage();
        yield return new WaitForSeconds(15f);
        Application.Quit();
    }

    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(1);
    }

    public void OnPressedQuitNo()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        Invoke("OnPressedBack", 0.1f);
    }

    #endregion

    #region Paused Buttons

    public void OnPressedPause()
    {
        Time.timeScale = 0;
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = false;
        AudioHandler.Instance.isPaused = true;
        AudioHandler.Instance.mainAudioSource.Pause();
        GamePlay.SetActive(false);
        Paused.SetActive(true);

        Button[] b = Paused.GetComponentsInChildren<Button>();
   
      //  b[0].Select();

        UpdateScoreUI();
    }
    public void OnPressedResume()
    {
        Time.timeScale = 1;
        AudioHandler.Instance.isPaused = false;
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;
        AudioHandler.Instance.mainAudioSource.UnPause();
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;
        Invoke("EnableResume", 0.1f);
    }

    private void EnableResume()
    {

        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        GamePlay.SetActive(true);
        Paused.SetActive(false);
        if (!currentButton.GetComponent<Button>())
            return;



        Button[] buttons = Paused.GetComponentsInChildren<Button>();
    }
    public void OnPressedRestart()
    {
        Time.timeScale = 1;
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        Invoke("RestartGame", 0.1f);
    }

    private void RestartGame()
    {
        // SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        LevelManager.current.LoadLevel(LevelManager.current.currentLevel);
    }

    public void OnPressedRetry()
    {

        LevelManager.current.LoadLevel(LevelManager.current.currentLevel);
        /*
        Time.timeScale = 1;
        Invoke("RestartGame", 0.1f);*/
    }
    #endregion

    #region Result Buttons

    public void ShowResult(int resultvalue)
    {
        GameManager.instance.isGameover = true;

        Analytics.CustomEvent("Lost Level", new Dictionary<string, object>
        {
            {"Level Name", SceneManager.GetActiveScene().name}
        });

        Analytics.CustomEvent("Score Lost Level", new Dictionary<string, object>
        {
            {"Level Name", SceneManager.GetActiveScene().name},
               {"Score", ScoreManager.instance.score}
        });
        // Time.timeScale = 0;
        /*
        if (GameManager.instance.currentPiece.GetComponent<PieceController>() != null)
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = false;
        */
        FindObjectOfType<GameManager>().enabled = false;
        GamePlay.SetActive(false);
        switch (resultvalue)
        {
            case 1:
                ResultWin.SetActive(true);
                break;
            case 2:
                ResultLose.SetActive(true);
                break;
            case 3:
                ResultLose.SetActive(true);
                LoseScreeninfoText.text = "Time's Up";
                break;
            case 4:
                LastWinScreen.SetActive(true);
                break;
        }
       // Result.SetActive(true);
        Debug.Log("MidRoll is working");

#if UNITY_IOS
        if (ScoreManager.instance.score >= 60000  && PlayerPrefs.GetInt("reviewRequest", 0) == 0 && GameManager.instance.isGameover)
        {
            PlayerPrefs.SetInt("reviewRequest", 1);
            UnityEngine.iOS.Device.RequestStoreReview();
        }
#endif
#if UNITY_IOS
        StartCoroutine(IOSNotification.instance.RequestAuthorization());
        //      Jio_AdsManager.Instance.Jio_MID_Roll.showAd();
        //       Jio_AdsManager.Instance.Jio_MID_Roll.cacheAd();

        Leaderboards.instance.FetchScoresFromGameCentre();
#endif

       // Button[] button = Result.GetComponentsInChildren<Button>();
   //     button[0].Select();
    }
#endregion

    public void UpdateScoreUI()
    {
       /* if (ScoreManager.instance.score < 10)
        {
            score_game.text = "0" + ScoreManager.instance.score;
            score_result.text = "0" + ScoreManager.instance.score;
        //    cleared_UI.text = "0" + ScoreManager.instance.cleared;
          
        }
        else
        {
            score_game.text = ScoreManager.instance.score.ToString();
            score_result.text = ScoreManager.instance.score.ToString();
      //      cleared_UI.text = ScoreManager.instance.cleared.ToString();

        }
        if(targettoAchieve != null)
        if(FindObjectOfType<LevelUniqueIdentifier>().levelTarget < 10)
        {
            targettoAchieve.text = "0" + FindObjectOfType<LevelUniqueIdentifier>().levelTarget;
        }
        else
        {
            targettoAchieve.text = "" + FindObjectOfType<LevelUniqueIdentifier>().levelTarget;
        }

        if (ScoreManager.instance.highScore < 10)
        {
            highScore_game.text = "0" + ScoreManager.instance.highScore;
            highscore_result.text = "0" + ScoreManager.instance.highScore;
        }
        else
        {
            highScore_game.text = ScoreManager.instance.highScore.ToString();
            highscore_result.text = ScoreManager.instance.highScore.ToString();
        }

       // coinsUI.text = GameManager.instance.Coins.ToString();

        */
    }

    public void UpdateCoins(int amount)
    {
        int coins = PlayerPrefs.GetInt("coins", 0);
        coins += amount;

        foreach(TextMeshProUGUI t in coinsText)
        {
            t.text = coins.ToString();
        }

        PlayerPrefs.SetInt("coins", coins);
    }

    public void SetCharacterName()
    {
        if(nameI.text != null)
        {
            PlayerPrefs.SetString("PlayerName", nameI.text);
        }
    }

    public void ShowEndDemoPage()
    {
        EndDemo.SetActive(true);
        PlayerPrefs.SetInt("showedDemoPage", 1);
    }

    public void SetDifficultyFromMenu(string mode)
    {
        mode = mode.ToLower();
        switch (mode)
        {
            case "easy":
                PlayerPrefs.SetInt("gamedifficulty", 0);
                break;
            case "medium":
                PlayerPrefs.SetInt("gamedifficulty", 1);
                break;
            case "hard":
                PlayerPrefs.SetInt("gamedifficulty", 2);
                break;
            default:
                break;
        }
    }

    public void ShowTimesUPPopup()
    {
        StartCoroutine(TimesUPPopup());
    }
    IEnumerator TimesUPPopup()
    {
        timesUPPopup.SetActive(true);
        yield return new WaitForSecondsRealtime(2f);
        timesUPPopup.SetActive(false);
    }

    public void SetGameplayUI(bool value)
    {
        GamePlay.SetActive(value);
    }
}
