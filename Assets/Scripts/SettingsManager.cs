﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager instance;

    public Slider musicSlider;
    public Slider sfxSlider;

    public TMP_Dropdown resolution_dropdown;
    public Toggle antiAliasingToggle, fullscreenToggle;

    public TMP_Dropdown fps_dropdown;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        SetResolutionDropdown();
        LoadStartupValues();

        if (PlayerPrefs.GetInt("FPS", -1) == -1)
        {
            fps_dropdown.value = 0;
        }
        else if (PlayerPrefs.GetInt("FPS", -1) == 30)
        {
            fps_dropdown.value = 1;
        }
        else if (PlayerPrefs.GetInt("FPS", -1) == 60)
        {
            fps_dropdown.value = 2;
        }
    }

    private void LoadStartupValues()
    {
        float musicValue = PlayerPrefs.GetFloat(StringValueHolder.musicVolumePrefs, 1);
        Debug.Log("Music Volume Set to " + musicSlider.value);
        musicSlider.value = musicValue;
        AudioHandler.Instance.mainAudioSource.volume = musicSlider.value;


        float sfxValue = PlayerPrefs.GetFloat(StringValueHolder.sfxVolumePrefs, 1);
        Debug.Log("SFX Volume Set to " + sfxSlider.value);
        sfxSlider.value = sfxValue;
        AudioHandler.Instance.mainAudioSource.volume = sfxSlider.value;
    }

    private void SetResolutionDropdown()
    {
        Resolution[] resolutions = Screen.resolutions;
        List<string> optionString = new List<string>(); 

        for(int i = 0; i < resolutions.Length; i++)
        {
            Resolution res = resolutions[i];
            optionString.Add(res.ToString());
        }

        resolution_dropdown.AddOptions(optionString);

        for (int i = 0; i < resolutions.Length; i++)
        {
            Resolution res = resolutions[i];

            if (res.ToString() == Screen.currentResolution.ToString())
            {
                Debug.Log(res.ToString() + " == " + Screen.currentResolution);
                resolution_dropdown.value = i;
              //  resolution_dropdown.RefreshShownValue();
            }
        }
    }

    public void OnResolutionValueChanged()
    {
        Resolution[] resolutions = Screen.resolutions;
        string selectedRes = resolution_dropdown.options[resolution_dropdown.value].text;
        for (int i = 0; i < resolutions.Length; i++)
        {
            Resolution res = resolutions[i];

            if (res.ToString() == selectedRes)
            {
                Debug.Log("Resolution Changed to = " + res.ToString());

                int isFullscreen = PlayerPrefs.GetInt(StringValueHolder.fullscreenPrefs, 1);
                bool status;
                if (isFullscreen == 1)
                    status = true;
                else
                    status = false;
                
                Screen.SetResolution(res.width, res.height, status);
            }
        }

    }

    public void OnFpsValueChanged()
    {
        if (fps_dropdown.value == 0)
        {
            Application.targetFrameRate = -1;
            PlayerPrefs.SetInt("FPS", -1);
        }
        else if (fps_dropdown.value == 1)
        {
            Application.targetFrameRate = 30;
            PlayerPrefs.SetInt("FPS", 30);
        }
        else if (fps_dropdown.value == 2)
        {
            Application.targetFrameRate = 60;
            PlayerPrefs.SetInt("FPS", 60);
        }
    }

    public void OnMusicSliderChanged()
    {
        PlayerPrefs.SetFloat(StringValueHolder.musicVolumePrefs, musicSlider.value);
        Debug.Log("Music Volume Set to " + musicSlider.value);
        AudioHandler.Instance.mainAudioSource.volume = musicSlider.value;
    }

    public void OnSfxSliderChanged()
    {
        PlayerPrefs.SetFloat(StringValueHolder.sfxVolumePrefs, sfxSlider.value);
        Debug.Log("SFX Volume Set to " + sfxSlider.value);
        AudioHandler.Instance.mainAudioSource.volume = sfxSlider.value;
    }

    public void OnAntialiasingValueChanged()
    {
        if(antiAliasingToggle.isOn)
        {
            QualitySettings.antiAliasing = 2;
        }
        else
        {
            QualitySettings.antiAliasing = 0;
        }

        PlayerPrefs.SetInt(StringValueHolder.antiAliasingPrefs, 0);
    }

    public void OnFullScreenValueChanged()
    {
        if(fullscreenToggle.isOn)
        {
            Screen.fullScreen = true;
            // Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            PlayerPrefs.SetInt(StringValueHolder.fullscreenPrefs, 1);
        }
        else
        {
            Screen.fullScreen = false;
            PlayerPrefs.SetInt(StringValueHolder.fullscreenPrefs, 0);
        }



    }
}
