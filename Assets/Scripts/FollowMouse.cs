
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

 

public class FollowMouse : MonoBehaviour
{
    // =================================	
    // Nested classes and structures.
    // =================================

    // ...

    // =================================	
    // Variables.
    // =================================

    // ...
    public Camera cam;
    public float speed = 8.0f;
    public float distanceFromCamera = 5.0f;

    // =================================	
    // Functions.
    // =================================

    // ...

    void Awake()
    {

    }

    // ...

    void Start()
    {

    }

    // ...

    void FixedUpdate()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = distanceFromCamera;

        Vector3 mouseScreenToWorld = cam.ScreenToWorldPoint(mousePosition);

        Vector3 position = Vector3.Lerp(transform.position, mouseScreenToWorld, 1.0f - Mathf.Exp(-speed * Time.deltaTime));

        transform.position = position;
    }

    // ...

    void LateUpdate()
    {

    }

    // =================================	
    // End functions.
    // =================================

}

        // =================================	
        // End namespace.
        // =================================

   




// =================================	
// --END-- //
// =================================