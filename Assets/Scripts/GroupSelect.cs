using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupSelect : MonoBehaviour
{
    public GameObject[] Objects;
    public int selectedGroup = 0;
    private string selectedGroupDataName = "selectedGroup";

    // Start is called before the first frame update
    void Start()
    {

        HideAllGroups();

        selectedGroup = PlayerPrefs.GetInt(selectedGroupDataName, 0);

        Objects[selectedGroup].SetActive(true);
        InvokeRepeating("GroupSelection", 5, 5);

    }

    private void HideAllGroups()
    {
        foreach (GameObject g in Objects)
        {
            g.SetActive(false);
        }
    }
    public void GroupSelection()
    {
        Objects[selectedGroup].SetActive(false);
        selectedGroup++;
        if (selectedGroup >= Objects.Length)
        {
            selectedGroup = 0;
        }
        Objects[selectedGroup].SetActive(true);
    }
}
