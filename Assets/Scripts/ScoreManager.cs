﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    public int score, highScore, cleared, coins;
    public Image LoadingBar;

    public int earnedXp;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        GetSavedScore();

    }
    public void SaveScore()
    {
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "hs", highScore);
            LeaderBoardManager.current.UpdateScore(highScore);
        }
        coins = GameManager.instance.Coins;
        PlayerPrefs.SetInt("coins", coins);

        if (score >= 10000 &&(SteamManager.s_EverInitialized == true))
        {
            AchievmentManager.current.UnlockAchievment(8);
        }

        if (score >= 20000 && (SteamManager.s_EverInitialized == true))
        {
            AchievmentManager.current.UnlockAchievment(9);
        }

       
    }

    public void GetSavedScore()
    {
        score = 0;
        highScore = PlayerPrefs.GetInt(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "hs", 0);
        PlayerPrefs.GetInt("coins", 0);
    }

    public int ReturnHighScore()
    {
        return PlayerPrefs.GetInt(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "hs", 0);
    }

    public int ReturnCoins()
    {
        return PlayerPrefs.GetInt("coins", 0);
    }


    public void UpdateScore()
    {
        score += 50;
        cleared++;
        if (score % 200 == 0)
        {
            LoadingBar.fillAmount += 0.033f;
        }
        if (score % 1000 == 0)
        {
            GameManager.instance.UpdateCoins(100);
        }


        if (score % 2000 == 0 && score < 10000)
        {
            GameManager.instance.FallSpeed -= 0.08f;
        }



        if (cleared >= FindObjectOfType<LevelUniqueIdentifier>().levelTarget)
            FindObjectOfType<LevelUniqueIdentifier>().targetAchieved = true;

        SaveScore();

        UIHandler.instance.UpdateScoreUI();
    }
    public bool CheckLoadingBar(float a, float b)
    {
        if (LoadingBar.fillAmount > a && LoadingBar.fillAmount <= b)
        {
            return true;
        }
        else
            return false;


    }
    public void LBA1(float f)
    {
        if (LoadingBar.fillAmount > 0.33f)
        {
            LoadingBar.fillAmount -= f;
        }
    }
    public void LBA2(float f)
    {
        if (LoadingBar.fillAmount > 0.66f)
        {
            LoadingBar.fillAmount -= f;
        }
    }
    public void LBA3(float f)
    {
        if (LoadingBar.fillAmount > 1.0f)
        {
            LoadingBar.fillAmount -= f;
        }
    }



}