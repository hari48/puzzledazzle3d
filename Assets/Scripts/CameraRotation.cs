﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    [SerializeField] Camera Cam;  
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelUniqueIdentifier.instance.levelName == "Starfield")
        {
            Invoke("Flip", 62);
            Invoke("brake", 63);
            Invoke("Flip", 144);
            Invoke("brake", 146);
        }        
        if (LevelUniqueIdentifier.instance.levelName == "Journey")
        {
            Invoke("Flip", 47);
            Invoke("brake", 48);
            Invoke("Flip", 129);
            Invoke("brake", 131);

        }       
        if (LevelUniqueIdentifier.instance.levelName == "Blackhole")
        {
            Invoke("Flip", 67);
            Invoke("brake", 68);
            Invoke("Flip", 149);
            Invoke("brake", 150);

        }
        if (LevelUniqueIdentifier.instance.levelName == "Ocean World")
        {
            Invoke("Flip", 45);
            Invoke("brake", 46);
        }       
        if (LevelUniqueIdentifier.instance.levelName == "Cherry Blossom")
        {
            Invoke("Flip", 45);
            Invoke("brake", 46);
        }        
        if (LevelUniqueIdentifier.instance.levelName == "Butterfly")
        {
            Invoke("Flip", 91);
            Invoke("brake", 92);
            Invoke("Flip", 196);
            Invoke("brake", 198);
        }
    }
    public float rotationSpeed;
    public void Flip()
    {
        Vector3 rotation = Cam.transform.eulerAngles;
        if(rotation.z>=0)
        {
            rotation.z += rotationSpeed * Time.deltaTime;
            Cam.transform.eulerAngles = rotation;
        }
    }
    public void brake()
    {
        Cam.transform.rotation = Quaternion.identity;
    }
}
