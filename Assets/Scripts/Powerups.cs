﻿using System.Collections.Generic;
using UnityEngine;

/*Place this script on an object in the main scene (do it in game prefab so it is applied to all the scene),
 * and I also recommend doing all the powerups here instead of doing it in the grid system. Try to keep the gridsystem as clean as possible. 
 * */
public class Powerups : MonoBehaviour
{
    public static Powerups instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void ComboExplosionPowerup(PieceController piece) //I am calling this function from piececontroller after the piece has settled (before the children are removed)
    {
        if (!piece.isPowerup)    //you can define which powerup it is, is it combo, is it any other or if it is even a powerup. I just made a bool in piece controller to test the power up and enabling it manually from editor.
            return;

        List<GameObject> objectsToDestroy = new List<GameObject>();

        foreach (Transform child in piece.transform)
        {
            Vector2 pos = child.position;
            int x = (int)pos.x;
            int y = (int)pos.y;
            if (!objectsToDestroy.Contains(child.gameObject))          //Adding the default block from piece controller
                objectsToDestroy.Add(child.gameObject);
            
            if (x - 1 >= 0 && GridSystem.grid[x - 1, y] != null)      //left side : Making sure that neighbouring block is inside the grid in x negative axis and if even there? 
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x - 1, y].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x - 1, y].gameObject);
            }
            if (x + 1 < GridSystem.instance.width && GridSystem.grid[x + 1, y] != null)   //Right side : Making sure that neighbouring block is inside the grid in x positive axis and if even there?
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x + 1, y].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x + 1, y].gameObject);
            }
            if (y - 1 >= 0 && GridSystem.grid[x, y - 1] != null)      //down side : Making sure that neighbouring block is inside the grid in y negative axis and if even there?
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x, y - 1].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x, y - 1].gameObject);
            }
            if (y < GridSystem.instance.height && GridSystem.grid[x, y + 1] != null)      //Up side : Making sure that neighbouring block is inside the grid in y positive axis and if even there?
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x, y + 1].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x, y + 1].gameObject);
            }
        }

        /******************* This is just pseudo code of what to do with objects stored later, you should change it as per your preference *****************/
        //1. Delete all the objects you prefer to do them
        //2. Clear the Grid System
        //3. Call settle pieces in empty from gridsystem
        foreach (GameObject go in objectsToDestroy)
        {
            Destroy(go,2f);
            Rigidbody rb = go.AddComponent<Rigidbody>();
            rb.useGravity = false;
            rb.AddExplosionForce(600, rb.transform.position + (new Vector3(Random.Range(0, 2), Random.Range(-2, 2), 2)), 5, 0);
            rb.AddTorque(new Vector3(Random.Range(-500, 500), Random.Range(-500, 500), Random.Range(-500, 500)));
            GridSystem.grid[(int)go.transform.position.x, (int)go.transform.position.y] = null;
            ScoreManager.instance.UpdateScore();
            FindObjectOfType<SpawnMechanic>().SpawnPiece();
        }
        GridSystem.instance.SettlePeicesinEmpty();
    }
    public void ComboExplosionPowerup2(PieceController piece) //I am calling this function from piececontroller after the piece has settled (before the children are removed)
    {
        if (!piece.isPowerup2)    //you can define which powerup it is, is it combo, is it any other or if it is even a powerup. I just made a bool in piece controller to test the power up and enabling it manually from editor.
            return;

        List<GameObject> objectsToDestroy = new List<GameObject>();

        foreach (Transform child in piece.transform)
        {
            Vector2 pos = child.position;
            int x = (int)pos.x;
            int y = (int)pos.y;
            if (!objectsToDestroy.Contains(child.gameObject))          //Adding the default block from piece controller
                objectsToDestroy.Add(child.gameObject);
            if (x - 1 >= 0 && GridSystem.grid[x - 1, y] == null)      //left side : Making sure that neighbouring block is inside the grid in x negative axis and if even there? 
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x - 1, y].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x - 1, y].gameObject);
            }
            if (x + 1 < GridSystem.instance.width && GridSystem.grid[x + 1, y] != null)   //Right side : Making sure that neighbouring block is inside the grid in x positive axis and if even there?
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x + 1, y].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x + 1, y].gameObject);
            }
            if (y - 1 >= 0 && GridSystem.grid[x, y - 1] != null)      //down side : Making sure that neighbouring block is inside the grid in y negative axis and if even there?
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x, y - 1].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x, y - 1].gameObject);
            }
            if (y < GridSystem.instance.height && GridSystem.grid[x, y + 1] != null)      //Up side : Making sure that neighbouring block is inside the grid in y positive axis and if even there?
            {
                if (!objectsToDestroy.Contains(GridSystem.grid[x, y + 1].gameObject))
                    objectsToDestroy.Add(GridSystem.grid[x, y + 1].gameObject);
            }
        }

        /******************* This is just pseudo code of what to do with objects stored later, you should change it as per your preference *****************/
        //1. Delete all the objects you prefer to do them
        //2. Clear the Grid System
        //3. Call settle pieces in empty from gridsystem
        foreach (GameObject go in objectsToDestroy)
        {
            Vector2 pos = transform.position;
            int x = (int)pos.x;
            int y = (int)pos.y;
            Destroy(go,2f);
            Rigidbody rb = go.AddComponent<Rigidbody>();
            rb.useGravity = false;
            rb.AddExplosionForce(600f, rb.transform.position + (new Vector3(Random.Range(x,y), Random.Range(-2, 2), 2)), 5, 0);
            rb.AddTorque(new Vector3(Random.Range(-500, 500), Random.Range(-500, 500), Random.Range(-500, 500)));
            GridSystem.grid[(int)go.transform.position.x, (int)go.transform.position.y] = null;
            ScoreManager.instance.UpdateScore();
        }
        GridSystem.instance.SettlePeicesinEmpty();
    }
}
