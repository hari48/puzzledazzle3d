﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackLoopRL : MonoBehaviour
{
    [SerializeField] float moveSpeed = 0f;
    [SerializeField] GameObject planeFab;

    private float distanceofDeletion = 32f;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(-Vector3.forward * Time.deltaTime * moveSpeed);
        if (gameObject.transform.position.z <= distanceofDeletion)
        {
            Destroy(gameObject.transform.GetChild(0).gameObject);
            distanceofDeletion = distanceofDeletion - 32f;
            GameObject temp = Instantiate(planeFab, Vector3.zero, Quaternion.Euler(-89.98f,90,0));
            temp.transform.position = new Vector3(0, -1, gameObject.transform.GetChild(gameObject.transform.childCount - 1).transform.position.z + 32f);
            temp.transform.parent = gameObject.transform;
        }
    }
}

