using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class desertPathFollower : MonoBehaviour
{
    public PathCreator pathCreator;
    //public EndOfPathInstruction endOfPathInstruction;
    public float speed = 5;
    float distanceTravelled;
    Vector3 angleRot = new Vector3();
    Quaternion currentRotation;

    void Start()
    {

        if (pathCreator != null)
        {
            // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
            pathCreator.pathUpdated -= OnPathChanged;
        }
    }

    void Update()
    {


        if (pathCreator != null)
        {
            distanceTravelled += speed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
            
            angleRot = pathCreator.path.GetRotationAtDistance(distanceTravelled).eulerAngles;
            angleRot = new Vector3(-90, angleRot.y, 0);
            currentRotation.eulerAngles = angleRot;
            transform.rotation = currentRotation;
        }
    }

    // If the path changes during the game, update the distance travelled so that the follower's position on the new path
    // is as close as possible to its position on the old path
    void OnPathChanged()
    {

        distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
    }
}
