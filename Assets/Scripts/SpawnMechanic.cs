﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMechanic : MonoBehaviour
{
    public List<GameObject> spawnPieces;
    public Transform[] ExtraPiecesTransform;
    public Sprite[] piece_Tex;
    public Material[] mats;
    public Sprite[] currentPieceTex;

    public string pieceName = "Piece_Square";

    public List<Combinations> combinationsEasy, combinationsMedium, combinationsHard;

    public int ChanceOFSpawningSpecial=10;
    [SerializeField]
    GameObject HorizontalSpecialVisual;
    [SerializeField]
    GameObject VerticalSpecialVisual;

    int x = 0;

    bool notInitialspawn = false;
    private void Start()
    {
        /* if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "IntoTheOcean")
         {
             pieceName = "Piece_Square_Underwater";
         }
         else
         {
             pieceName = "Piece_Square";
         }



         int index = PlayerPrefs.GetInt("theme", 0);
         currentPieceTex = new Sprite[2];
         currentPieceTex[0] = piece_Tex[index * 2];
         currentPieceTex[1] = piece_Tex[(index * 2) + 1];
         spawnPieces = new List<GameObject>();

         if (LevelUniqueIdentifier.instance.animateToMusic)
         {

             mats[0].SetColor("Color_48F7F441", LevelUniqueIdentifier.instance.blockColor1);
             mats[1].SetColor("Color_48F7F441", LevelUniqueIdentifier.instance.blockColor2);
             mats[0].SetColor("Color_D18141CE", LevelUniqueIdentifier.instance.blockColor1);
             mats[1].SetColor("Color_D18141CE", LevelUniqueIdentifier.instance.blockColor2);
         }
         else
         {

            LevelUniqueIdentifier.instance.basicMat1.color = LevelUniqueIdentifier.instance.blockColor1;
             LevelUniqueIdentifier.instance.basicMat2.color = LevelUniqueIdentifier.instance.blockColor2;
             mats[0] = LevelUniqueIdentifier.instance.basicMat1;
             mats[1] = LevelUniqueIdentifier.instance.basicMat2;
             mats[0].SetColor("_EmissionColor", LevelUniqueIdentifier.instance.blockColor1);
             mats[1].SetColor("_EmissionColor", LevelUniqueIdentifier.instance.blockColor2);

         }

         GameManager.instance.SetGameDifficulty();

         SpawnPreProgrammedPieces();

         ReArrangeExtraPieces();*/

        x = 0;
        int index = PlayerPrefs.GetInt("theme", 0);
        /*
        currentPieceTex = new Sprite[4];
        currentPieceTex[0] = piece_Tex[0];
        currentPieceTex[1] = piece_Tex[1];
        currentPieceTex[2] = piece_Tex[2];
        currentPieceTex[3] = piece_Tex[3];*/

        mats[0] = LevelUniqueIdentifier.instance.basicMat1;
        mats[1] = LevelUniqueIdentifier.instance.basicMat2;
        mats[2] = LevelUniqueIdentifier.instance.basicMat3;
        spawnPieces = new List<GameObject>();

        for (int i = 0; i < 4; i++)
        {
            SpawnPiece();
            if (i == 0)
            {
                spawnPieces[i].gameObject.SetActive(false);
            }
            if (i > 0)
            {
                spawnPieces[i].GetComponent<PieceController>().enabled = false;
                spawnPieces[i].gameObject.SetActive(false);
            }
        }
        ReArrangeExtraPieces();


        ActivateNextSet();

    }



    public void SpawnPiece()
    {
        /*
        if (spawnPieces.Count != 0)
        {
            if (GameManager.instance.currentPiece == null)
                spawnPieces[0].GetComponent<PieceController>().enabled = true;
            GameManager.instance.currentPiece = spawnPieces[0].gameObject;
            GameManager.instance.currentPiece.transform.rotation = Quaternion.identity;

            if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.65f-1, GridSystem.instance.height + 0.7f, 0);
            else
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0);

            if (LevelUniqueIdentifier.instance.fallingParticle != null)
            {
                if(GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle != null)
                GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle.SetActive(true);
            }
        }

        if (spawnPieces.Count <= 3)
            SpawnPreProgrammedPieces();

        if (spawnPieces.Count > 0)
            ReArrangeExtraPieces();

        GameManager.instance.spawnPiece = false;*/



        if (spawnPieces.Count != 0)
        {
            if (GameManager.instance.currentPiece == null)
                spawnPieces[0].GetComponent<PieceController>().enabled = true;
            GameManager.instance.currentPiece = spawnPieces[0].gameObject;

            if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.65f - 1, GridSystem.instance.height + 0.7f, 0);
            else
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0);

          
        }
        GameObject go;
        if (GameManager.instance.gameOrientation == GameOrientation.landscape)
            go = Instantiate(Resources.Load("Piece_Square") as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.65f - 1, GridSystem.instance.height + 0.7f, 0), Quaternion.identity, transform);
        else
            go = Instantiate(Resources.Load("Piece_Square") as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0), Quaternion.identity, transform);
        go.GetComponent<PieceController>().enabled = false;
        SetPiece(go);
        spawnPieces.Add(go);
        if (spawnPieces.Count == 4)
            ReArrangeExtraPieces();
        GameManager.instance.spawnPiece = false;

    }

    public void ActivateNextSet()
    {
        GameManager.instance.currentPiece.SetActive(true);
        spawnPieces[1].SetActive(true);
    }
    /*
    public void SpawnPreProgrammedPieces()
    {

        #region setting difficulty
        List<Combinations> currentCombination = null;
        if (GameManager.instance.gameDifficulty == GameDifficulty.Easy)
            currentCombination = combinationsEasy;
        else if (GameManager.instance.gameDifficulty == GameDifficulty.Medium)
            currentCombination = combinationsMedium;
        else if (GameManager.instance.gameDifficulty == GameDifficulty.Hard)
            currentCombination = combinationsHard;
        #endregion

        #region calculating amount
        int amount = TotalAmountInASet(currentCombination);
        #endregion



        #region spawning the Set


        string tag1, tag2;
        GameObject go = null;
        List<GameObject> tempSpawnPieces = new List<GameObject>();

        foreach (Combinations c in currentCombination)
        {

            for (int i = 0; i < c.amount; i++)
            {
                int random = Random.Range(0, 2);
                if (random == 0)
                {
                    tag1 = "Blue";
                    tag2 = "Yellow";
                }
                else
                {
                    tag1 = "Yellow";
                    tag2 = "Blue";
                }

                if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                    go = Instantiate(Resources.Load(pieceName) as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.65f-1, GridSystem.instance.height + 0.7f, 0), Quaternion.identity, transform);
                else
                    go = Instantiate(Resources.Load(pieceName) as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0), Quaternion.identity, transform);
                go.GetComponent<PieceController>().enabled = false;

                if (c.color.Length == 2)
                {
                    GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 1).gameObject);
                    GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 2).gameObject);
                }


                for (int j = 0; j < c.color.Length; j++)
                {
                    if (c.color[j] == 0)
                    {
                        SetPiece(go.transform.GetChild(j).gameObject, tag1);
                    }
                    else
                    {
                        SetPiece(go.transform.GetChild(j).gameObject, tag2);
                    }
                }


                if (LevelUniqueIdentifier.instance.fallingParticle != null)
                {
                    GameObject fallParticle = Instantiate(LevelUniqueIdentifier.instance.fallingParticle as GameObject);
                    go.GetComponent<PieceController>().fallingParticle = fallParticle;
                    fallParticle.name = "fallingParticle";
                    fallParticle.SetActive(false);
                    fallParticle.transform.position = go.transform.position;
                }
                tempSpawnPieces.Add(go);

                if (tempSpawnPieces.Count >= 4)
                    ReArrangeExtraPieces();
                GameManager.instance.spawnPiece = false;
            }
        }

        UtilityHelper.Shuffle(tempSpawnPieces);

        foreach(GameObject g in tempSpawnPieces)
        {
            spawnPieces.Add(g);
        }

        tempSpawnPieces.Clear();

        if (spawnPieces.Count != 0)
        {
            if (GameManager.instance.currentPiece == null)
                spawnPieces[0].GetComponent<PieceController>().enabled = true;
            GameManager.instance.currentPiece = spawnPieces[0].gameObject;
            GameManager.instance.currentPiece.transform.rotation = Quaternion.identity;

            if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.65f-1, GridSystem.instance.height + 0.7f, 0);
            else
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0);

            if (LevelUniqueIdentifier.instance.fallingParticle != null)
            {
                if (GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle != null)
                    GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle.SetActive(true);
            }
        }
        #endregion
    }*/

    public void ReArrangeExtraPieces()
    {/*
        for(int i = 1; i < spawnPieces.Count; i++)
        {
            if (i >= ExtraPiecesTransform.Length)
            {
                spawnPieces[i].transform.position = ExtraPiecesTransform[ExtraPiecesTransform.Length - 1].position;
                ExtraPiecesTransform[ExtraPiecesTransform.Length - 1].transform.LookAt(Camera.main.transform);
                spawnPieces[i].transform.LookAt(Camera.main.transform);
            }
            else
            {
                spawnPieces[i].transform.position = ExtraPiecesTransform[i - 1].position;
                ExtraPiecesTransform[i - 1].transform.LookAt(Camera.main.transform);
                spawnPieces[i].transform.LookAt(Camera.main.transform);
            }
        }*/


        for (int i = 1; i < 4; i++)
        {
            spawnPieces[i].transform.position = ExtraPiecesTransform[i - 1].position;
            spawnPieces[i].gameObject.SetActive(false);
        }
    }

    private void SetPiece(GameObject go)
    {/*
        go.tag = tag;
        int rand = 0;
        if (tag == "Blue")
            rand = 0;
        else if (tag == "Yellow")
            rand = 1;
        go.GetComponent<Renderer>().material = mats[rand];

        int random1 = Random.Range(0, ChanceOFSpawningSpecial);
        if (random1 == 0)
        {
            int random2 = Random.Range(0, 2);
            if (random2 == 0)
            {
                go.name= "stripeHorizontal";
                GameObject hsv = Instantiate(HorizontalSpecialVisual, go.transform.position, Quaternion.Euler(-90,0,0));
                hsv.GetComponent<FollowSpecialBlock>().target = go.transform;
            }
            else if (random2==1)
            {
                go.name = "stripeVertical";
                GameObject vsv = Instantiate(VerticalSpecialVisual, go.transform.position, Quaternion.Euler(-90, 0, 0));
                vsv.GetComponent<FollowSpecialBlock>().target = go.transform;
            } 
        }*/

        int randomP = Random.Range(0, LevelManager.current.ChanceOfSpawning4Piece + LevelManager.current.ChanceOfSpawning2Piece);


        if (randomP >= 0 && randomP < LevelManager.current.ChanceOfSpawning4Piece)//4 piece spawn
        {
            foreach (Transform t in go.transform)
            {
                int rand = Random.Range(0, (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed + LevelManager.current.ChanceOfSpawningGreen));
                int rand1 = Random.Range(0, LevelManager.current.ChanceOfSpawningHorizontal + LevelManager.current.ChanceOfSpawningVertical);
                int randForSpecial = Random.Range(0, LevelManager.current.ChanceOfSpawningSpecial);


                if (rand >= 0 && rand < LevelManager.current.ChanceOfSpawningBlue)
                {
                    t.GetComponent<Renderer>().material = mats[0];
                    t.tag = "Blue";
                }
                else if (rand >= LevelManager.current.ChanceOfSpawningBlue && rand < (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow))
                {
                    t.GetComponent<Renderer>().material = mats[1];
                    t.tag = "Yellow";
                }
                else if (rand >= (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow) && rand < (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed))
                {
                    t.GetComponent<Renderer>().material = mats[2];
                    t.tag = "Red";
                }
                else if (rand >= (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed) && rand <= (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed + LevelManager.current.ChanceOfSpawningGreen))
                {
                    t.GetComponent<Renderer>().material = mats[3];
                    t.tag = "Green";
                }


                if (rand1 >= 0 && rand1 < LevelManager.current.ChanceOfSpawningHorizontal)
                {
                    if (randForSpecial == 0)
                    {
                        t.name = "stripeHorizontal";
                        GameObject hsv = Instantiate(HorizontalSpecialVisual, t.position, Quaternion.Euler(-90, 0, 0));
                        hsv.GetComponent<FollowSpecialBlock>().target = t;

                    }
                }
                else if (rand1 >= LevelManager.current.ChanceOfSpawningHorizontal && rand1 <= LevelManager.current.ChanceOfSpawningVertical)
                {
                    if (randForSpecial == 0)
                    {
                        t.name = "stripeVertical";
                        GameObject vsv = Instantiate(VerticalSpecialVisual, t.position, Quaternion.Euler(-90, 0, 0));
                        vsv.GetComponent<FollowSpecialBlock>().target = t;
                    }
                }


            }
        }
        else if (randomP >= LevelManager.current.ChanceOfSpawning4Piece && randomP <= (LevelManager.current.ChanceOfSpawning4Piece + LevelManager.current.ChanceOfSpawning2Piece))//2 piece spawn
        {
            GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 1).gameObject);
            GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 2).gameObject);
            foreach (Transform t in go.transform)
            {
                int rand = Random.Range(0, (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed + LevelManager.current.ChanceOfSpawningGreen));
                int rand1 = Random.Range(0, LevelManager.current.ChanceOfSpawningHorizontal + LevelManager.current.ChanceOfSpawningVertical);
                int randForSpecial = Random.Range(0, LevelManager.current.ChanceOfSpawningSpecial);


                if (rand >= 0 && rand < LevelManager.current.ChanceOfSpawningBlue)
                {
                    t.GetComponent<Renderer>().material = mats[0];
                    t.tag = "Blue";
                }
                else if (rand >= LevelManager.current.ChanceOfSpawningBlue && rand < (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow))
                {
                    t.GetComponent<Renderer>().material = mats[1];
                    t.tag = "Yellow";
                }
                else if (rand >= (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow) && rand < (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed))
                {
                    t.GetComponent<Renderer>().material = mats[2];
                    t.tag = "Red";
                }
                else if (rand >= (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed) && rand <= (LevelManager.current.ChanceOfSpawningBlue + LevelManager.current.ChanceOfSpawningYellow + LevelManager.current.ChanceOfSpawningRed + LevelManager.current.ChanceOfSpawningGreen))
                {
                    t.GetComponent<Renderer>().material = mats[3];
                    t.tag = "Green";
                }


                if (rand1 >= 0 && rand1 < LevelManager.current.ChanceOfSpawningHorizontal)
                {
                    if (randForSpecial == 0)
                    {
                        t.name = "stripeHorizontal";
                        GameObject hsv = Instantiate(HorizontalSpecialVisual, t.position, Quaternion.Euler(-90, 0, 0));
                        hsv.GetComponent<FollowSpecialBlock>().target = t;


                    }
                }
                else if (rand1 >= LevelManager.current.ChanceOfSpawningHorizontal && rand1 <= LevelManager.current.ChanceOfSpawningVertical)
                {
                    if (randForSpecial == 0)
                    {
                        t.name = "stripeVertical";
                        GameObject vsv = Instantiate(VerticalSpecialVisual, t.position, Quaternion.Euler(-90, 0, 0));
                        vsv.GetComponent<FollowSpecialBlock>().target = t;
                    }
                }


            }
        }

    }

    private int TotalAmountInASet(List<Combinations> currentCombination)
    {
        int amount = 0;
        foreach (Combinations c in currentCombination)
        {
            amount += c.amount;
        }
        return amount;
    }
}
