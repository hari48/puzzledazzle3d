﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.Analytics;

public enum GameOrientation
{
    landscape = 0,
    portrait = 1
}
 public enum GameMode
{
    Arcade = 0,
    Survival = 1

}

public enum GameDifficulty
{
    Easy = 0,
    Medium = 1,
    Hard = 2
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool isGameover = false;
    public GameOrientation gameOrientation;
    public GameMode gameMode;
    public GameDifficulty gameDifficulty;
    public bool isSurvival = true;


    public bool spawnPiece = false;

    public GameObject currentPiece;

    public GameObject[] pieceControllerObj;

    public float customTimeScale = 1f;

    public TMP_Dropdown dropdown;

    public GameObject controlButtons;
    public bool isSwipingEnabled = false;
    private float fallSpeed = 0.8f;

    public GameObject Effects;

    public Scene intoTheOcenaScene, magicOrbScene, snowFallScene;

    private bool Loader = false;

    PieceControl controls;

    public float FallSpeed
    {
        get
        {
            return fallSpeed;
        }
        set
        {
            fallSpeed = value;
        }
    }

    private int coins = 0;

    public int Coins { get { return coins; } set { coins = value; } }

    private void Start()
    {
 

        UIHandler.instance.UpdateScoreUI();
        coins = PlayerPrefs.GetInt("coins", 0);

        UIHandler.instance.UpdateScoreUI();

        if(SceneManager.GetActiveScene().name == "Menu")
        ControlDropDown();

        Analytics.CustomEvent("Started Level", new Dictionary<string, object>
        {
            {"Level Name", SceneManager.GetActiveScene().name}
        });

        PauseGameWithoutUI();
    }

    private IEnumerator DelayEffects()
    {
        yield return new WaitForSeconds(1f);
        Effects.SetActive(true);
    }
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

        int level = SceneManager.GetActiveScene().buildIndex;
        Debug.Log("current levelno: " + level + " levelName: " + GetSceneNameFromBuildIndex(level) + "\n Next level name: " + GetSceneNameFromBuildIndex(level + 1));

        controls = new PieceControl();
        controls.Gameplay.PauseGame.performed += ctx => PauseGame();
    }

    void PauseGame()
    {
        UIHandler.instance.OnPressedPause();
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }
    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    private void Update()
    {/*
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIHandler.instance.OnPressedPause();
        }*/


        if (Input.GetKeyDown(KeyCode.Z))
        {
            FindObjectOfType<AudioHandler>().mainAudioSource.time = FindObjectOfType<AudioHandler>().mainAudioSource.clip.length - 3;
        }

        if (FindObjectOfType<LevelUniqueIdentifier>().songcompleted  || Input.GetKeyDown(KeyCode.Q))
        {
            
            /*   int level = SceneManager.GetActiveScene().buildIndex;

               level++;

               if (level > SceneManager.sceneCountInBuildSettings)
                   level = 3;

               Debug.LogError("SF " + SceneManager.sceneCount);
               SceneManager.LoadScene(level);*/


            Analytics.CustomEvent("Completed Level", new Dictionary<string, object>
                {
                    {"Level Name", SceneManager.GetActiveScene().name}
                });

            Analytics.CustomEvent("Score Won Level", new Dictionary<string, object>
                {
                    {"Level Name", SceneManager.GetActiveScene().name},
                    {"Score", ScoreManager.instance.score}
                });
            int l1 = PlayerPrefs.GetInt("levelsCompleted_Snowfall", 0);
            int l2 = PlayerPrefs.GetInt("levelsCompleted_Underwater", 0);
            int l3 = PlayerPrefs.GetInt("levelsCompleted_Starfield", 0);
            int l4 = PlayerPrefs.GetInt("levelsCompleted_Desert", 0);
            int l5 = PlayerPrefs.GetInt("levelsCompleted_Northern Lights", 0);
            int l6 = PlayerPrefs.GetInt("levelsCompleted_Journey", 0);
            int l7 = PlayerPrefs.GetInt("levelsCompleted_Space", 0);
            int l8 = PlayerPrefs.GetInt("levelsCompleted_Magic Orb", 0);
            int l9 = PlayerPrefs.GetInt("levelsCompleted_Black Hole", 0);
            int l10 = PlayerPrefs.GetInt("levelsCompleted_Into The Depths", 0);
            int l11 = PlayerPrefs.GetInt("levelsCompleted_Magic Orb 2", 0);
            int l12 = PlayerPrefs.GetInt("levelsCompleted_Seven Wonders", 0);
            int l13 = PlayerPrefs.GetInt("levelsCompleted_Movement", 0);
            int l14 = PlayerPrefs.GetInt("levelsCompleted_Ocean World", 0);
            int l15 = PlayerPrefs.GetInt("levelsCompleted_Retro Landscape", 0);
            int l16 = PlayerPrefs.GetInt("levelsCompleted_Cherry Blossom", 0);
            int l17 = PlayerPrefs.GetInt("levelsCompleted_Hot Air Balloon", 0);
            int l18 = PlayerPrefs.GetInt("levelsCompleted_Butterfly", 0);
            int l19 = PlayerPrefs.GetInt("levelsCompleted_Beach", 0);
            int l20 = PlayerPrefs.GetInt("levelsCompleted_Fireworks", 0);

            if (Loader == false)
            { 
            Debug.Log("Game Manager line 133 anand v   New Level Logic ?");
            PlayerPrefs.SetInt("levelsCompleted_" + SceneManager.GetActiveScene().name, 1);
            int level = SceneManager.GetActiveScene().buildIndex;  //anand v addition

            level++;

            if (level > SceneManager.sceneCountInBuildSettings)
                level = 3;

                LevelAsyncInBetween.instance.CircleGrowCouroutineStart(GetSceneNameFromBuildIndex(level));
                Loader = true;
             }




            Debug.Log(l1+" L:"+l2+ " L:" + l3 + " L:" + l4 + " L5: " + l5 + " L6: " + l6 + " L7: " + l7 + " L8: " + l8 + " L9: " + l9 + " L10: " + l10 + " L11: " + l11 + " L12: " + l12 + " L1: " + l13 + " L14: " + l14 + " L15: " + l15 + " L16: " + l16 + " L17: " + l17 + " L18: " + l18 + " L19:" + l19 + " L20:" + l20);
           /* if (l1 == 0)
            {
                SceneManager.LoadScene("Snowfall");
            }
                
            else if (l2 == 0)
            {
                SceneManager.LoadScene("Underwater");
            }
                
            else if (l3 == 0)
            {
                SceneManager.LoadScene("Starfield");
            }
                
            else if (l4 == 0)
            {
                LevelAsync.instance.CircleGrowCouroutineStart("Desert");  // anand v additions  transition scene
                Debug.Log("Game Manager line 185 anand v");
                //SceneManager.LoadScene("Desert");
            }
                
            else if (l5 == 0)
            {
                SceneManager.LoadScene("Northern Lights");
            }
                
            else if (l6 == 0)
            {
                SceneManager.LoadScene("Journey");
            }
                
            else if (l7 == 0)
            {
                SceneManager.LoadScene("Space");
            }
                
            else if (l8 == 0)
            {
                SceneManager.LoadScene("Magic Orb");   
            }
                
            else if (l9 == 0)
            {
                SceneManager.LoadScene("Blackhole");  //anand v: earlier Black Hole
            }
                
            else if (l10 == 0)
            {
                SceneManager.LoadScene("Into The Depths");
            }
                
            else if (l11 == 0)
            {
                SceneManager.LoadScene("Magic Orb 2");
            }
                
            else if (l12 == 0)
            {
                SceneManager.LoadScene("Seven Wonders");
            }
                
            else if (l13 == 0)
            {
                SceneManager.LoadScene("Movement");
            }
                
            else if (l14 == 0)
            {
                SceneManager.LoadScene("Ocean World");
            }
                
            else if (l15 == 0)
            {
                SceneManager.LoadScene("Retro Landscape");
            }
                
            else if (l16 == 0)
            {
                SceneManager.LoadScene("Cherry Blossom");
            }
                
            else if (l17 == 0)
            {
                SceneManager.LoadScene("Hot Air Balloon");
            }
                
            else if (l18 == 0)
            {
                SceneManager.LoadScene("Butterfly");
            }
                
            else if (l19 == 0)
            {
                SceneManager.LoadScene("Beach");
            }
                
            else if (l20 == 0)
            {
                SceneManager.LoadScene("Fireworks");
            }
                
            else
                SceneManager.LoadScene("Menu");
            */
      


        }
        else if (FindObjectOfType<LevelUniqueIdentifier>().songcompleted && !FindObjectOfType<LevelUniqueIdentifier>().targetAchieved)
        {
           /* ScoreManager.instance.SaveScore();
            AudioHandler.Instance.FadeAudio();
            GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
            GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            FindObjectOfType<TrailHead>().StopAllCoroutines();
            UIHandler.instance.ShowResult();*/
        }
    }
    public void PauseGameWithoutUI()
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        currentPiece.GetComponent<PieceController>().enabled = false;
    }
    public void ResumeGameWithoutUI()
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        currentPiece.GetComponent<PieceController>().enabled = true;
    }

    private void ShowResult()
    {
        //UIHandler.instance.ShowResult();
        Instantiate(Resources.Load("Effects") as GameObject);
    }

    public void OnPressedLeft()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(1);
    }

    public void OnPressedRight()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(0);
    }

    public void OnPressedFlip()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(3);
    }

    public void ControlDropDown()
    {
        if(dropdown.value == 0)
        {
            isSwipingEnabled = true;
            controlButtons.SetActive(false);
        }
        else if(dropdown.value == 1)
        {
            isSwipingEnabled = false;
            controlButtons.SetActive(true);
        }
    }

    public void UpdateCoins(int amount)
    {
        Coins += amount;
        UIHandler.instance.UpdateScoreUI();
    }

    private void SetGameMode()
    {
        switch(PlayerPrefs.GetInt("gamemode"))
        {
            case 0:
                gameMode = GameMode.Arcade;
                break;
            case 1:
                gameMode = GameMode.Survival;
                break;
            default:
                break;
        }
    }

    public void SetGameDifficulty()
    {
        switch (PlayerPrefs.GetInt("gamedifficulty"))
        {
            case 0:
                gameDifficulty = GameDifficulty.Easy;
                break;
            case 1:
                gameDifficulty = GameDifficulty.Medium; 
                break;
            case 2:
                gameDifficulty = GameDifficulty.Hard;
                break;
            default:
                break;
        }
    }

    public IEnumerator TempSlowMotion(float seconds, float slowValue)
    {
        Time.timeScale = slowValue;
        yield return new WaitForSeconds(seconds);
        Time.timeScale = 1;
    }

    public static string GetSceneNameFromBuildIndex(int index)
    {
        string scenePath = SceneUtility.GetScenePathByBuildIndex(index);
        string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePath);

        return sceneName;
    }
}
