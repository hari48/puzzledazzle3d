﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonderSelection : MonoBehaviour
{
    public GameObject[] Objects;
    public int selectedWonder = 0;
    private string selectedWonderDataName = "selectedWonder";
    
    // Start is called before the first frame update
    void Start()
    {
        
        HideAllWonders();

        selectedWonder = PlayerPrefs.GetInt(selectedWonderDataName, 0);

        Objects[selectedWonder].SetActive(true);
        InvokeRepeating("WonderSelect", 5,5);

    }
    
    private void HideAllWonders()
    {
        foreach (GameObject g in Objects)
        {
            g.SetActive(false);
        }
    }
    public void WonderSelect()
    {
        Objects[selectedWonder].SetActive(false);
        selectedWonder++;
        if (selectedWonder >= Objects.Length)
        {
            selectedWonder = 0;
        }
        Objects[selectedWonder].SetActive(true);
    }
}
