﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelUniqueIdentifier : MonoBehaviour
{
    public static LevelUniqueIdentifier instance;

    public int levelTarget = 20;
    public string levelName;

    public bool animateToMusic = true;

    public AudioClip[] clipstobeplayed;

    public GameObject clearingAnim;
    public GameObject fallingParticle;

    public Color blockColor1, blockColor2,blockColor3,blockColor4;

    public Material basicMat1, basicMat2,basicMat3,basicMat4;


    [HideInInspector]
    public bool levelCompleted = false;
    [HideInInspector]
    public bool targetAchieved = false;
    [HideInInspector]
    public bool songcompleted = false;
    private void Awake()
    {
        instance = null;
        //Assigning Instance
        if(instance == null)
        {
            instance = this;
        }
    }
    private void Start()
    {
        PlayerPrefs.SetInt("lastlevelPlayed", SceneManager.GetActiveScene().buildIndex);

        if (levelName == "Space")
            Instantiate(Resources.Load("Effects") as GameObject);
    }
}
