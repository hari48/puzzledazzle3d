using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScene : MonoBehaviour
{
    public void MoveToScene(string sceneID)
    {
        Time.timeScale = 1;
        LevelAsync.instance.LoadScene(sceneID);
    }
}
