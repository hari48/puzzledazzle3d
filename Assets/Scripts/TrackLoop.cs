﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackLoop : MonoBehaviour
{
    [SerializeField] float moveSpeed = 0f;
    [SerializeField] GameObject sciFiLoop;

    private float distanceofDeletion = 30f;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(-Vector3.forward * Time.deltaTime * moveSpeed);
        if (gameObject.transform.position.z <= distanceofDeletion)
        {
            Destroy(gameObject.transform.GetChild(0).gameObject);
            distanceofDeletion = distanceofDeletion - 30f;
            GameObject temp = Instantiate(sciFiLoop, Vector3.zero, Quaternion.identity);
            temp.transform.position = new Vector3(0, 0, gameObject.transform.GetChild(gameObject.transform.childCount - 1).transform.position.z + 30f);
            temp.transform.parent = gameObject.transform;
        }
    }
}
