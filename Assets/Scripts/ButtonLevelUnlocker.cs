using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
///  Code by anand V 15-09-21
/// </summary>
public class ButtonLevelUnlocker : MonoBehaviour
{
    public GameObject duller2;
    public GameObject duller3;
    public GameObject duller4;
    public GameObject duller5;
    public GameObject duller6;
    public GameObject duller7;
    public GameObject duller8;
    public GameObject duller9;
    public GameObject duller10;
    public GameObject duller11;
    public GameObject duller12;
    public GameObject duller13;
    public GameObject duller14;
    public GameObject duller15;
    public GameObject duller16;
    public GameObject duller17;
    public GameObject duller18;
    public GameObject duller19;
    public GameObject duller20;
    // Start is called before the first frame update
    void Start()
    {
        int l1 = PlayerPrefs.GetInt("levelsCompleted_Snowfall", 0);
        int l2 = PlayerPrefs.GetInt("levelsCompleted_Underwater", 0);
        int l3 = PlayerPrefs.GetInt("levelsCompleted_Starfield", 0);
        int l4 = PlayerPrefs.GetInt("levelsCompleted_Desert", 0);
        int l5 = PlayerPrefs.GetInt("levelsCompleted_Northern Lights", 0);
        int l6 = PlayerPrefs.GetInt("levelsCompleted_Journey", 0);
        int l7 = PlayerPrefs.GetInt("levelsCompleted_Space", 0);
        int l8 = PlayerPrefs.GetInt("levelsCompleted_Magic Orb", 0);
        int l9 = PlayerPrefs.GetInt("levelsCompleted_Black Hole", 0);
        int l10 = PlayerPrefs.GetInt("levelsCompleted_Into The Depths", 0);
        int l11 = PlayerPrefs.GetInt("levelsCompleted_Magic Orb 2", 0);
        int l12 = PlayerPrefs.GetInt("levelsCompleted_Seven Wonders", 0);
        int l13 = PlayerPrefs.GetInt("levelsCompleted_Movement", 0);
        int l14 = PlayerPrefs.GetInt("levelsCompleted_Ocean World", 0);
        int l15 = PlayerPrefs.GetInt("levelsCompleted_Retro Landscape", 0);
        int l16 = PlayerPrefs.GetInt("levelsCompleted_Cherry Blossom", 0);
        int l17 = PlayerPrefs.GetInt("levelsCompleted_Hot Air Balloon", 0);
        int l18 = PlayerPrefs.GetInt("levelsCompleted_Butterfly", 0);
        int l19 = PlayerPrefs.GetInt("levelsCompleted_Beach", 0);
        int l20 = PlayerPrefs.GetInt("levelsCompleted_Fireworks", 0);

        if (l2 == 1)
        {

            duller2.transform.Find("duller").gameObject.SetActive(false);
            duller2.GetComponent<BoxCollider>().enabled = true;
            
        }

        if (l3 == 1)
        {
            duller3.transform.Find("duller").gameObject.SetActive(false);
            duller3.GetComponent<BoxCollider>().enabled = true;
        }
        if (l4 == 1)
        {
            duller4.transform.Find("duller").gameObject.SetActive(false);
            duller4.GetComponent<BoxCollider>().enabled = true;
        }
        if (l5 == 1)
        {
            duller5.transform.Find("duller").gameObject.SetActive(false);
            duller5.GetComponent<BoxCollider>().enabled = true;
        }
        if (l6 == 1)
        {
            duller6.transform.Find("duller").gameObject.SetActive(false);
            duller6.GetComponent<BoxCollider>().enabled = true;
        }
        if (l7 == 1)
        {
            duller7.transform.Find("duller").gameObject.SetActive(false);
            duller7.GetComponent<BoxCollider>().enabled = true;
        }
        if (l8 == 1)
        {
            duller8.transform.Find("duller").gameObject.SetActive(false);
            duller8.GetComponent<BoxCollider>().enabled = true;
        }
        if (l9 == 1)
        {
            duller9.transform.Find("duller").gameObject.SetActive(false);
            duller9.GetComponent<BoxCollider>().enabled = true;
        }
        if (l10 == 1)
        {
            duller10.transform.Find("duller").gameObject.SetActive(false);
            duller10.GetComponent<BoxCollider>().enabled = true;
        }
        if (l11 == 1)
        {
            duller11.transform.Find("duller").gameObject.SetActive(false);
            duller11.GetComponent<BoxCollider>().enabled = true;
        }
        if (l12 == 1)
        {
            duller12.transform.Find("duller").gameObject.SetActive(false);
            duller12.GetComponent<BoxCollider>().enabled = true;
        }
        if (l13 == 1)
        {
            duller13.transform.Find("duller").gameObject.SetActive(false);
            duller13.GetComponent<BoxCollider>().enabled = true;
        }
        if (l14 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }
        if (l15 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }
        if (l16 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }
        if (l17 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }
        if (l18 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }
        if (l19 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }
        if (l20 == 1)
        {
            duller14.transform.Find("duller").gameObject.SetActive(false);
            duller14.GetComponent<BoxCollider>().enabled = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
