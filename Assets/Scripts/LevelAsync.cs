using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using UnityEngine.UI;

public class LevelAsync : MonoBehaviour
{
    public static LevelAsync instance;

    public Text m_Text;

[SerializeField] public GameObject _loaderCanvas;
    [SerializeField] private GameObject _Transitor;
    [SerializeField] private GameObject _env;
    [SerializeField] private GameObject _ui;
    public Animator fader;
    private bool is_circleGrowDone = false;
    // [SerializeField] private Animator circleBlackFader;



    // Start is called before the first frame update
    private void Awake()
    {
        

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
              
    }
 
    public void CircleGrowCouroutineStart(string sceneName)
    {
        StartCoroutine(CircleGrowThenLoadSyncCoroutine(sceneName));
        Debug.Log("Circle Gow coroutine line46");
       // LoadMyAsyncScene(sceneName);
        //StartCoroutine(LoadYourAsyncScene(sceneName));
        

    }

    IEnumerator CircleGrowThenLoadSyncCoroutine(string SceneName)
    {
        fader = _loaderCanvas.GetComponent<Animator>();
        _loaderCanvas.SetActive(true);
        fader.SetBool("Start", true);
        Debug.Log(fader.GetBool("Start") + "  bool start for animator Fade, level async line 50");

        
        yield return new WaitForSeconds(2f);
        _env.SetActive(false);
        _ui.SetActive(false);

        StartCoroutine(TransitorStartThenAsync(SceneName));

        //LoadScene(SceneName);

    }

    IEnumerator TransitorStartThenAsync(string SceneName)
    { 


        _Transitor.SetActive(true);
        _loaderCanvas.SetActive(false);
        Debug.Log("inside transitor Coroutine");


        yield return new WaitForSeconds(2f);
         Debug.Log("levelSync after coroutine transitor");
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(SceneName);

        //LoadScene(SceneName);

    }
    // Update is called once per frame

    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public async void LoadMyAsyncScene(string sceneName)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
       // Stopwatch watch = new Stopwatch();
        //watch.Start();
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        var result = await Task.Run(() =>
        {
            

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                Debug.Log("waiting)");
            }
            return 0;
        });

        Debug.Log("await run done");
        
    }





    public async void LoadScene(string sceneName)    //public async void LoadScene(string sceneName)
    {
       /* fader = _loaderCanvas.GetComponent<Animator>();
        //_loaderCanvas.SetActive(true);
        //_Transitor.SetActive(true);
        fader.SetBool("Start", true);
        Debug.Log(fader.GetBool("Start") + "  bool start for animator Fade, level async line 50");*/

        /*while (AnimatorIsPlaying())
        {
            Debug.Log("animator play wait level async line 49");
        }
        while (!(fader.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !fader.IsInTransition(0)))
        {
            is_circleGrowDone = false;
        }*/
 
        //is_circleGrowDone = true;
     

        
            
            var scene = SceneManager.LoadSceneAsync(sceneName);
            scene.allowSceneActivation = false;
            //Star.loadlevel(sceneName);

            Debug.Log("LevelAsync: line 32");
        
            do
            {
            //circleBlackFader.SetBool("Start", true);
            /* _loaderCanvas.SetActive(true);
             fader.SetBool("Start", true);
             //_Transitor.SetActive(true) ;*/
            //_Transitor.SetActive(true);


                await System.Threading.Tasks.Task.Delay(20);
            } while (scene.progress < 0.9f);

            scene.allowSceneActivation = true;
            _loaderCanvas.SetActive(false);
           _Transitor.SetActive(false);
        Destroy(this.gameObject);
        }



    /* bool AnimatorIsPlaying() 
     { 
         return fader.GetCurrentAnimatorStateInfo(0).length > fader.GetCurrentAnimatorStateInfo(0).normalizedTime;
     }*/
    public void LoadButtonAsyncScene(string sceneName)
    {
        //Start loading the Scene asynchronously and output the progress bar
        StartCoroutine(LoadScener(sceneName));
    }

    IEnumerator LoadScener(string sceneName)
    {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Output the current progress
            System.Threading.Tasks.Task.Delay(500);
            m_Text.text = "Loading progress: " + (asyncOperation.progress * 100) + "%";

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
                //Change the Text to show the Scene is ready
                m_Text.text = "starting scene....";
                //Wait to you press the space key to activate the Scene
                /*if (Input.GetKeyDown(KeyCode.Space))
                    //Activate the Scene
                    asyncOperation.allowSceneActivation = true;*/
            }

            yield return null;
        }
    }


}
