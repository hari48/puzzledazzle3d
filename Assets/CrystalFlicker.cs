using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalFlicker : MonoBehaviour
{
    public float FlickerSpeed;
    public float EmissionStrength;

    List<Material> matlist = new List<Material>();
    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in transform)
        {
            matlist.Add(child.GetComponent<Renderer>().material);
        }
    }
    private void OnEnable()
    {
        StartCoroutine(Flicker());
    }
    private void OnDisable()
    {
        StopCoroutine(Flicker());
    }

    IEnumerator Flicker()
    {
        while (true)
        {
            foreach (var item in matlist)
            {
                item.SetFloat("_EmissionPower", 0);
            }
            yield return new WaitForSecondsRealtime(FlickerSpeed);
            foreach (var item in matlist)
            {
                item.SetFloat("_EmissionPower", EmissionStrength);
            }
            yield return new WaitForSecondsRealtime(FlickerSpeed);
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
